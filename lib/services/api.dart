import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http_parser/http_parser.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';

class Api {
  static final BASE_URL = 'https://admin.2bill.net';
  Helper helper = new Helper();
  LocalLogin localLogin = new LocalLogin();
  final JsonDecoder _decoder = new JsonDecoder();
  Future<bool> logout() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.clear();
  }

  doUpload(File image, var headers) {
    String url = "/api/uploader";
    var request = http.MultipartRequest(
      'POST',
      Uri.parse('$BASE_URL' + '$url'),
    );

    request.files.add(
      http.MultipartFile(
        'image',
        image.readAsBytes().asStream(),
        image.lengthSync(),
        filename: "filename",
        contentType: MediaType('image', 'jpeg'),
      ),
    );
    request.headers.addAll(headers);
    print("request: " + request.toString());
    request.send().then((value) => print(value.statusCode));
  }

  Future<dynamic> getData(String url, var headers,BuildContext context ,{body, encoding}) {
    print(headers);

    var uri = Uri.parse('$BASE_URL' + '$url');
    uri = uri.replace(queryParameters: body);
    print(uri);
    return http.get(uri, headers: headers).then((http.Response response) async {
      print(response.statusCode);

       if ( response.statusCode == 401) {
           localLogin.auth_refreshtoken(context);
        }
      return response;
    });
  }

  Future<dynamic> postData(String url, var headers,BuildContext context, {body, encoding}) {
    print('$BASE_URL' + '$url');
    print(headers);

    var data = json.encode(body);
    print(data);
    return http
        .post('$BASE_URL' + '$url', body: data, headers: headers)
        .then((http.Response response) async {
      
        if ( response.statusCode == 401) {
           localLogin.auth_refreshtoken(context);
        }
     
      return response;
    });
  }

  Future<dynamic> putData(String url, var headers,BuildContext context, {body, encoding}) {
    print(headers);

    var data = json.encode(body);
    print(data);
    return http
        .put('$BASE_URL' + '$url', body: data, headers: headers)
        .then((http.Response response) async {
       if ( response.statusCode == 401) {
           localLogin.auth_refreshtoken(context);
        }
      return response;
    });
  }

  Future<dynamic> deleteData(String url, var headers,BuildContext context) {
    print(headers);

    print('$BASE_URL' + '$url');
    return http
        .delete('$BASE_URL' + '$url', headers: headers)
        .then((http.Response response) async {
      final int statusCode = response.statusCode;
      print(statusCode);
      if ( response.statusCode == 401) {
           localLogin.auth_refreshtoken(context);
        }
      return response;
    });
  }
}
