import 'dart:convert';

import 'package:two_bill/model/provider.dart';
import 'package:two_bill/model/updateDates.dart';

import 'createDates.dart';

class ServiceProvider {
  int id;
  String name;
  String nameAr;
  String nameEn;
  String logo;
Provider  provider;
  CreateDates createDates;
  UpdateDates updateDates;

  ServiceProvider(
      {this.id,
      this.name,
      this.nameAr,
      this.nameEn,
      this.logo,
      this.provider,
      this.createDates,
      this.updateDates});

  ServiceProvider.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    logo = json['logo'];
    provider = json['provider'] != null
        ? new Provider.fromJson(json['provider'])
        : null;
    createDates = json['create_dates'] != null
        ? new CreateDates.fromJson(json['create_dates'])
        : null;
    updateDates = json['update_dates'] != null
        ? new UpdateDates.fromJson(json['update_dates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['logo'] = this.logo;
    if (this.provider != null) {
      data['provider'] = this.provider.toJson();
    }
    if (this.createDates != null) {
      data['create_dates'] = this.createDates.toJson();
    }
    if (this.updateDates != null) {
      data['update_dates'] = this.updateDates.toJson();
    }
    return data;
  }
   static String encode(List<ServiceProvider> serviceProviders) => json.encode(
        serviceProviders
            .map<Map<String, dynamic>>((serviceProvider) => toMap(serviceProvider))
            .toList(),
      );

  static List<ServiceProvider> decode(String serviceProvider) =>
      (json.decode(serviceProvider) as List<dynamic>)
          .map<ServiceProvider>((item) => ServiceProvider.fromJson(item))
          .toList();
  static Map<String, dynamic> toMap(ServiceProvider serviceProvider) => {
        "id": serviceProvider.id,
        "name": serviceProvider.name,
        "name_ar": serviceProvider.nameAr,
        "name_en": serviceProvider.nameEn,
        "logo": serviceProvider.logo,
          if (serviceProvider.provider != null) 
      "provider": Provider.toMap(serviceProvider.provider),
        if (serviceProvider.createDates != null) 
      "create_dates": CreateDates.toMap(serviceProvider.createDates),
    
    if (serviceProvider.updateDates != null) 
      "update_dates": UpdateDates.toMap(serviceProvider.updateDates),
    
      };

}
