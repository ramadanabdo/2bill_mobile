import 'dart:convert';

import 'package:two_bill/model/updateDates.dart';
import 'package:two_bill/model/user.dart';

import 'createDates.dart';

class NotificationModel {
  int id;
  String type;
  String message;
  User user;
  String sender;
  CreateDates createDates;
  UpdateDates updateDates;

  NotificationModel(
      {this.id,
      this.type,
      this.message,
      this.user,
      this.sender,
      this.createDates,
      this.updateDates});

  NotificationModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    type = json['type'];
    message = json['message'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    sender = json['sender'];
    createDates = json['create_dates'] != null
        ? new CreateDates.fromJson(json['create_dates'])
        : null;
    updateDates = json['update_dates'] != null
        ? new UpdateDates.fromJson(json['update_dates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['type'] = this.type;
    data['message'] = this.message;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['sender'] = this.sender;
    if (this.createDates != null) {
      data['create_dates'] = this.createDates.toJson();
    }
    if (this.updateDates != null) {
      data['update_dates'] = this.updateDates.toJson();
    }
    return data;
  }
  static String encode(List<NotificationModel> notification) => json.encode(
        notification
            .map<Map<String, dynamic>>((notification) => toMap(notification))
            .toList(),
      );

  static List<NotificationModel> decode(String notifications) =>
      (json.decode(notifications) as List<dynamic>)
          .map<NotificationModel>((item) => NotificationModel.fromJson(item))
          .toList();
  static Map<String, dynamic> toMap(NotificationModel notification) => {
        "id": notification.id,
        "type":notification.type,
        "message":notification.message,
      
         if (notification.user != null) 
      "user": User.toMap(notification.user),
     "sender":notification.sender,
     if (notification.createDates != null) 
      "create_dates": CreateDates.toMap(notification.createDates),
    if (notification.updateDates != null) 
      "update_dates": UpdateDates.toMap(notification.updateDates),
    
      };

}

