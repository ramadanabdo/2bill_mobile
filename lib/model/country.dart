import 'dart:convert';

import 'package:two_bill/model/updateDates.dart';

import 'createDates.dart';

class Country {
  int id;
  String code;
  String name;
  String nameAr;
  String nameEn;
  Null photo;
  CreateDates createDates;
  UpdateDates updateDates;

  Country(
      {this.id,
      this.code,
      this.name,
      this.nameAr,
      this.nameEn,
      this.photo,
      this.createDates,
      this.updateDates});

  Country.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    name = json['name'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    photo = json['photo'];
    createDates = json['create_dates'] != null
        ? new CreateDates.fromJson(json['create_dates'])
        : null;
    updateDates = json['update_dates'] != null
        ? new UpdateDates.fromJson(json['update_dates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['name'] = this.name;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['photo'] = this.photo;
    if (this.createDates != null) {
      data['create_dates'] = this.createDates.toJson();
    }
    if (this.updateDates != null) {
      data['update_dates'] = this.updateDates.toJson();
    }
    return data;
  }

  static String encode(List<Country> countries) => json.encode(
        countries
            .map<Map<String, dynamic>>((country) => toMap(country))
            .toList(),
      );

  static List<Country> decode(String countries) =>
      (json.decode(countries) as List<dynamic>)
          .map<Country>((item) => Country.fromJson(item))
          .toList();
  static Map<String, dynamic> toMap(Country country) => {
        "id": country.id,
        "code": country.code,
        "name": country.name,
        "name_ar": country.nameAr,
        "name_en": country.nameEn,
        "photo": country.photo,
        if (country.createDates != null) 
      "create_dates": CreateDates.toMap(country.createDates),
    
    if (country.updateDates != null) 
      "update_dates": UpdateDates.toMap(country.updateDates),
    
      };
}
