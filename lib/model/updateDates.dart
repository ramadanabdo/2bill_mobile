import 'dart:convert';

class UpdateDates {
  String updatedAtHuman;
  String updatedAt;

  UpdateDates({this.updatedAtHuman, this.updatedAt});

  UpdateDates.fromJson(Map<String, dynamic> json) {
    updatedAtHuman = json['updated_at_human'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['updated_at_human'] = this.updatedAtHuman;
    data['updated_at'] = this.updatedAt;
    return data;
  }
   static String encode(List<UpdateDates> updateDatess) => json.encode(
        updateDatess
            .map<Map<String, dynamic>>((updateDates) => toMap(updateDates))
            .toList(),
      );

  static List<UpdateDates> decode(String updateDatess) =>
      (json.decode(updateDatess) as List<dynamic>)
          .map<UpdateDates>((item) => UpdateDates.fromJson(item))
          .toList();
         static Map<String, dynamic> toMap(UpdateDates updateDates) => {
           
        'updated_at_human': updateDates.updatedAtHuman,
        'updated_at': updateDates.updatedAt,
      
      };
      
}