 
import 'dart:convert';
import 'dart:developer';

import 'package:two_bill/model/service.dart';
import 'package:two_bill/model/updateDates.dart';

import 'createDates.dart';

class Provider {
  int id;
  String name;
  String nameAr;
  String nameEn;
  String logo;
  CreateDates createDates;
  UpdateDates updateDates;

  Provider(
      {this.id,
      this.name,
      this.nameAr,
      this.nameEn,
      this.logo,
      this.createDates,
      this.updateDates});

  Provider.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    logo = json['logo'];
    createDates = json['create_dates'] != null
        ? new CreateDates.fromJson(json['create_dates'])
        : null;
    updateDates = json['update_dates'] != null
        ? new UpdateDates.fromJson(json['update_dates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['logo'] = this.logo;
    if (this.createDates != null) {
      data['create_dates'] = this.createDates.toJson();
    }
    if (this.updateDates != null) {
      data['update_dates'] = this.updateDates.toJson();
    }
    return data;
  }
   static String encode(List<Provider> providers) => json.encode(
        providers
            .map<Map<String, dynamic>>((provider) => toMap(provider))
            .toList(),
      );

  static List<Provider> decode(String providers) =>
      (json.decode(providers) as List<dynamic>)
          .map<Provider>((item) => Provider.fromJson(item))
          .toList();
  static Map<String, dynamic> toMap(Provider provider) => {
        "id": provider.id,
        "name": provider.name,
        "name_ar": provider.nameAr,
        "name_en": provider.nameEn,
        "logo": provider.logo,
        if (provider.createDates != null) 
      "create_dates": CreateDates.toMap(provider.createDates),
    
    if (provider.updateDates != null) 
      "update_dates": UpdateDates.toMap(provider.updateDates),
    
      };
}
