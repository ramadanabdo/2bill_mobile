import 'dart:convert';

import 'package:two_bill/model/updateDates.dart';
import 'package:two_bill/model/user.dart';

import 'createDates.dart';
import 'package.dart';

class Favorit {
  int id;
  Package package;
  User user;
  CreateDates createDates;
  UpdateDates updateDates;

  Favorit(
      {this.id, this.package, this.user, this.createDates, this.updateDates});

  Favorit.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    package =
        json['package'] != null ? new Package.fromJson(json['package']) : null;
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    createDates = json['create_dates'] != null
        ? new CreateDates.fromJson(json['create_dates'])
        : null;
    updateDates = json['update_dates'] != null
        ? new UpdateDates.fromJson(json['update_dates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.package != null) {
      data['package'] = this.package.toJson();
    }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.createDates != null) {
      data['create_dates'] = this.createDates.toJson();
    }
    if (this.updateDates != null) {
      data['update_dates'] = this.updateDates.toJson();
    }
    return data;
  }
   static String encode(List<Favorit> favorits) => json.encode(
        favorits
            .map<Map<String, dynamic>>((Favorit) => toMap(Favorit))
            .toList(),
      );

  static List<Favorit> decode(String favorits) =>
      (json.decode(favorits) as List<dynamic>)
          .map<Favorit>((item) => Favorit.fromJson(item))
          .toList();
  static Map<String, dynamic> toMap(Favorit favorit) => {
        "id": favorit.id,
      
         if (favorit.package != null) 
      "package": Package.toMap(favorit.package),
        if (favorit.user != null) 
      "user": User.toMap(favorit.user),
     if (favorit.createDates != null) 
      "create_dates": CreateDates.toMap(favorit.createDates),
    if (favorit.updateDates != null) 
      "update_dates": UpdateDates.toMap(favorit.updateDates),
    
      };
}