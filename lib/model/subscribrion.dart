import 'dart:convert';

import 'package:two_bill/model/updateDates.dart';
import 'package:two_bill/model/user.dart';

import 'createDates.dart';
import 'package.dart';

class Subscription {
  int id;
  String endDate;
  String endPermited;
  String phone;
  String promocode;
  int isPaid;
  int active;
  String transfer;
  User user;
  User parent;
  Subscription parentSubscription;
  Package package;
  CreateDates createDates;
  UpdateDates updateDates;

  Subscription(
      {this.id,
      this.endDate,
      this.endPermited,
      this.phone,
      this.promocode,
      this.isPaid,
      this.active,
      this.transfer,
      this.user,
      this.parent,
      this.parentSubscription,
      this.package,
      this.createDates,
      this.updateDates});

  Subscription.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    endDate = json['end_date'];
    endPermited = json['end_permited'];
    phone = json['phone'];
    promocode = json['promocode'];
    isPaid = int.parse(json['is_paid']);
    active = int.parse(json['active']);
     transfer = json['transfer'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    parent = json['parent']!=null?new User.fromJson(json['parent']) : null;
    parentSubscription = json['parentSubscription']!=null ?new Subscription.fromJson(json['parentSubscription']) : null;
    package =
        json['package'] != null ? new Package.fromJson(json['package']) : null;
    createDates = json['create_dates'] != null
        ? new CreateDates.fromJson(json['create_dates'])
        : null;
    updateDates = json['update_dates'] != null
        ? new UpdateDates.fromJson(json['update_dates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['end_date'] = this.endDate;
    data['end_permited'] = this.endPermited;
    data['phone'] = this.phone;
    data['promocode'] = this.promocode;
    data['is_paid'] = this.isPaid;
    data['active'] = this.active;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
     if (this.parent != null) {
      data['parent'] = this.parent.toJson();
    }
     if (this.parentSubscription != null) {
      data['parentSubscription'] = this.parentSubscription.toJson();
    }
    if (this.package != null) {
      data['package'] = this.package.toJson();
    }
    if (this.createDates != null) {
      data['create_dates'] = this.createDates.toJson();
    }
    if (this.updateDates != null) {
      data['update_dates'] = this.updateDates.toJson();
    }
    return data;
  }
    static String encode(List<Subscription> subscriptions) => json.encode(
        subscriptions
            .map<Map<String, dynamic>>((subscription) => toMap(subscription))
            .toList(),
      );

  static List<Subscription> decode(String subscriptions) =>
      (json.decode(subscriptions) as List<dynamic>)
          .map<Subscription>((item) => Subscription.fromJson(item))
          .toList();
  static Map<String, dynamic> toMap(Subscription subscription) => {
        "id": subscription.id,
         'end_date':subscription.endDate,
         'end_permited': subscription.endPermited,
         'phone':subscription.phone,
         'promocode':subscription.promocode,
         'is_paid': subscription.isPaid,
         'active':subscription.active,
           if (subscription.user != null) 
      "user": User.toMap(subscription.user),
        if (subscription.parent != null) 
      "parent": User.toMap(subscription.parent),
      if (subscription.parentSubscription != null) 
      "parentSubscription": Subscription.toMap(subscription.parentSubscription),
    
     

         if (subscription.package != null) 
      "package": Package.toMap(subscription.package),
      
     if (subscription.createDates != null) 
      "create_dates": CreateDates.toMap(subscription.createDates),
    if (subscription.updateDates != null) 
      "update_dates": UpdateDates.toMap(subscription.updateDates),
    
      };
}
