

import 'dart:convert';

import 'package:two_bill/model/createDates.dart';
import 'package:two_bill/model/service.dart';
import 'package:two_bill/model/updateDates.dart';

class Package {
  int id;
  String name;
  String nameAr;
  String nameEn;
  String priceBeforeDiscount;
  String priceAfterDiscount;
  String discountPercentage;
  String points;
  String firstHeadlineAr;
  String firstHeadlineEn;
  String firstHeadline;
  String secondHeadlineAr;
  String secondHeadlineEn;
  String secondHeadline;
  String descriptionAr;
  String descriptionEn;
  String description;
  String permitDays;
  String type;
  String couponValue;
  String logo;
  ServiceProvider service;
  CreateDates createDates;
  UpdateDates updateDates;

  Package(
      {this.id,
      this.name,
      this.nameAr,
      this.nameEn,
      this.priceBeforeDiscount,
      this.priceAfterDiscount,
      this.discountPercentage,
      this.points,
      this.firstHeadlineAr,
      this.firstHeadlineEn,
      this.firstHeadline,
      this.secondHeadlineAr,
      this.secondHeadlineEn,
      this.secondHeadline,
      this.descriptionAr,
      this.descriptionEn,
      this.description,
      this.permitDays,
      this.type,
      this.couponValue,
      this.logo,
      this.service,
      this.createDates,
      this.updateDates});

  Package.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    priceBeforeDiscount = json['priceBeforeDiscount'];
    priceAfterDiscount = json['priceAfterDiscount'];
    discountPercentage = json['discountPercentage'];
    points = json['points'];
    firstHeadlineAr = json['first_headline_ar'];
    firstHeadlineEn = json['first_headline_en'];
    firstHeadline = json['firstHeadline'];
    secondHeadlineAr = json['second_headline_ar'];
    secondHeadlineEn = json['second_headline_en'];
    secondHeadline = json['secondHeadline'];
    descriptionAr = json['description_ar'];
    descriptionEn = json['description_en'];
    description = json['description'];
    permitDays = json['permitDays'];
    type = json['type'];
    couponValue = json['couponValue'];
    logo = json['logo'];
    service =
        json['service'] != null ? new ServiceProvider.fromJson(json['service']) : null;
    createDates = json['create_dates'] != null
        ? new CreateDates.fromJson(json['create_dates'])
        : null;
    updateDates = json['update_dates'] != null
        ? new UpdateDates.fromJson(json['update_dates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['priceBeforeDiscount'] = this.priceBeforeDiscount;
    data['priceAfterDiscount'] = this.priceAfterDiscount;
    data['discountPercentage'] = this.discountPercentage;
    data['points'] = this.points;
    data['first_headline_ar'] = this.firstHeadlineAr;
    data['first_headline_en'] = this.firstHeadlineEn;
    data['firstHeadline'] = this.firstHeadline;
    data['second_headline_ar'] = this.secondHeadlineAr;
    data['second_headline_en'] = this.secondHeadlineEn;
    data['secondHeadline'] = this.secondHeadline;
    data['description_ar'] = this.descriptionAr;
    data['description_en'] = this.descriptionEn;
    data['description'] = this.description;
    data['permitDays'] = this.permitDays;
    data['type'] = this.type;
    data['couponValue'] = this.couponValue;
    data['logo'] = this.logo;
    if (this.service != null) {
      data['service'] = this.service.toJson();
    }
    if (this.createDates != null) {
      data['create_dates'] = this.createDates.toJson();
    }
    if (this.updateDates != null) {
      data['update_dates'] = this.updateDates.toJson();
    }
    return data;
  }

   static String encode(List<Package> packages) => json.encode(
        packages
            .map<Map<String, dynamic>>((package) => toMap(package))
            .toList(),
      );

  static List<Package> decode(String packages) =>
      (json.decode(packages) as List<dynamic>)
          .map<Package>((item) => Package.fromJson(item))
          .toList();
  static Map<String, dynamic> toMap(Package package) => {
        "id": package.id,
        "name": package.name,
        "name_ar": package.nameAr,
        "name_en": package.nameEn,
        "priceBeforeDiscount":package.priceBeforeDiscount,
        "priceAfterDiscount":package.priceAfterDiscount,
        "discountPercentage":package.discountPercentage,
        "first_headline_ar":package.firstHeadlineAr,
        "first_headline_en":package.firstHeadlineEn,
        "firstHeadline":package.firstHeadline,
        "second_headline_ar":package.secondHeadlineAr,
        "second_headline_en":package.secondHeadlineEn,
        "secondHeadline":package.secondHeadline,
        "description_ar":package.descriptionAr,
        "description_en":package.descriptionEn,
        "description":package.description,
       

        "permitDays":package.permitDays,
        "type":package.type,
        "couponValue":package.couponValue,
        "logo": package.logo,
         if (package.service != null) 
      "service": ServiceProvider.toMap(package.service),
        if (package.createDates != null) 
      "create_dates": CreateDates.toMap(package.createDates),
    
    if (package.updateDates != null) 
      "update_dates": UpdateDates.toMap(package.updateDates),
    
      };


}