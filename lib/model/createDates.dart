import 'dart:convert';

class CreateDates {
  String createdAtHuman;
  String createdAt;

  CreateDates({this.createdAtHuman, this.createdAt});

  CreateDates.fromJson(Map<String, dynamic> json) {
    createdAtHuman = json['created_at_human'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at_human'] = this.createdAtHuman;
    data['created_at'] = this.createdAt;
    return data;
  }
   static String encode(List<CreateDates> createDatess) => json.encode(
        createDatess
            .map<Map<String, dynamic>>((createDates) => toMap(createDates))
            .toList(),
      );

  static List<CreateDates> decode(String createDatess) =>
      (json.decode(createDatess) as List<dynamic>)
          .map<CreateDates>((item) => CreateDates.fromJson(item))
          .toList();
         static Map<String, dynamic> toMap(CreateDates createDates) => {
           
        'created_at_human': createDates.createdAtHuman,
        'created_at': createDates.createdAt,
      
      };
}