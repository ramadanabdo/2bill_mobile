import 'package:two_bill/model/updateDates.dart';

import 'createDates.dart';

class User {
  int id;
  String name;
  String email;
  String phone;
  String type;
  String gender;
  String birthDate;
  Null logo;
  Country country;
  CreateDates createDates;
  UpdateDates updateDates;

  User(
      {this.id,
      this.name,
      this.email,
      this.phone,
      this.type,
      this.gender,
      this.birthDate,
      this.logo,
      this.country,
      this.createDates,
      this.updateDates});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    type = json['type'];
    gender = json['gender'];
    birthDate = json['birthDate'];
    logo = json['logo'];
   country =
        json['country'] != null ? new Country.fromJson(json['country']) : null;
    createDates = json['create_dates'] != null
        ? new CreateDates.fromJson(json['create_dates'])
        : null;
    updateDates = json['update_dates'] != null
        ? new UpdateDates.fromJson(json['update_dates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['type'] = this.type;
    data['gender'] = this.gender;
    data['birthDate'] = this.birthDate;
    data['logo'] = this.logo;
   if (this.country != null) {
      data['country'] = this.country.toJson();
    }
    if (this.createDates != null) {
      data['create_dates'] = this.createDates.toJson();
    }
    if (this.updateDates != null) {
      data['update_dates'] = this.updateDates.toJson();
    }
    return data;
  }
    static Map<String, dynamic> toMap(User user) => {
       'id' : user.id,
    'name' :user.name,
    'email' : user.email,
    'phone' : user.phone,
    'type': user.type,
    'gender':user.gender,
    'birthDate': user.birthDate,
    'logo' :user.logo,
      if (user.createDates != null) 
      "create_dates": CreateDates.toMap(user.createDates),
    
    if (user.updateDates != null) 
      "update_dates": UpdateDates.toMap(user.updateDates),
    };
}
class Country {
  int id;
  String nameAr;
  String nameEn;
  Null logo;
  String name;
  Null photo;

  Country(
      {this.id, this.nameAr, this.nameEn, this.logo, this.name, this.photo});

  Country.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    logo = json['logo'];
    name = json['name'];
    photo = json['photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['logo'] = this.logo;
    data['name'] = this.name;
    data['photo'] = this.photo;
    return data;
  }
}
