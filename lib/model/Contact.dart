import 'package:two_bill/model/updateDates.dart';

import 'createDates.dart';

class Contact {
  int id;
  String name;
  String nameAr;
  String nameEn;
  String phone;
  String email;
  String phoneMessage;
  String emailMessage;
  String logo;
  var appStoreLink;
  var googlePlayLink;
  CreateDates createDates;
  UpdateDates updateDates;

  Contact(
      {this.id,
      this.name,
      this.nameAr,
      this.nameEn,
       this.appStoreLink,
      this.googlePlayLink,
      this.phone,
      this.email,
      this.phoneMessage,
      this.emailMessage,
      this.logo,
      this.createDates,
      this.updateDates});

  Contact.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    appStoreLink = json['appStore_link'];
    googlePlayLink = json['googlePlay_link'];
    phone = json['phone'];
    email = json['email'];
    phoneMessage = json['phone_message'];
    emailMessage = json['email_message'];
    logo = json['logo'];
    createDates = json['create_dates'] != null
        ? new CreateDates.fromJson(json['create_dates'])
        : null;
    updateDates = json['update_dates'] != null
        ? new UpdateDates.fromJson(json['update_dates'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['phone_message'] = this.phoneMessage;
    data['email_message'] = this.emailMessage;
    data['logo'] = this.logo;
    if (this.createDates != null) {
      data['create_dates'] = this.createDates.toJson();
    }
    if (this.updateDates != null) {
      data['update_dates'] = this.updateDates.toJson();
    }
    return data;
  }
}