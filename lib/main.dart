import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:two_bill/routes.dart';

import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/helper.dart';



import 'utils/app_translations_delegate.dart';
import 'utils/application.dart';

void main() {
  
   WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
  
 
}

class _MyAppState extends State<MyApp> {
 AppTranslationsDelegate _newLocaleDelegate;
  String dir="";
  Helper helper;

  bool route = false;
  getUser() async {
   helper=new Helper();

  }
  

   @override
 initState() {
    super.initState();
    
   _newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
    application.onLocaleChanged = onLocaleChange;
    // getUser();
    
  }
   
  
  @override
  Widget build(BuildContext context) {
  
    
    return new MaterialApp(
      title: '2BILL',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        primarySwatch: Colors.cyan,
      ),
        builder: (BuildContext context, Widget child) {
          return new Directionality(
            textDirection: AppTranslations.of(context).text("and") == "and"
                ? TextDirection.ltr
                : TextDirection.rtl,
              
           child: new Builder(
              builder: (BuildContext context) {
                return new MediaQuery(
                  data: MediaQuery.of(context).copyWith(
                    textScaleFactor: 1.0,
                  ),
                  child: child,
                );
              },
            ),
              );
        
       },
      
  //  initialRoute: "/Intro",
     routes: routes,
      localizationsDelegates: [
        _newLocaleDelegate,
       const AppTranslationsDelegate(),
       
       
        GlobalMaterialLocalizations.delegate,
      
        GlobalWidgetsLocalizations.delegate,
      ],
     
       supportedLocales: application.supportedLocales(),
  
    );
        
     
  }

void onLocaleChange(Locale locale) {
    setState(() {
      dir=locale.languageCode;
      print(dir);
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }
}
