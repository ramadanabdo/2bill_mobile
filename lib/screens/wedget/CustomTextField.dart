import 'package:flutter/material.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/validators/email_validator.dart';
import 'package:two_bill/validators/password_validator.dart';
import 'package:two_bill/validators/phone_validator.dart';

// ignore: must_be_immutable
class CustomTextField extends StatelessWidget {
  final String hint;
  final String icon;
  final bool color;
  final int mainlan;
  final Function onclick;
  final String initvalue;
  final bool isNumber;
  final bool isEmail;
  final bool issecure;
  final bool isPhone;
  final bool mainLength;
  final bool minLength;
  const CustomTextField(
      {@required this.hint,
      @required this.onclick,
      this.initvalue,
      this.isNumber = false,
      this.isEmail = false,
      this.issecure = false,
      this.mainlan=1, 
      this.isPhone=false,
      this.mainLength=false,
      this.minLength=false,
      this.icon, this.color=false});
   
    
  // ignore: missing_return

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

   return TextFormField(
        obscureText: issecure ? true : false,
        onSaved: onclick,
        initialValue: initvalue,
        maxLength: mainLength?1:null,
      
        maxLines: mainlan,
        keyboardType: isEmail
            ? TextInputType.emailAddress
            : (isNumber ? TextInputType.number : TextInputType.text),
        validator: (value) {
          if (value.isEmpty) {
            return AppTranslations.of(context).text("value is empty");
            
          }
          if(issecure){
            if(value.length<8){
               return AppTranslations.of(context).text("Password contain at least 8 letters");
            }
          }
          if(minLength){
             if(value.length<10){
               return AppTranslations.of(context).text("Message contain at least 10 letters");
            }
          }
          if (isEmail) {
            if (!value.contains(EmailValidator.regex)) {
              return AppTranslations.of(context).text("enter_valid_mail");
            }
          }
          if(isPhone){
           if (!value.contains(PhoneValidator.regex)) {
              return AppTranslations.of(context).text("enter_valid_phone");
            } 
          }
        },
        decoration: InputDecoration(
          hintText: hint,
          prefixIcon:getImage()!=null?Image.asset(getImage(),):null,
          fillColor: Colors.white,
          filled: true,
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(
                color:color ?Color(0xff787878):Colors.white,
              )),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(
               color:color ?Color(0xff787878):Colors.white,
              )),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(
                color:color ?Color(0xff787878):Colors.white,
              )),
        ),
     
    );
  }
  String getImage(){
    if(icon==null){
      return null;
    }
    else if(icon=="phone2"){
      return "assets/image/phone2.png";
    }
    else if(icon=="phoneAr"){
       return "assets/image/phoneAr.png";
    }
    else if(icon=="passwordImage"){
      return "assets/image/password.png";
    }

  }
}
