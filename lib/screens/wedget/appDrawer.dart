import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:two_bill/model/user.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/application.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/style.dart';

import 'CustemLogo.dart';
class AppDrawer extends StatefulWidget {
    final GlobalKey<ScaffoldState> scafoldkey;
  AppDrawer({Key key, @required this.scafoldkey}) : super(key: key);
  @override
  _AppDrawer createState() => _AppDrawer(scafoldkey);
}

class _AppDrawer extends State<AppDrawer> {
  final GlobalKey<ScaffoldState> scafoldkey;
  _AppDrawer(this.scafoldkey);
  String username='';
  String phone='';
   Helper helper=new Helper();
    LocalLogin localLogin=new LocalLogin();
User user;
 loadData() async {
   helper=new Helper();
   await helper.getsharedpref("user").then((dynamic muser){
     setState(() {
       print(muser);
      user=User.fromJson(jsonDecode(muser));
      username=user.name;
      phone=user.phone;

     });
    
   });
  
  }

  @override
  initState() {
    super.initState();
    loadData();
  }


String getLang(BuildContext context){
  if(AppTranslations.of(context).text("Settings")=="Settings")
   return "عربي";
   else
   return "English";
}
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          UserAccountsDrawerHeader(
              decoration: new BoxDecoration(
              gradient: new LinearGradient(
                colors: [Color(0xFF3DBCA7), Color(0xFF007AE7)],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(1.0, 0.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp,
              ),
            ),
              accountName:
                            Padding(
                padding: EdgeInsets.only(top:20),
                                                            child: Text(
                username,
               
                style: TextStyle(
      color: Colors.white, fontSize: 20, fontWeight: FontWeight.w700,fontFamily: 'Monstserrat'),

            
              ),
                             ),
              accountEmail:  Padding(
                padding: EdgeInsets.only(top:5),
                              child: Text(
          phone,
             
              style: TextStyle(
      color: Colors.white, fontSize: 15, fontWeight: FontWeight.w600,fontFamily: 'Monstserrat'),

            ),
              ),
              currentAccountPicture: CircleAvatar(

                radius: 50,
                backgroundColor:
                  Colors.transparent,
                       
                
                  child:AppTranslations.of(context).text("and")=="and"? Image.asset(logo,width: 50,):Image.asset(logoAr,width: 50,)),
              
          ),
       

          Column(
            children: [
              SizedBox(
                height: 10.0,
              ),
             
              Card(
               
                  child: ListTile(
                    leading: Icon(Icons.language, color:Color(0xff465c85)),
                    title: Text(
                     getLang(context),
                      style: TextStyle(color: Color(0xff6b6b6b)),
                    ),
                    onTap: () {
                   _changeLanguage(getLang(context));
                  },
                  ),
                 
                 
              ),
              SizedBox(
                height: 5.0,
              ),
              Card(
                child: ListTile(
                  leading: Icon(
                      Icons.lock,
                      color: Color(0xff465c85)), 
                  title: Text(AppTranslations.of(context).text("Change Password"),
                      style: TextStyle(color:  Color(0xff6b6b6b))),

                  onTap: () {
                    Navigator.pushNamed(
                        context, '/changePassword');
                  },
                ),
              ),
               Card(
                child: ListTile(
                  leading:Icon(Icons.person,  color: Color(0xff465c85)),
                  title: Text(AppTranslations.of(context).text("My Profile"),
                      style: TextStyle(color:  Color(0xff6b6b6b))),

                  onTap: () {
                    Navigator.pushNamed(
                        context, '/MyProfile');
                  },
                ),
              ),
               Card(
                child: ListTile(
                  
                  leading: Image.asset(dollar,width: 25,  color: Color(0xff465c85)),
                  title: Text(AppTranslations.of(context).text("My Packages"),
                      style: TextStyle(color:Color(0xff6b6b6b))),

                  onTap: () {
                    Navigator.pushNamed(
                        context, '/MyPackages');
                  },
                ),
              ),
              
              Card(
                child: ListTile(
                  leading:Icon(Icons.info,  color: Color(0xff465c85)),
                  title: Text(
                    AppTranslations.of(context).text("About"),
                    style: TextStyle(color: Color(0xff6b6b6b)),
                  ),
                  onTap: () {
                     
                    Navigator.pushNamed(
                        context, '/about');
                  },
                ),
              ),
               Card(
                child: ListTile(
                  leading:Icon(Icons.notifications,  color: Color(0xff465c85)),
                  title: Text(
                    AppTranslations.of(context).text("Notification"),
                    style: TextStyle(color: Color(0xff6b6b6b)),
                  ),
                  onTap: () {
                     
                    Navigator.pushNamed(
                        context, '/notification');
                  },
                ),
              ),
               Card(
                child: ListTile(
                  leading:Icon(Icons.help,  color: Color(0xff465c85)),
                  title: Text(
                    AppTranslations.of(context).text("Help & Support"),
                    style: TextStyle(color: Color(0xff6b6b6b)),
                  ),
                  onTap: () {
                    Navigator.pushNamed(
                        context, '/contact');
                  },
                ),
              ), Card(
                child: ListTile(
                  leading: Icon(
                      Icons.contact_mail,
                      color: Color(0xff465c85)), 
                  title: Text(AppTranslations.of(context).text("Contact Us"),
                      style: TextStyle(color:  Color(0xff6b6b6b))),

                  onTap: () {
                   Navigator.pushNamed(context,'/sendMail');
                  },
                ),
              ),
              Card(
                child: ListTile(
                  leading:Icon(Icons.exit_to_app,  color: Color(0xff465c85)), //new Image.asset('assets/image/.png'),
                  title: Text(AppTranslations.of(context).text("Logout"),
                      style: TextStyle(color: Color(0xff6b6b6b))),

                  onTap: () async {
                    
                    await helper.removeToken();
                    await localLogin.clearFile();
                      print('cleared');
                Navigator.pushNamedAndRemoveUntil(
                  context, '/SignIn', (_) => false);
                  }
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

 

  void _changeLanguage(lang) {
     if(lang=="عربي")
  lang="Arabic";
    print(lang);
    Locale locale;
    switch (lang) {
      case "English":
        locale = Locale('en');
        application.onLocaleChanged(Locale('en'));
        break;
      case "Arabic":
        locale = Locale('ar');
        application.onLocaleChanged(Locale('ar'));
        break;
      default:
        locale = Locale('en');
        application.onLocaleChanged(Locale('en'));
    }
  }


}
