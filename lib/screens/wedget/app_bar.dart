import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/utils/helper.dart';

import '../../utils/app_translations.dart';
import '../../utils/colors.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final String title2;
   bool hasarowBack ;
  bool drwer;

  final GlobalKey<ScaffoldState> scafoldkey;
  @override
  final Size preferredSize;

  CustomAppBar({
    this.title2,
    this.title,
    this.scafoldkey,
    this.hasarowBack = true,
    this.drwer=true,
    Key key,
  })  : preferredSize = Size.fromHeight(50.0),
        super(key: key);

  checkArowBack(BuildContext context) {
    print('hasArrowBack $hasarowBack');
    if (hasarowBack) {
      return InkWell(
        child: Icon(
          Icons.arrow_back_ios,
          color: Colors.white,
        ),
        onTap: () => {
          Navigator.of(context).pop(),
        },
      );
    } else
      return Container();
  }

 
     
   

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return AppBar(
      backgroundColor: appPrimaryColor,
      title: Center(
        child: title2 == null
            ? Text(
                AppTranslations.of(context).text(title),
                style: TextStyle(color: Colors.white),
              )
            : Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Text(
                  title2,
                  style: TextStyle(color: Colors.white),
                ),
                SizedBox(
                  width: width * .01,
                ),
                Text(
                  AppTranslations.of(context).text(title),
                  style: TextStyle(color: Colors.white),
                ),
              ]),
      ),
      leading: checkArowBack(context),
      actions: <Widget>[
        // ignore: unrelated_type_equality_checks
      drwer?IconButton(
        icon: Icon(Icons.menu, color: Colors.white),
        onPressed: () {
          print('object');
          scafoldkey.currentState.openDrawer();
        },
      ):Container(),
      ],
    );
  }

 
}
