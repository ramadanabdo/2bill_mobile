import 'package:flutter/material.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';


circularProgress() {
  return Container(
    alignment: Alignment.center,
    padding: EdgeInsets.only(top: 12.0),
    child: CircularProgressIndicator(backgroundColor: Colors.black,),
   
  );
}









linearProgress() {
  return Container(
    alignment: Alignment.center,
    padding: EdgeInsets.only(top: 12.0),
    child: LinearProgressIndicator(valueColor: AlwaysStoppedAnimation(Colors.lightGreenAccent),),
  );
}
 Widget customBottomNavigationBar(BuildContext context,double height,double width,GlobalKey<ScaffoldState> _scaffoldKey){
   return
         Theme(
      data: Theme.of(context).copyWith(
         
          canvasColor: appPrimaryColor,
         
          primaryColor: Colors.green,
          textTheme: Theme
              .of(context)
              .textTheme
              .copyWith(caption: new TextStyle(color: Colors.white))), 
           child:   BottomAppBar(
          
            child:   AppTranslations.of(context).text('and')=="and"?Container(
               height: 50,
              color: appPrimaryColor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                     InkWell(
                      onTap: (){
                        Navigator.pushNamed(context,"/home");
                      },
                      child: Padding(padding: EdgeInsets.all(3),

                                  child:
                                   Column(
                                     mainAxisAlignment: MainAxisAlignment.center,
                                     crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                                  Icon(Icons.home,color: Colors.white,size: 25,),
                                
                                  Text(AppTranslations.of(context).text("Home"),style: TextStyle(
                                     color:Colors.white,
                                   ),),
                        ]),
                        
                      )
                        
                                
                    ),


                     InkWell(
                      onTap: (){
                         Navigator.pushNamed(context,"/MyPackages");
                      },
                      child: Padding(padding: EdgeInsets.all(3),

                                  child:
                                   Column(
                                     mainAxisAlignment: MainAxisAlignment.center,
                                     crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                                  Image.asset(dollar,width: 25,),
                                
                                  Text(AppTranslations.of(context).text("My Packages"),style: TextStyle(
                                     color:Colors.white,
                                   ),),
                        ]),

                       
                      )
                                  
                                
                    ),


                     InkWell(
                      onTap: (){
                       Navigator.pushNamed(context,"/MyFavorit");
                      },
                      child: Padding(padding: EdgeInsets.all(3),

                                  child:
                                   Column(
                                     mainAxisAlignment: MainAxisAlignment.center,
                                     crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                                Image.asset(add,width: 25,),
                                  
                                   Text(AppTranslations.of(context).text("Favourite"),style: TextStyle(
                                     color:Colors.white,
                                   ),),
                        ]
                                   )
                      )
                       
                      ),
                                
                  

                     InkWell(
                      onTap: (){
                     _scaffoldKey.currentState.openDrawer();
                      },
                      child:
                              Padding(padding: EdgeInsets.all(3),

                                  child:
                                   Column(
                                     mainAxisAlignment: MainAxisAlignment.center,
                                     crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(settings,width: 25,),
                                  
                                   Text(AppTranslations.of(context).text("Settings"),style: TextStyle(
                                     color:Colors.white,
                                   ),),
                                    ],
                      ),
                       
                                
                    ),

                     ),
                    
                  ],
                ),
              
               ):Container(
               height: 55,
              color: appPrimaryColor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                     InkWell(
                      onTap: (){
                        Navigator.pushNamed(context,"/home");
                      },
                      child: Padding(padding: EdgeInsets.all(3),

                                  child:
                                   Column(
                                     mainAxisAlignment: MainAxisAlignment.center,
                                     crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                                  Icon(Icons.home,color: Colors.white,size: 25,),
                                
                                  Text(AppTranslations.of(context).text("Home"),style: TextStyle(
                                     color:Colors.white,
                                   ),),
                        ]),
                        
                      )
                        
                                
                    ),


                     InkWell(
                      onTap: (){
                         Navigator.pushNamed(context,"/MyPackages");
                      },
                      child: Padding(padding: EdgeInsets.all(3),

                                  child:
                                   Column(
                                     mainAxisAlignment: MainAxisAlignment.center,
                                     crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                                  Image.asset(dollar,width: 25,),
                                
                                  Text(AppTranslations.of(context).text("My Packages"),style: TextStyle(
                                     color:Colors.white,
                                   ),),
                        ]),

                       
                      )
                                  
                                
                    ),


                     InkWell(
                      onTap: (){
                       Navigator.pushNamed(context,"/MyFavorit");
                      },
                      child: Padding(padding: EdgeInsets.all(3),

                                  child:
                                   Column(
                                     mainAxisAlignment: MainAxisAlignment.center,
                                     crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                                Image.asset(add,width: 25,),
                                  
                                   Text(AppTranslations.of(context).text("Favourite"),style: TextStyle(
                                     color:Colors.white,
                                   ),),
                        ]
                                   )
                      )
                       
                      ),
                                
                  

                     InkWell(
                      onTap: (){
                     _scaffoldKey.currentState.openDrawer();
                      },
                      child:
                              Padding(padding: EdgeInsets.all(3),

                                  child:
                                   Column(
                                     mainAxisAlignment: MainAxisAlignment.center,
                                     crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset(settings,width: 25,),
                                  
                                   Text(AppTranslations.of(context).text("Settings"),style: TextStyle(
                                     color:Colors.white,
                                   ),),
                                    ],
                      ),
                       
                                
                    ),

                     ),
                    
                  ],
                ),
              
               ),

      ),
     
  
   );
 }
showErrorDialog(_ctx,text) {
   
    showDialog(
        context: _ctx,
        builder: (BuildContext context) {
          return AlertDialog(
            title: null,
            content: Text(
              AppTranslations.of(context).text(text),
              
              textAlign: TextAlign.start,
              style: TextStyle(fontSize: 15),
            ),
            actions: [
              FlatButton(
                child: Text(
                  AppTranslations.of(context).text("Ok"),
                ),
                onPressed: () {
                         Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) {
      if(value)Navigator.of(context).pop();
    
    });
                  
                },
              )
            ],
          );
        });
  }
