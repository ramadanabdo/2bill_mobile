import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:two_bill/utils/app_translations.dart';

showAlert(BuildContext context, String text, String type,String routeName) {

  if (type == "error") {
   
        Alert(
      context: context,
      type: AlertType.error,
      title:  AppTranslations.of(context).text("and") == "and" ? "Error" : "خطا",
      desc: AppTranslations.of(context).text("and")=="and"?text:text,
      buttons: [
        DialogButton(
          child: Text(
            "Ok",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
  }
  if (type == "success") {
   Alert(
      context: context,
      type: AlertType.success,
      title:  AppTranslations.of(context).text("and") == "and"
            ? "Success"
            : "تم بنجاح",
      desc: AppTranslations.of(context).text("and")=="and"?text:"نجاح",
      buttons: [
        DialogButton(
          child: Text(
            "Ok",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pushNamed(context, routeName),
          width: 120,
        )
      ],
    ).show();
}
}
 
