import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:two_bill/model/subscribrion.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';

class SuccessSubsciption extends StatefulWidget {
  Subscription subscription;
  SuccessSubsciption({Key key, @required this.subscription}) : super(key: key);
  @override
  _SuccessSubsciption createState() => _SuccessSubsciption(subscription);
}

class _SuccessSubsciption extends State<SuccessSubsciption> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Subscription subscription;
  _SuccessSubsciption(Subscription subscription) {
    this.subscription = subscription;
  }

  String getDate(String date) {
    DateTime parseDate = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(date);
    print(DateFormat('EEEE, d MMM, yyyy').format(parseDate));
    return DateFormat('EEEE, d MMM, yyyy').format(parseDate);
  }
connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  @override
  void initState() {
    super.initState();
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          title: "Service Payment",
          scafoldkey: _scaffoldKey,
          hasarowBack: false,
        ),
        drawer: AppDrawer(
          scafoldkey: _scaffoldKey,
        ),
        bottomNavigationBar:
            customBottomNavigationBar(context, height, width, _scaffoldKey),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.05),
              child: ListView(children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    AppTranslations.of(context).text('and') == "and"
                        ? SizedBox(
                            height: height * .02,
                          )
                        : SizedBox(
                            height: height * .01,
                          ),
                    Text(
                      AppTranslations.of(context).text('Congratulation'),
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                      ),
                    ),
                    AppTranslations.of(context).text('and') == "and"
                        ? SizedBox(
                            height: height * .02,
                          )
                        : SizedBox(
                            height: height * .01,
                          ),
                    Image.asset(success),
                    AppTranslations.of(context).text('and') == "and"
                        ? SizedBox(
                            height: height * .02,
                          )
                        : SizedBox(
                            height: height * .01,
                          ),
                    Text(
                      AppTranslations.of(context)
                          .text('Request confermed successfully'),
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w900,
                        color: Colors.green,
                      ),
                    ),
                    Text(
                      AppTranslations.of(context)
                              .text('your money will be transformed within') +
                          ' ${int.parse(subscription.package.permitDays.toString()) + 10} ' +
                          AppTranslations.of(context).text('days'),
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                    ),
                    Text(
                      AppTranslations.of(context).text('start now to invite') +
                          ' ${subscription.package.points} ' +
                          AppTranslations.of(context)
                              .text('of your friends within') +
                          ' ${subscription.package.permitDays} ' +
                          AppTranslations.of(context).text('days'),
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                    ),
                    Text(
                      AppTranslations.of(context)
                          .text('to benfit from the Discount'),
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                    ),
                    AppTranslations.of(context).text('and') == "and"
                        ? SizedBox(
                            height: height * .02,
                          )
                        : SizedBox(
                            height: height * .01,
                          ),
                    Container(
                      height: 1.0,
                      width: width,
                      color: Color(0xffb9b9b9),
                    ),
                    AppTranslations.of(context).text('and') == "and"
                        ? SizedBox(
                            height: height * .02,
                          )
                        : SizedBox(
                            height: height * .01,
                          ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: width * .02),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppTranslations.of(context).text('you must add') +
                                ' ${subscription.package.points} ' +
                                AppTranslations.of(context)
                                    .text('of your friends within') +
                                ' ${subscription.package.permitDays} ' +
                                AppTranslations.of(context).text('days'),
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Color(0xff3864b9),
                            ),
                          ),
                          AppTranslations.of(context).text('and') == "and"
                              ? SizedBox(
                                  height: height * .02,
                                )
                              : SizedBox(
                                  height: height * .005,
                                ),
                          Row(
                            children: [
                              Image.asset(
                                clender2,
                                color: Color(0xff3864b9),
                              ),
                              SizedBox(
                                width: width * .05,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    getDate(subscription.endPermited)
                                        .toString(),
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.black,
                                    ),
                                  ),
                                  Text(
                                    AppTranslations.of(context)
                                        .text('From 10:00 Am to 10:00 Pm'),
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    AppTranslations.of(context).text('and') == "and"
                        ? SizedBox(
                            height: height * .02,
                          )
                        : SizedBox(
                            height: height * .01,
                          ),
                    Container(
                      height: 1.0,
                      width: width,
                      color: Color(0xffb9b9b9),
                    ),
                    AppTranslations.of(context).text('and') == "and"
                        ? SizedBox(
                            height: height * .02,
                          )
                        : SizedBox(
                            height: height * .01,
                          ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: width * .02),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppTranslations.of(context).text(
                                    'your money will be transformed within') +
                                ' ${int.parse(subscription.package.permitDays.toString()) + 10} ' +
                                AppTranslations.of(context).text('days'),
                            style: TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Color(0xff3864b9),
                            ),
                          ),
                          AppTranslations.of(context).text('and') == "and"
                              ? SizedBox(
                                  height: height * .01,
                                )
                              : SizedBox(
                                  height: height * .005,
                                ),
                          Row(
                            children: [
                              Image.asset(
                                clock,
                                color: Color(0xff3864b9),
                              ),
                              SizedBox(
                                width: width * .05,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    getDate(subscription.endDate).toString(),
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.black,
                                    ),
                                  ),
                                  Text(
                                    AppTranslations.of(context)
                                        .text('From 10:00 Am to 10:00 Pm'),
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.black,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    AppTranslations.of(context).text('and') == "and"
                        ? SizedBox(
                            height: height * .02,
                          )
                        : SizedBox(
                            height: height * .01,
                          ),
                    Container(
                      height: 1.0,
                      width: width,
                      color: Color(0xffb9b9b9),
                    ),
                    AppTranslations.of(context).text('and') == "and"
                        ? SizedBox(
                            height: height * .03,
                          )
                        : SizedBox(
                            height: height * .02,
                          ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                AppTranslations.of(context).text("and") == "and"
                                    ? Text(
                                        subscription.package.firstHeadlineEn,
                                        style: TextStyle(
                                            color: Color(0xff3864b9),
                                            fontSize: 25,
                                            fontWeight: FontWeight.w900),
                                      )
                                    : Text(
                                        subscription.package.firstHeadlineAr,
                                        style: TextStyle(
                                            color: Color(0xff3864b9),
                                            fontSize: 25,
                                            fontWeight: FontWeight.w900),
                                      ),
                                SizedBox(
                                  width: width * .01,
                                ),
                                Text(
                                  AppTranslations.of(context).text("Gigabyte"),
                                  style: TextStyle(
                                      color: Color(0xff3864b9),
                                      fontSize: 25,
                                      fontWeight: FontWeight.w900),
                                ),
                              ],
                            ),
                            AppTranslations.of(context).text('and') == "and"
                                ? SizedBox(
                                    height: height * .02,
                                  )
                                : SizedBox(
                                    height: height * .01,
                                  ),
                            Row(children: [
                              AppTranslations.of(context).text("and") == "and"
                                  ? Text(
                                      subscription.package.nameEn,
                                      style: TextStyle(
                                        color: Color(0xff464646),
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  : Text(
                                      subscription.package.nameAr,
                                      style: TextStyle(
                                        color: Color(0xff464646),
                                        fontSize: 22,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                            ]),
                            AppTranslations.of(context).text("and") == "and"
                                ? Row(
                                    children: [
                                      RichText(
                                        text: new TextSpan(
                                          text:
                                              '${subscription.package.priceBeforeDiscount}EG',
                                          style: new TextStyle(
                                            color: Color(0xff464646),
                                            fontSize: 16,
                                            decoration:
                                                TextDecoration.lineThrough,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: width * .05,
                                      ),
                                      Text(
                                          '${subscription.package.priceAfterDiscount}EG/${subscription.package.type}',
                                          style: TextStyle(
                                            color: Colors.green,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                          )),
                                      SizedBox(
                                        width: width * .04,
                                      ),
                                    ],
                                  )
                                : Row(children: [
                                    RichText(
                                      text: new TextSpan(
                                        text:
                                            '${subscription.package.priceBeforeDiscount}جنيه',
                                        style: new TextStyle(
                                          color: Color(0xff464646),
                                          fontSize: 16,
                                          decoration:
                                              TextDecoration.lineThrough,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: width * .04,
                                    ),
                                    Text(
                                        '${subscription.package.priceAfterDiscount}جنيه/${subscription.package.type}',
                                        style: TextStyle(
                                          color: Color(0xff464646),
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        )),
                                    SizedBox(
                                      width: width * .02,
                                    ),
                                    /* Text(
                                '${subscription.package.discountPercentage}%OF',
                                    style: TextStyle(
                                      color: Colors.green,
                                    ),
                                  ),*/
                                  ]),
                                     AppTranslations.of(context).text("and") != "and"?SizedBox(
                                      height: height * .01,
                                    ):SizedBox(),
                          ]),
                    ),
                  ],
                ),
              ]),
            )));
  }
}
