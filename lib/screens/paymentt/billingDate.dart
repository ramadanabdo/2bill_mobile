import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:two_bill/model/subscribrion.dart';
import 'package:two_bill/screens/wedget/CustomTextField.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/screens/wedget/show_%20SweetAlert.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/utils/style.dart';

import 'creditCard.dart';

class BillingData extends StatefulWidget {
  Subscription subscription;
  String type;
  String promocode;
  String lang;
  BillingData(
      {Key key,
      @required this.promocode,
      @required this.subscription,
      @required this.type,
      @required this.lang})
      : super(key: key);
  @override
  _BillingData createState() =>
      _BillingData(subscription, promocode, type, lang);
}

class _BillingData extends State<BillingData> {
  Subscription subscription;
  String promocode;
  String type;
  String lang;
  _BillingData(
      Subscription subscription, String promocode, String type, String lang) {
    this.subscription = subscription;
    this.promocode = promocode;
    this.type = type;
    this.lang = lang;
  }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
  final GlobalKey<FormState> globalKey3 = new GlobalKey<FormState>();
  String payType;
  bool _isLoading = false;
  String firstName;
  String email;
  String lastName;
  String phone;
  String token;
  String weletphone;
  int paidValue;
  Api api;
  var headers;
  Request request;
  String url;
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }
  connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
 @override
  initState() {

    super.initState();
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
   if(type=="creditCard")
   payType="card";
   else if(type=="mahfaza")
   payType="wallet";
    else if(type=="amaan")
   payType="kiosh";

  }

  showWelet() {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            scrollable: true,
            backgroundColor: appfixedColor,
            title: Text(AppTranslations.of(context).text("Wallet Number")),
            content: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Form(
                key: globalKey3,
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: height * 0.01,
                    ),
                    Container(
                        width: width * .5,
                        height: height * .1,
                      
                        child: Wrap(children: <Widget>[
                          CustomTextField(
                            onclick: (value) {
                              weletphone = value;
                            },
                            hint: AppTranslations.of(context)
                                .text("Phone Number"),
                            isNumber: true,
                          
                          ),
                        ])),
                    SizedBox(
                      height: height * 0.01,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RaisedButton(
                              onPressed: () {
                                globalKey3.currentState.save();

                                if (globalKey3.currentState.validate()) {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => CardInfo(
                                                type: type,
                                                subscription: subscription,
                                                promocode: promocode,
                                                lang: lang,
                                                phone: weletphone,
                                                token: token,
                                              )));
                                }
                              }, // Refer step 3
                              child: Text(
                                AppTranslations.of(context).text("Save"),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              color: Colors.blue,
                            ),
                            SizedBox(
                              width: width * .02,
                            ),
                            RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                AppTranslations.of(context).text("Cansel"),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                              color: Colors.red,
                            ),
                          ]),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  billingDate() async {
    _showLoading();
    setState(() {
      if (promocode != null && promocode != "")
        paidValue = int.parse(subscription.package.priceAfterDiscount) -
            int.parse(subscription.package.couponValue);
      else
        paidValue = int.parse(subscription.package.priceAfterDiscount);
    });
    var body = <String, dynamic>{
      'first_name': firstName,
      'last_name': lastName,
      'phone_number': phone,
      'email': email,
      'lang': lang,
      'amount': paidValue,
      'subscription_id': subscription.id,
      'type' :payType
    };
    api = new Api();
    request = new Request();
    url = "/api/payment/generateToken";
    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    await api.postData(url, headers,context, body: body).then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print(statusCode);
        _hideLoading();
        Map<String, dynamic> json = jsonDecode(res);
        if (json['errors']['first_name'] != null) {
          showAlert(context, json["errors"]["first_name"][0], "error", null);
        } else if (json['errors']['last_name'] != null) {
          showAlert(context, json["errors"]["last_name"][0], "error", null);
        } else if (json['errors']['phone_number'] != null) {
          showAlert(context, json["errors"]["phone_number"][0], "error", null);
        } else if (json['errors']['email'] != null) {
          showAlert(context, json["errors"]["email"][0], "error", null);
        } else if (json['errors']['amount'] != null) {
          showAlert(context, json["errors"]["amount"][0], "error", null);
        } else if (json['errors']['subscription_id'] != null) {
          showAlert(
              context, json["errors"]["subscription_id"][0], "error", null);
        } else if (json['message'] != null) {
          showAlert(context, json["message"], "error", null);
        }
      } else {
        Map<String, dynamic> json = jsonDecode(res);
        setState(() {
          token = json['data']['token'];
        });
        print(token);
        _hideLoading();
        if (type == "mahfaza")
          showWelet();
        else {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => CardInfo(
                        type: type,
                        subscription: subscription,
                        promocode: promocode,
                        lang: lang,
                        token: token,
                      )));
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        key: _scaffoldKey,
          appBar: CustomAppBar(
          title: "Billing Date",
          scafoldkey: _scaffoldKey,
          hasarowBack: true,
        ),
        drawer: AppDrawer(
          scafoldkey: _scaffoldKey,
        ),
        bottomNavigationBar:
            customBottomNavigationBar(context, width, height, _scaffoldKey),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: ListView(shrinkWrap: true, children: [
              Container(
                width: width,
                height: height,
                padding: EdgeInsets.all(0),
                decoration: new BoxDecoration(
                  color: appfixedColor,
                  image: DecorationImage(
                    image: AssetImage(
                      background,
                    ),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Padding(
                    padding: EdgeInsets.only(
                        left: width * .05,
                        right: width * .05,
                        top: height * .1),
                    child: ListView(shrinkWrap: true, children: [
                      Text(AppTranslations.of(context).text('Billing Date'),
                          textAlign: TextAlign.start, style: big2Style()),
                      SizedBox(
                        height: height * .04,
                      ),
                      Form(
                        key: globalKey,
                        child: ListView(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            children: <Widget>[
                              CustomTextField(
                                hint: AppTranslations.of(context)
                                    .text("First Name"),
                                onclick: (value) {
                                  firstName = value;
                                },
                                isEmail: false,
                              ),
                              SizedBox(
                                height: height * .02,
                              ),
                              CustomTextField(
                                hint: AppTranslations.of(context)
                                    .text("Last Name"),
                                onclick: (value) {
                                  lastName = value;
                                },
                                // container: countryDropDown,

                                isNumber: false,
                              ),
                              SizedBox(
                                height: height * .02,
                              ),
                              CustomTextField(
                                hint: AppTranslations.of(context).text("Email"),
                                onclick: (value) {
                                  email = value;
                                },
                             

                                isEmail: true,
                              ),
                              SizedBox(
                                height: height * .02,
                              ),
                              CustomTextField(
                                hint: AppTranslations.of(context)
                                    .text("Phone Number"),
                                onclick: (value) {
                                  phone = value;
                                },
                                isNumber: true,
                              ),
                              SizedBox(
                                height: height * .02,
                              ),
                              ButtonTheme(
                                height: height * .08,
                                minWidth: MediaQuery.of(context).size.width,
                                child: _isLoading
                                    ? circularProgress()
                                    : RaisedButton(
                                        elevation: 0.0,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        onPressed: () async {
                                          globalKey.currentState.save();

                                          if (globalKey.currentState
                                              .validate()) {
                                            billingDate();
                                          }
                                        },
                                        padding: EdgeInsets.all(0),
                                        color: appPrimaryColor,
                                        child: Text(
                                            AppTranslations.of(context)
                                                .text('Next'),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16)),
                                      ),
                              ),
                            ]),
                      ),
                    ])),
              ),
            ])));
  }
}
