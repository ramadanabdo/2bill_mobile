import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/model/Contact.dart';
import 'package:two_bill/model/subscribrion.dart';
import 'package:two_bill/screens/Home/subscribePackage.dart';
import 'package:two_bill/screens/paymentt/successSubcription.dart';
import 'package:two_bill/screens/user_management/myPrfoilePackages.dart';
import 'package:two_bill/screens/user_management/myProfile.dart';
import 'package:two_bill/screens/user_management/payment.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/utils/style.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CardInfo extends StatefulWidget {
  Subscription subscription;
  String type;
  String promocode;
  String lang;
  String phone;
  String token;
  CardInfo(
      {Key key,
      @required this.promocode,
      @required this.subscription,
      @required this.type,
      @required this.lang,
      this.phone,
      this.token})
      : super(key: key);
  @override
  _CardInfo createState() => _CardInfo(subscription, promocode, type, lang,phone,token);
}

class _CardInfo extends State<CardInfo> {
  Subscription subscription;
  String promocode;
  String type;
  String lang;
    String phone;
  String token;
  _CardInfo(
      Subscription subscription, String promocode, String type, String lang,String phone,String token) {
    this.subscription = subscription;
    this.promocode = promocode;
    this.type = type;
    this.lang = lang;
    this.phone=phone;
    this.token=token;
  }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  Subscription acctiveSubscription;
Contact contact;
  Api api;
  Request request;
  String url;
  var headers;
  static final BASE_URL = 'https://admin.2bill.net';
  int paidValue;
  String baseUrl;
  bool _isLoading = false;

  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }

  Future<Subscription> getData() async {
    print(subscription.id);
    api = new Api();
    request = new Request();
    url = "/api/subscriptions/${subscription.id}";

    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });

    await api.getData(url, headers,context).then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print('dhdshdhjfhjsfhfh$statusCode');
        _hideLoading();
      } else {
        Map<String, dynamic> json = jsonDecode(res);

        setState(() {
          print(res);
          acctiveSubscription = Subscription.fromJson(json['data']['data']);
        });
        print(res);
        print(acctiveSubscription.toJson());
        _hideLoading();
      }
    });
    return acctiveSubscription;
  }
connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  @override
  void initState() {
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
    api = new Api();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    super.initState();
    print(subscription.phone);
    print("subscription paymen ${subscription.id}");

    loadData();

  }

  loadData() {
    setState(() {
      if (promocode != null && promocode != "")
        paidValue = int.parse(subscription.package.priceAfterDiscount) -
            int.parse(subscription.package.couponValue);
      else
        paidValue = int.parse(subscription.package.priceAfterDiscount);
    });
    switch (type) {
      case "creditCard":
        baseUrl =
            "$BASE_URL/api/payment/card/$token/$lang";
        print(baseUrl);
        break;
      case "amaan":
        baseUrl =
            "$BASE_URL/api/payment/kiosh/$token/$lang";
              print(baseUrl);
        break;
      case "mahfaza":
        baseUrl =
            " $BASE_URL/api/payment/wallet/$token/$phone/$lang";
              print(baseUrl);
        break;
      case "fawery":
        baseUrl =
            "$BASE_URL/api/payment/fawry/${subscription.id}/${subscription.user.id}/$paidValue/$lang";
              print(baseUrl);
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: appfixedColor,
      appBar: CustomAppBar(
        title: "Service Payment",
        scafoldkey: _scaffoldKey,
        hasarowBack: false,
        drwer: false,
      ),
      body: WebView(
        initialUrl: baseUrl,
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController webViewController) {
          _controller.complete(webViewController);
        },
        javascriptChannels: Set.from(
          [
            JavascriptChannel(
                name: 'OnApprove',
                onMessageReceived: (JavascriptMessage message) async {}),
            JavascriptChannel(
                name: 'OnSuccess',
                onMessageReceived: (JavascriptMessage message) {}),
            JavascriptChannel(
                name: 'OnCancel',
                onMessageReceived: (JavascriptMessage message) {}),
            JavascriptChannel(
                name: 'OnError',
                onMessageReceived: (JavascriptMessage message) {}),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          await getData().then((Subscription newSuubscription) {
            setState(() {
              acctiveSubscription = newSuubscription;
            });
            print('jjhjkjkkjj ${acctiveSubscription.isPaid}');
          });
          if (acctiveSubscription != null && acctiveSubscription.isPaid == 1) {
            print('SuccessSubsciption');
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => SuccessSubsciption(
                    subscription: acctiveSubscription,
                  ),
                ),
                (_) => false);
          } else {
            print('SubscribePackage');
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (context) => MyPackages(
                    hasArowBack: false,
                    package: subscription.package,
                  ),
                ),
                (_) => false);
          }
        },
        child: new Icon(
          Icons.arrow_back,
          color: Colors.white,
        ),
      ),
    );
  }
}
