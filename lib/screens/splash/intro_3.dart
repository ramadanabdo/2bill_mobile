import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/style.dart';

class Intro_3 extends StatefulWidget {
  @override
  _Intro_3 createState() => _Intro_3();
}

class _Intro_3 extends State<Intro_3> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        width: width,
        height: height,
        padding: EdgeInsets.all(0),
        decoration: new BoxDecoration(
          color: appfixedColor,
          image: DecorationImage(
            image: AssetImage(
              splash_background2,
            ),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(shrinkWrap: true, children: [
          SizedBox(
            height: height * .5,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(AppTranslations.of(context).text('Invite your Friends'),
                  textAlign: TextAlign.start,
                  strutStyle: StrutStyle(
                    forceStrutHeight: false,
                  ),
                  style: TextStyle(
                    
                      color: Color(0xff555555),
                      fontSize: 25,
                      fontWeight: FontWeight.w900,
                      fontFamily: 'Monstserrat')),
               AppTranslations.of(context).text("and")=="and"?SizedBox(
                height: height * .01,
              ):SizedBox(
                height: height * .005,
              ),
              
              
              
              Text(
                AppTranslations.of(context)
                    .text('To get 50% discount on your package'),
                textAlign: TextAlign.start,
                strutStyle: StrutStyle(
                    forceStrutHeight: false,
                  ),
                style: smalBigStyle(),
              ),
              Text(
                AppTranslations.of(context)
                    .text('add 5 friends to the same package'),
                textAlign: TextAlign.start,
                strutStyle: StrutStyle(
                    forceStrutHeight: false,
                  ),
                style: smalBigStyle(),
              ),
              Text(
                AppTranslations.of(context)
                    .text('within 20 days'),
                textAlign: TextAlign.start,
                strutStyle: StrutStyle(
                    forceStrutHeight: false,
                  ),
                style: smalBigStyle(),
              ),
            ],
          ),
            AppTranslations.of(context).text("and")=="and"?SizedBox(
                height: height * .05,
              ):SizedBox(
                height: height * .04,
              ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width * .04),
            child: ButtonTheme(
              height: height * .08,
              minWidth: MediaQuery.of(context).size.width,
              child: RaisedButton(
                elevation: 0.0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/Intro_4', (_) => false);
                },
                padding: EdgeInsets.all(0),
                color: appPrimaryColor,
                child: Text(AppTranslations.of(context).text('Next'),
                    style: TextStyle(color: Colors.white)),
              ),
            ),
          ),
           AppTranslations.of(context).text("and")=="and"?SizedBox(
                height: height * .05,
              ):SizedBox(
                height: height * .035,
              ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 10.0,
                height: 10.0,
                decoration: BoxDecoration(
                  color: Colors.grey[500],
                  shape: BoxShape.circle,
                ),
                child: InkWell(onTap: () {
                  Navigator.pushNamed(context, '/Intro_1');
                }),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                width: 10.0,
                height: 10.0,
                decoration: BoxDecoration(
                  color: Colors.grey[500],
                  shape: BoxShape.circle,
                ),
                child: InkWell(onTap: () {
                  Navigator.pushNamed(context, '/Intro_2');
                }),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                width: 10.0,
                height: 10.0,
                decoration: BoxDecoration(
                  color: appPrimaryColor,
                  shape: BoxShape.circle,
                ),
                child: InkWell(onTap: () {
                  Navigator.pushNamed(context, '/Intro_3');
                }),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                width: 10.0,
                height: 10.0,
                decoration: BoxDecoration(
                  color: Colors.grey[500],
                  shape: BoxShape.circle,
                ),
                child: InkWell(onTap: () {
                  Navigator.pushNamed(context, '/Intro_4');
                }),
              ),
            ],
          ),
        ]),
      ),
    );
  }
}
