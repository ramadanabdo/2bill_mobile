import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/style.dart';

import 'intro_1.dart';

class Intro extends StatefulWidget {
  @override
  _Intro createState() => _Intro();
}

class _Intro extends State<Intro> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    Timer _timer;
    Helper helper = new Helper();
  void startTime() async{
   _timer = Timer(new Duration(seconds: 4), () => { Navigator.pushNamed(
         context, "/Intro_1",
           
   )}
   );
  }


 @override
  void initState() {
    super.initState();
   
   
  Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  handleValue(value) {

    if (value == "null" || value=="" || value ==null) {
      startTime();
    }else{
        Navigator.of(context).pushNamedAndRemoveUntil("/home", (_) => false);
    }
  }
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      body:Container(
          width: width,
          height: height,
          padding: EdgeInsets.all(0),
          decoration: new BoxDecoration(
            color: appfixedColor,
            image: DecorationImage(
              image: AssetImage(
                splash_background,
              ),
              fit: BoxFit.cover,
            ),
          ),
          child: Column( 
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
            SizedBox(
              height: height * .3,
            ),
            Text(
              '2BILL',
              textScaleFactor: 2,
              style: bigStyle()
            ),
          ]),
        ),
      
    );
  }
}
