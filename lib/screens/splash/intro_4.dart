import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/style.dart';

class Intro_4 extends StatefulWidget {
  @override
  _Intro_4 createState() => _Intro_4();
}

class _Intro_4 extends State<Intro_4> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
          width: width,
          height: height,
          padding: EdgeInsets.all(0),
          decoration: new BoxDecoration(
            color: appfixedColor,
            image: DecorationImage(
              image: AssetImage(
                splash_background3,
              ),
              fit: BoxFit.cover,
            ),
          ),
          child: ListView(
            shrinkWrap: true,
            children: [
              SizedBox(
                      height: height * .47,
                    ),
                Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                    
                    Text(AppTranslations.of(context).text('Join & Save your Money'),
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Color(0xff555555),
                            fontSize: 25,
                            fontWeight: FontWeight.w900,
                            fontFamily: 'Monstserrat')),
                    Text(
                      AppTranslations.of(context)
                          .text('Your credit will be transferred'),
                      textAlign: TextAlign.start,
                      style: smalBigStyle(),
                    ),
                    Text(
                      AppTranslations.of(context)
                          .text('after 30 days from the date'),
                      textAlign: TextAlign.start,
                      style: smalBigStyle(),
                    ),
                    Text(
                      AppTranslations.of(context)
                          .text('you subscribed to the package'),
                      textAlign: TextAlign.start,
                      style: smalBigStyle(),
                    ),
                    
                    
                  ],
                ),
               
             AppTranslations.of(context).text("and")=="and"?SizedBox(
                height: height * .05,
              ):SizedBox(
                height: height * .04,
              ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: width*.04),
                                      child: ButtonTheme(
                      height: height * .08,
                      minWidth: MediaQuery.of(context).size.width,
                      child: RaisedButton(
                        elevation: 0.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        onPressed: () {
                          Navigator.pushNamedAndRemoveUntil(
                              context, '/createAccount', (_) => false);
                        },
                        padding: EdgeInsets.all(0),
                        color: appPrimaryColor,
                        child: Text(AppTranslations.of(context).text('Next'),
                            style: TextStyle(color: Colors.white)),
                      ),
                    ),
                  ),
                 AppTranslations.of(context).text("and")=="and"?SizedBox(
                height: height * .05,
              ):SizedBox(
                height: height * .04,
              ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 10.0,
                height: 10.0,
                decoration: BoxDecoration(
                  color: Colors.grey[500],
                  shape: BoxShape.circle,
                ),
                child: InkWell(onTap: () {
                  Navigator.pushNamed(context, '/Intro_1');
                }),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                width: 10.0,
                height: 10.0,
                decoration: BoxDecoration(
                  color: Colors.grey[500],
                  shape: BoxShape.circle,
                ),
                child: InkWell(onTap: () {
                  Navigator.pushNamed(context, '/Intro_2');
                }),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                width: 10.0,
                height: 10.0,
                decoration: BoxDecoration(
                  color: Colors.grey[500],
                  shape: BoxShape.circle,
                ),
                child: InkWell(onTap: () {
                  Navigator.pushNamed(context, '/Intro_3');
                }),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                width: 10.0,
                height: 10.0,
                decoration: BoxDecoration(
                  color: appPrimaryColor,
                  shape: BoxShape.circle,
                ),
                child: InkWell(onTap: () {
                  Navigator.pushNamed(context, '/Intro_4');
                }),
              ),
            ],
          ),
       
            ]),

           
      ),
    );
  }
}
