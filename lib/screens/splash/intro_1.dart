import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/application.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/style.dart';


class Intro_1 extends StatefulWidget {
  @override
  _Intro_1 createState() => _Intro_1();
}

class _Intro_1 extends State<Intro_1> {

  void _changeLanguage(lang) {
    
    Locale locale;
    switch (lang) {
      case "English":
        locale = Locale('en');
        application.onLocaleChanged(Locale('en'));
        break;
      case "Arabic":
        locale = Locale('ar');
        application.onLocaleChanged(Locale('ar'));
        break;
      default:
        locale = Locale('en');
        application.onLocaleChanged(Locale('en'));
    }
  }
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        width: width,
        height: height,
        padding: EdgeInsets.all(0),
        decoration: new BoxDecoration(
          color: appfixedColor,
          image: DecorationImage(
             
              image :AssetImage(
                background,
              ),
              fit: BoxFit.cover,

            ),
        ),
        child: ListView(shrinkWrap: true, children: [
          SizedBox(height: height * .3),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width * .08),
            child: Column(
              children: [
                Container(
                  width: width,
                  child: Text(
                    AppTranslations.of(context).text('hello'),
                
                    textAlign: TextAlign.start,
                        textScaleFactor: 1.2,
                    style: bigStyle(),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                   
                  child: Text(
                    AppTranslations.of(context).text('Choose Your Preffered language'),
                    strutStyle: StrutStyle(
                    forceStrutHeight: false,
                    
                  ),
                    textAlign: TextAlign.start,
                    style: mediumStyle(),
                  ),
                ),
               
                AppTranslations.of(context).text("and")=="and"?  SizedBox(
                  height: height * .02,
                ): SizedBox(
                  height: height * .01,
                ),
                Card(
                  color: Colors.white,
                  child: ExpansionTile(
                      title: Text(
                       
                          AppTranslations.of(context).text('language'),
                          strutStyle: StrutStyle(
                    forceStrutHeight: true,
                     
                  ),
                           
                        style: TextStyle(
                          color: Color(0xff555555),
                        ),
                      ),
                      children: <Widget>[
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.all(1.0),
                            child: ListTile(
                              title: Text(
                             'English',
                                textDirection: TextDirection.ltr,
                                strutStyle: StrutStyle(
                    forceStrutHeight: true,
                  ),
                                style: TextStyle(
                                    color: AppTranslations.of(context)
                                                .text('and') ==
                                            "and"
                                        ? Colors.white
                                        : appPrimaryColor,
                                    fontWeight: FontWeight.bold),
                              ),
                              onTap: () {
                                  _changeLanguage("English");
                              },
                            ),
                          ),
                          color: AppTranslations.of(context).text('and') ==
                                  "and"
                              ? appPrimaryColor
                              : Colors.white,
                        ),
                        Container(
                          color: AppTranslations.of(context).text('and') ==
                                  "and"
                                  ? Colors.white
                              : appPrimaryColor,
                              
                          child: Padding(
                            padding: const EdgeInsets.all(1.0),
                            child: ListTile(
                                onTap: () {
                                    _changeLanguage("Arabic");
                                },
                                title: Text("العربية",
                                strutStyle: StrutStyle(
                    forceStrutHeight: false,
                  ),
                                    textDirection: TextDirection.rtl,
                                    style: TextStyle(
                                        color: AppTranslations.of(context).text('and') ==
                                  "and"?appPrimaryColor:
                                             Colors.white,
                                        fontWeight: FontWeight.bold))),
                          ),
                        ),
                      ]),
                ),
                 AppTranslations.of(context).text("and")=="and"?  SizedBox(
                  height: height * .03,
                ): SizedBox(
                  height: height * .02,
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width * .04),
            child: ButtonTheme(
              height: height * .08,
              minWidth: MediaQuery.of(context).size.width,
              child: RaisedButton(
                elevation: 0.0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, '/Intro_2', (_) => false);
                },
                padding: EdgeInsets.all(0),
                color: appPrimaryColor,
                child: Text(AppTranslations.of(context).text('Next'),
                strutStyle: StrutStyle(
                    forceStrutHeight: false,
                  ),
                    style: TextStyle(color: Colors.white)),
              ),
            ),
          ),
          AppTranslations.of(context).text("and")=="and"?  SizedBox(
                  height: height * .03,
                ): SizedBox(
                  height: height * .02,
                ),
          
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
             Container(
              width: 10.0,
              height: 10.0,
              decoration: BoxDecoration(
                color: appPrimaryColor,
                shape: BoxShape.circle,
              ),
              child: InkWell(onTap: () {
                Navigator.pushNamed(context, '/Intro_1');
              }),
            ),
            SizedBox(
              width: 10,
            ),
            Container(
              width: 10.0,
              height: 10.0,
              decoration: BoxDecoration(
                color: Colors.grey[500],
                shape: BoxShape.circle,
              ),
              child: InkWell(onTap: () {
                Navigator.pushNamed(context, '/Intro_2');
              }),
            ),
             SizedBox(
              width: 10,
            ),
            Container(
              width: 10.0,
              height: 10.0,
              decoration: BoxDecoration(
                color: Colors.grey[500],
                shape: BoxShape.circle,
              ),
              child: InkWell(onTap: () {
                Navigator.pushNamed(context, '/Intro_3');
              }),
            ),
             SizedBox(
              width: 10,
            ),
            Container(
              width: 10.0,
              height: 10.0,
              decoration: BoxDecoration(
                color: Colors.grey[500],
                shape: BoxShape.circle,
              ),
              child: InkWell(onTap: () {
                Navigator.pushNamed(context, '/Intro_4');
              }),
            ),
            ],
          ),
        ]),
      ),
    );
  }
}
