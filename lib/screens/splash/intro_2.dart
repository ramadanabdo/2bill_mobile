import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/style.dart';

class Intro_2 extends StatefulWidget {
  @override
  _Intro_2 createState() => _Intro_2();
}

class _Intro_2 extends State<Intro_2> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Container(
        width: width,
        height: height,
        padding: EdgeInsets.all(0),
        decoration: new BoxDecoration(
          color: appfixedColor,
          
          image: DecorationImage(
         image:  AppTranslations.of(context).text('and')=="and"?
           AssetImage(
              splash_backgroundEn,
              
            ): AssetImage(
              splash_backgroundAr,
              
            ),
            fit: BoxFit.cover,
          ),
        ),
      
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: width*.05),
                      child: ListView(
              children: [
               AppTranslations.of(context).text("and")=="and"?  SizedBox(
                  height: height * .19,
                ): SizedBox(
                  height: height * .18,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                                  "2Bill ",
                                
                                  textAlign: TextAlign.start,
                                  style: bigStyle(),
                                ),
                               Text(
                                  AppTranslations.of(context).text('2Bill Save 50%'),
                                
                                  textAlign: TextAlign.start,
                                  style: bigStyle(),
                                ),
                  ],
                ),

                 Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                                  "2Bill ",
                                
                                  textAlign: TextAlign.start,
                                  style: smalBigStyle(),
                                ),
                               Text(
                                  AppTranslations.of(context).text('2Bill is the only app to get 50% off'),
                                
                                  textAlign: TextAlign.start,
                                  style: smalBigStyle(),
                                ),
                  ],
                ),
               
               
                Text(
                  AppTranslations.of(context).text('for all telecome & internet packages'),
                
                  textAlign: TextAlign.start,
                  style: smalBigStyle(),
                ),
               
                 AppTranslations.of(context).text("and")=="and"?  SizedBox(
                  height: height * .39,
                ): SizedBox(
                  height: height * .38,
                ),
                ButtonTheme(
                  height: height * .08,
                  minWidth: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    elevation: 0.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onPressed: () {
                      Navigator.pushNamedAndRemoveUntil(
                          context, '/Intro_3', (_) => false);
                    },
                    padding: EdgeInsets.all(0),
                    color: appPrimaryColor,
                    child: Text(AppTranslations.of(context).text('Next'),
                   
                        style: TextStyle(color: Colors.white,height: 1.5)),
                  ),
                ),
                AppTranslations.of(context).text("and")=="and"?  SizedBox(
                  height: height * .05,
                ): SizedBox(
                  height: height * .04,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 10.0,
                      height: 10.0,
                      decoration: BoxDecoration(
                        color:Colors.grey[500],
                        shape: BoxShape.circle,
                      ),
                      child: InkWell(onTap: () {
                        Navigator.pushNamed(context, '/Intro_1');
                      }),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      width: 10.0,
                      height: 10.0,
                      decoration: BoxDecoration(
                        color: appPrimaryColor,
                        shape: BoxShape.circle,
                      ),
                      child: InkWell(onTap: () {
                        Navigator.pushNamed(context, '/Intro_2');
                      }),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      width: 10.0,
                      height: 10.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[500],
                        shape: BoxShape.circle,
                      ),
                      child: InkWell(onTap: () {
                        Navigator.pushNamed(context, '/Intro_3');
                      }),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      width: 10.0,
                      height: 10.0,
                      decoration: BoxDecoration(
                        color: Colors.grey[500],
                        shape: BoxShape.circle,
                      ),
                      child: InkWell(onTap: () {
                        Navigator.pushNamed(context, '/Intro_4');
                      }),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      
    );
  }
}
