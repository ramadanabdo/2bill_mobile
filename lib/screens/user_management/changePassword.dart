import 'dart:convert';

import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:two_bill/model/user.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/screens/wedget/show_%20SweetAlert.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePassword createState() => _ChangePassword();
}

class _ChangePassword extends State<ChangePassword> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
  String newPassword;
  String currentPassword;
  String confirmNewPasswword;

  User user;
  bool _isLoading = false;
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }

  connectstatus(value) {
    if (value == false) {
      showErrorDialog(context, "check_internet");
    }
  }

  @override
  void initState() {
    Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
    Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
  }

  handleValue(value) {
    if (value == "null" || value == "" || value == null) {
      helper.removeToken();
      localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/Intro', (_) => false);
    } else {}
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(AppTranslations.of(context).text(text))));
  }

  bool _obscureText1 = true;
  bool _obscureText2 = true;
  bool _obscureText3 = true;
  LocalLogin localLogin = new LocalLogin();
  Api api;
  Helper helper = new Helper();
  Request request;
  String url;
  var headers;
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: appfixedColor,
      appBar: CustomAppBar(
        title: "Change Password",
        scafoldkey: _scaffoldKey,
      ),
      drawer: AppDrawer(
        scafoldkey: _scaffoldKey,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: ListView(
          shrinkWrap: true,
          children: [
            SizedBox(
              height: height * .05,
            ),
            Form(
              key: globalKey,
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * .04),
                    child: Container(
                      height: height * .45,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[400],
                              spreadRadius: 2,
                              blurRadius: 2,
                              offset: Offset(0, 7),
                            ),
                          ],
                          borderRadius: BorderRadius.circular(17),
                          color: Colors.white),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: width * .05),
                        child: Column(
                          children: [
                            TextFormField(
                              validator: (value) {
                                if (value.isEmpty) if (value.length < 8) {
                                  return AppTranslations.of(context).text(
                                      "Password contain at least 8 letters");
                                }
                              },
                              obscureText: _obscureText1,
                              onSaved: (val) => currentPassword = val,
                              decoration: InputDecoration(
                                labelText: AppTranslations.of(context)
                                    .text('Current Password'),
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    // Based on passwordVisible state choose the icon
                                    _obscureText1
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: passwordColor,
                                  ),
                                  onPressed: () {
                                    // Update the state i.e. toogle the state of passwordVisible variable
                                    setState(() {
                                      _obscureText1 = !_obscureText1;
                                    });
                                  },
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey[100]),
                                ),
                              ),
                            ),
                            TextFormField(
                              validator: (value) {
                                if (value.length < 8) {
                                  return AppTranslations.of(context).text("Password contain at least 8 letters");
                                }
                              },
                              obscureText: _obscureText2,
                              onSaved: (val) => newPassword = val,
                              decoration: InputDecoration(
                                labelText: AppTranslations.of(context)
                                    .text('New Password'),
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    // Based on passwordVisible state choose the icon
                                    _obscureText2
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: passwordColor,
                                  ),
                                  onPressed: () {
                                    // Update the state i.e. toogle the state of passwordVisible variable
                                    setState(() {
                                      _obscureText2 = !_obscureText2;
                                    });
                                  },
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey[100]),
                                ),
                              ),
                            ),
                            TextFormField(
                              validator: (value) {
                                if (value.length < 8) {
                                  return AppTranslations.of(context).text("Password contain at least 8 letters");
                                }
                              },
                              obscureText: _obscureText3,
                              onSaved: (val) => confirmNewPasswword = val,
                              decoration: InputDecoration(
                                labelText: AppTranslations.of(context)
                                    .text('Confirm New Password'),
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    // Based on passwordVisible state choose the icon
                                    _obscureText3
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: passwordColor,
                                  ),
                                  onPressed: () {
                                    // Update the state i.e. toogle the state of passwordVisible variable
                                    setState(() {
                                      _obscureText3 = !_obscureText3;
                                    });
                                  },
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey[100]),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height * .05,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * .11),
                    child: Column(children: [
                      Text(
                        AppTranslations.of(context)
                            .text('Password Is at least 8-20 characters long'),
                        style: TextStyle(
                          color: Colors.grey[600],
                          fontSize: 15,
                        ),
                      ),
                      Text(
                        AppTranslations.of(context)
                            .text('Contain an uper case letter and number'),
                        style: TextStyle(
                          color: Colors.grey[600],
                          fontSize: 12,
                        ),
                      )
                    ]),
                  ),
                  SizedBox(
                    height: height * .08,
                  ),
                  _isLoading
                      ? circularProgress()
                      : ButtonTheme(
                          height: height * .08,
                          minWidth: width * .9,
                          child: /* _isLoading?   circularProgress():*/ RaisedButton(
                            elevation: 0.0,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            onPressed: () async {
                              globalKey.currentState.save();
                              if (newPassword == confirmNewPasswword) {
                                if (globalKey.currentState.validate()) {
                                  _showLoading();
                                  helper = new Helper();
                                  // Navigator.pushNamed(context, '/Login');
                                  var body = <String, dynamic>{
                                    "oldpassword": currentPassword,
                                    "password": newPassword,
                                    "password_confirmation": confirmNewPasswword
                                  };
                                  await helper
                                      .getUserData(context)
                                      .then((dynamic dyuser) async {
                                    user = dyuser;
                                  });
                                  print('user ${user.id}');
                                  api = new Api();
                                  request = new Request();

                                  url = "/api/changePassword/${user.id}";
                                  await request
                                      .getHeaader(context)
                                      .then((dynamic dyHeaders) async {
                                    headers = dyHeaders;
                                  });
                                  print('headers $headers');
                                  //  Navigator.pushNamed(context, '/Login');
                                  api
                                      .putData(url, headers, context,
                                          body: body)
                                      .then((dynamic response) async {
                                    final String res = response.body;
                                    final int statusCode = response.statusCode;
                                    if (statusCode == 401 ||
                                        statusCode == 400 ||
                                        statusCode == 403 ||
                                        statusCode == 501 ||
                                        statusCode == 404) {
                                      _hideLoading();
                                      print(statusCode);
                                      _hideLoading();
                                      Map<String, dynamic> json =
                                          jsonDecode(res);
                                      if (json['errors']['oldpassword'] !=
                                          null) {
                                        showAlert(
                                            context,
                                            json["errors"]["oldpassword"][0],
                                            "error",
                                            null);
                                      } else if (json['errors']['password'] !=
                                          null) {
                                        showAlert(
                                            context,
                                            json["errors"]["password"][0],
                                            "error",
                                            null);
                                      } else if (json['errors']
                                              ['password_confirmation'] !=
                                          null) {
                                        showAlert(
                                            context,
                                            json["errors"]
                                                ["password_confirmation"][0],
                                            "error",
                                            null);
                                      }
                                    } else {
                                      Map<String, dynamic> json =
                                          jsonDecode(res);
                                      print(json['access_token']);
                                        _hideLoading();
                                      await localLogin.clearFile();
                                      await localLogin
                                          .encrypt(
                                              user.phone + ' ' + newPassword)
                                          .then((var encr) {
                                        localLogin.writeData(encr);
                                        print(encr);
                                        localLogin.readData().then((var decr) {
                                          localLogin
                                              .decrypt(decr)
                                              .then((var data) {
                                            print(data);
                                          });
                                        });
                                      });
                                    
                                      Alert(
                                        context: context,
                                        type: AlertType.success,
                                        title: AppTranslations.of(context)
                                                    .text("and") ==
                                                "and"
                                            ? "Success"
                                            : "تم بنجاح",
                                        desc: AppTranslations.of(context).text(
                                            "Password change  successfully"),
                                        buttons: [
                                          DialogButton(
                                            child: Text(
                                              "Ok",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 20),
                                            ),
                                            onPressed: () {
                                              Navigator.pushNamed(context, '/');
                                            },
                                            width: 120,
                                          )
                                        ],
                                      ).show();
                                    }
                                  });
                                }
                              } else {
                                _showSnackBar("Enter the same password");
                              }
                            },
                            padding: EdgeInsets.all(0),
                            color: appPrimaryColor,
                            child: Text(
                                AppTranslations.of(context).text('Change'),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16)),
                          ),
                        ),
                  SizedBox(
                    height: height * .08,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
