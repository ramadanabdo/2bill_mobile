import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/screens/wedget/show_%20SweetAlert.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/utils/style.dart';
import 'package:two_bill/screens/wedget/CustomTextField.dart';

class PerfiyCod extends StatefulWidget {
  @override
  _PerfiyCod createState() => _PerfiyCod();
}

class _PerfiyCod extends State<PerfiyCod> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
  FocusNode nod1 = new FocusNode();
  FocusNode nod2 = new FocusNode();
  FocusNode nod3 = new FocusNode();
  FocusNode nod4 = new FocusNode();
  FocusNode nod5 = new FocusNode();
  FocusNode nod6 = new FocusNode();

  String num1, num2, num3, num4, num5;
  String newPassword;
  String confirmNewPasswword;
  Helper helper = new Helper();
  Request request;
  Api api;
   LocalLogin localLogin=new LocalLogin();
  String resetCode;
  bool _obscureText1 = true;
  bool _obscureText2 = true;
  bool _obscureText3 = true;
  bool _isLoading = false;
  String url;
  var headers;

  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }
connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
   @override
  void initState() {
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
  
    
  }
 
  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(AppTranslations.of(context).text(text))));
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: ListView(shrinkWrap: true, children: [
            Container(
              width: width,
              height: height,
              padding: EdgeInsets.all(0),
              decoration: new BoxDecoration(
                color: appfixedColor,
                image: DecorationImage(
                  image: AssetImage(
                    perify,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
              child: ListView(children: [
                SizedBox(
                  height: height * .3,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Form(
                      key: globalKey,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding:
                                  EdgeInsets.symmetric(horizontal: width * .04),
                              child: Container(
                                height: height * .49,
                                decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey[400],
                                        spreadRadius: 2,
                                        blurRadius: 2,
                                        offset: Offset(0, 7),
                                      ),
                                    ],
                                    borderRadius: BorderRadius.circular(17),
                                    color: Colors.grey[200]),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width * .05),
                                  child: Column(
                                    children: [
                                      AppTranslations.of(context).text("and") ==
                                              "and"
                                          ? SizedBox(
                                              height: height * .03,
                                            )
                                          : SizedBox(
                                              height: height * .02,
                                            ),
                                      Directionality(
                                        textDirection:
                                            AppTranslations.of(context)
                                                        .text("and") ==
                                                    "and"
                                                ? TextDirection.ltr
                                                : TextDirection.ltr,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Container(
                                              width: width * .12,
                                              child: TextFormField(
                                                maxLength: 1,
                                                onChanged: (val) {
                                                  if (val.isEmpty) {
                                                  } else
                                                    FocusScope.of(context)
                                                        .requestFocus(nod2);
                                                },
                                                focusNode: nod1,
                                                validator: (value) {
                                                  if (value.isEmpty)
                                                         return '';
                                                  
                                                },
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.bold),
                                                keyboardType:
                                                    TextInputType.number,
                                                decoration: InputDecoration(
                                                  counterStyle:
                                                      TextStyle(fontSize: 0),
                                                  hintText: '',
                                                  fillColor: Colors.white,
                                                  filled: true,
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          borderSide:
                                                              BorderSide(
                                                            color: Colors.white,
                                                          )),
                                                  border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      borderSide: BorderSide(
                                                        color: Colors.white,
                                                      )),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          borderSide:
                                                              BorderSide(
                                                            color: Colors.white,
                                                          )),
                                                ),
                                                onSaved: (value) {
                                                  num1 = value;
                                                },
                                              ),
                                            ),
                                            SizedBox(
                                              width: width * .02,
                                            ),
                                            Container(
                                              width: width * .12,
                                              child: TextFormField(
                                                maxLength: 1,
                                                onChanged: (val) {
                                                  if (val.isEmpty) {
                                                    FocusScope.of(context)
                                                        .requestFocus(nod1);
                                                  } else
                                                    FocusScope.of(context)
                                                        .requestFocus(nod3);
                                                },
                                                focusNode: nod2,
                                                validator: (value) {
                                                  if (value.isEmpty)
                                                         return '';
                                                },
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.bold),
                                                keyboardType:
                                                    TextInputType.number,
                                                decoration: InputDecoration(
                                                  counterStyle:
                                                      TextStyle(fontSize: 0),
                                                  hintText: '',
                                                  fillColor: Colors.white,
                                                  filled: true,
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          borderSide:
                                                              BorderSide(
                                                            color: Colors.white,
                                                          )),
                                                  border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      borderSide: BorderSide(
                                                        color: Colors.white,
                                                      )),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          borderSide:
                                                              BorderSide(
                                                            color: Colors.white,
                                                          )),
                                                ),
                                                onSaved: (value) {
                                                  num2 = value;
                                                },
                                              ),
                                            ),
                                            SizedBox(
                                              width: width * .02,
                                            ),
                                            Container(
                                              width: width * .12,
                                              child: TextFormField(
                                                maxLength: 1,
                                                onChanged: (val) {
                                                  if (val.isEmpty) {
                                                    FocusScope.of(context)
                                                        .requestFocus(nod2);
                                                  } else
                                                    FocusScope.of(context)
                                                        .requestFocus(nod4);
                                                },
                                                focusNode: nod3,
                                                validator: (value) {
                                                  if (value.isEmpty)
                                                         return '';
                                                },
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.bold),
                                                keyboardType:
                                                    TextInputType.number,
                                                decoration: InputDecoration(
                                                  counterStyle:
                                                      TextStyle(fontSize: 0),
                                                  hintText: '',
                                                  fillColor: Colors.white,
                                                  filled: true,
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          borderSide:
                                                              BorderSide(
                                                            color: Colors.white,
                                                          )),
                                                  border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      borderSide: BorderSide(
                                                        color: Colors.white,
                                                      )),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          borderSide:
                                                              BorderSide(
                                                            color: Colors.white,
                                                          )),
                                                ),
                                                onSaved: (value) {
                                                  num3 = value;
                                                },
                                              ),
                                            ),
                                            SizedBox(
                                              width: width * .02,
                                            ),
                                            Container(
                                              width: width * .12,
                                              child: TextFormField(
                                                maxLength: 1,
                                                onChanged: (val) {
                                                  if (val.isEmpty) {
                                                    FocusScope.of(context)
                                                        .requestFocus(nod3);
                                                  } else
                                                    FocusScope.of(context)
                                                        .requestFocus(nod5);
                                                },
                                                focusNode: nod4,
                                                validator: (value) {
                                                  if (value.isEmpty)
                                                    if (value.isEmpty)
                                                         return '';
                                                },
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.bold),
                                                keyboardType:
                                                    TextInputType.number,
                                                decoration: InputDecoration(
                                                  counterStyle:
                                                      TextStyle(fontSize: 0),
                                                  hintText: '',
                                                  fillColor: Colors.white,
                                                  filled: true,
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          borderSide:
                                                              BorderSide(
                                                            color: Colors.white,
                                                          )),
                                                  border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      borderSide: BorderSide(
                                                        color: Colors.white,
                                                      )),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          borderSide:
                                                              BorderSide(
                                                            color: Colors.white,
                                                          )),
                                                ),
                                                onSaved: (value) {
                                                  num4 = value;
                                                },
                                              ),
                                            ),
                                            SizedBox(
                                              width: width * .02,
                                            ),
                                            Container(
                                              width: width * .12,
                                              child: TextFormField(
                                                maxLength: 1,
                                                onChanged: (val) {
                                                  if (val.isEmpty) {
                                                    FocusScope.of(context)
                                                        .requestFocus(nod4);
                                                  } 
                                                    
                                                },
                                                focusNode: nod5,
                                                validator: (value) {
                                                  if (value.isEmpty)
                                                     if (value.isEmpty)
                                                         return '';
                                                },
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.bold),
                                                keyboardType:
                                                    TextInputType.number,
                                                decoration: InputDecoration(
                                                  counterStyle:
                                                      TextStyle(fontSize: 0),
                                                  hintText: '',
                                                  fillColor: Colors.white,
                                                  filled: true,
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          borderSide:
                                                              BorderSide(
                                                            color: Colors.white,
                                                          )),
                                                  border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                      borderSide: BorderSide(
                                                        color: Colors.white,
                                                      )),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10),
                                                          borderSide:
                                                              BorderSide(
                                                            color: Colors.white,
                                                          )),
                                                ),
                                                onSaved: (value) {
                                                  num5 = value;
                                                },
                                              ),
                                            ),
                                            SizedBox(
                                              width: width * .02,
                                            ),
                                          ],
                                        ),
                                      ),
                                      AppTranslations.of(context).text("and") ==
                                              "and"
                                          ? SizedBox(
                                              height: height * .02,
                                            )
                                          : SizedBox(
                                              height: height * .01,
                                            ),
                                      TextFormField(
                                        validator: (value) {
                                          if (value.length < 8) {
                                            return AppTranslations.of(context).text(
                                                "Password contain at least 8 letters");
                                          }
                                        },
                                        focusNode: nod6,
                                        obscureText: _obscureText2,
                                        onSaved: (val) => newPassword = val,
                                        decoration: InputDecoration(
                                          labelText: AppTranslations.of(context)
                                              .text('New Password'),
                                          suffixIcon: IconButton(
                                            icon: Icon(
                                              // Based on passwordVisible state choose the icon
                                              _obscureText2
                                                  ? Icons.visibility
                                                  : Icons.visibility_off,
                                              color: passwordColor,
                                            ),
                                            onPressed: () {
                                              // Update the state i.e. toogle the state of passwordVisible variable
                                              setState(() {
                                                _obscureText2 = !_obscureText2;
                                              });
                                            },
                                          ),
                                          focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.grey[100]),
                                          ),
                                        ),
                                      ),
                                      TextFormField(
                                        validator: (value) {
                                          if (value.length < 8) {
                                            return AppTranslations.of(context).text(
                                                "Password contain at least 8 letters");
                                          }
                                        },
                                        obscureText: _obscureText3,
                                        onSaved: (val) =>
                                            confirmNewPasswword = val,
                                        decoration: InputDecoration(
                                          labelText: AppTranslations.of(context)
                                              .text('Confirm New Password'),
                                          suffixIcon: IconButton(
                                            icon: Icon(
                                              // Based on passwordVisible state choose the icon
                                              _obscureText3
                                                  ? Icons.visibility
                                                  : Icons.visibility_off,
                                              color: passwordColor,
                                            ),
                                            onPressed: () {
                                              // Update the state i.e. toogle the state of passwordVisible variable
                                              setState(() {
                                                _obscureText3 = !_obscureText3;
                                              });
                                            },
                                          ),
                                          focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.grey[100]),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            AppTranslations.of(context).text("and") == "and"
                                ? SizedBox(
                                    height: height * .02,
                                  )
                                : SizedBox(
                                    height: height * .01,
                                  ),
                            Padding(
                              padding:
                                  EdgeInsets.symmetric(horizontal: width * .08),
                              child: Column(children: [
                                Text(
                                  AppTranslations.of(context).text(
                                      'You will receive a message within 15 minutes'),
                                  style: TextStyle(
                                    color: Colors.grey[600],
                                    fontSize: 15,
                                  ),
                                ),
                              ]),
                            ),
                            AppTranslations.of(context).text("and") == "and"
                                ? SizedBox(
                                    height: height * .02,
                                  )
                                : SizedBox(
                                    height: height * .01,
                                  ),
                            _isLoading
                                ? circularProgress()
                                : ButtonTheme(
                                    height: height * .08,
                                    minWidth:
                                        MediaQuery.of(context).size.width * .9,
                                    child: _isLoading
                                        ? circularProgress()
                                        : RaisedButton(
                                            elevation: 0.0,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            onPressed: () async {
                                              globalKey.currentState.save();
                                              if (globalKey.currentState
                                                  .validate()) {
                                                if (newPassword ==
                                                    confirmNewPasswword) {
                                                  _showLoading();
                                                  resetCode = num1 +
                                                      num2 +
                                                      num3 +
                                                      num4 +
                                                      num5;
                                                  print(resetCode);
                                                  helper = new Helper();
                                                  // Navigator.pushNamed(context, '/Login');
                                                  var body = <String, dynamic>{
                                                    "reset_code": resetCode,
                                                    "password": newPassword,
                                                    "password_confirmation":
                                                        confirmNewPasswword
                                                  };

                                                  api = new Api();
                                                  request = new Request();

                                                  url = "/api/auth/reset";
                                                  await request
                                                      .getHeaader(context)
                                                      .then((dynamic
                                                          dyHeaders) async {
                                                    headers = dyHeaders;
                                                  });
                                                  print('headers $headers');
                                                  //  Navigator.pushNamed(context, '/Login');
                                                  api
                                                      .postData(url, headers,
                                                         context, body: body)
                                                      .then((dynamic
                                                          response) async {
                                                    final String res =
                                                        response.body;
                                                    final int statusCode =
                                                        response.statusCode;
                                                    if (statusCode == 401 ||
                                                        statusCode == 400 ||
                                                        statusCode == 403 ||
                                                        statusCode == 501 ||
                                                        statusCode == 404) {
                                                      _hideLoading();
                                                      print(statusCode);
                                                      _hideLoading();
                                                      Map<String, dynamic>
                                                          json =
                                                          jsonDecode(res);
                                                      print(json);
                                                      showAlert(
                                                          context,
                                                          json['errors']
                                                              ['reset_code'][0],
                                                          "error",
                                                          null);
                                                    } else {
                                                      Alert(
                                                        context: context,
                                                        type: AlertType.success,
                                                        title: AppTranslations.of(
                                                                        context)
                                                                    .text(
                                                                        "and") ==
                                                                "and"
                                                            ? "Success"
                                                            : "تم بنجاح",
                                                        desc: AppTranslations
                                                                .of(context)
                                                            .text(
                                                                "Password Reset successfully"),
                                                        buttons: [
                                                          DialogButton(
                                                            child: Text(
                                                              "Ok",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 20),
                                                            ),
                                                            onPressed: () {
                                                              helper
                                                                  .removeToken();
                                                              Navigator
                                                                  .pushNamed(
                                                                      context,
                                                                      '/SignIn');
                                                            },
                                                            width: 120,
                                                          )
                                                        ],
                                                      ).show();
                                                    }
                                                  });
                                                } else {
                                                  print('object');
                                                  _showSnackBar(
                                                      "Enter the same password");
                                                }
                                              }
                                            },
                                            padding: EdgeInsets.all(0),
                                            color: appPrimaryColor,
                                            child: Text(
                                                AppTranslations.of(context)
                                                    .text('Continue'),
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 16)),
                                          ),
                                  ),
                          ]),
                    ),
                    SizedBox(
                      height: height * .04,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                            AppTranslations.of(context).text(
                                'By contining | confirm that i have read & agree to the'),
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Color(0xffA0A4AB),
                                fontSize: 12,
                                fontFamily: 'Monstserrat')),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                AppTranslations.of(context)
                                    .text('Term & condition'),
                                style: TextStyle(
                                    fontSize: 13,
                                    color: appPrimaryColor,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(AppTranslations.of(context).text('and'),
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      color: Color(0xffA0A4AB),
                                      fontSize: 13,
                                      fontFamily: 'Monstserrat')),
                              Text(
                                AppTranslations.of(context)
                                    .text('Privacy & policy'),
                                style: TextStyle(
                                    fontSize: 13,
                                    color: appPrimaryColor,
                                    fontWeight: FontWeight.bold),
                              ),
                            ]),
                      ],
                    ),
                    SizedBox(
                      height: height * .05,
                    ),
                  ],
                ),
              ]),
            ),
          ])),
    );
  }
}
