import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:two_bill/model/user.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/screens/wedget/show_%20SweetAlert.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/validators/email_validator.dart';
import 'package:two_bill/validators/phone_validator.dart';

class MyProfile extends StatefulWidget {
  @override
  _MyProfile createState() => _MyProfile();
}

class _MyProfile extends State<MyProfile> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
  String name='', phone='', email='';
  String selectedGender;
  File _imageFile;

  Api api;
   Helper helper= new Helper();
  Request request;
  String url;


  var headers;
 LocalLogin localLogin=new LocalLogin();
  User user;
  bool _isLoading = false;
    bool _isLoading2 = false;

  File image;
  final picker = ImagePicker();

  pickImageFromGallery(ImageSource source) async {
    final image = await picker.getImage(source: source);

    setState(() {
      this.image = File(image.path);
    });
  }

  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }
  _showLoading2() {
    setState(() {
      _isLoading2 = true;
    });
  }

  _hideLoading2() {
    setState(() {
      _isLoading2 = false;
    });
  }
connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  @override
  void initState() {
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
     loadDate();
  Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  handleValue(value) {

   if (value == "null" || value=="" || value ==null) {
      helper.removeToken();
       localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/Intro', (_) => false);
    }else{
     loadDate();
    }
  }
  loadDate() async {
    _showLoading();
    helper = new Helper();
     helper=new Helper();
   await helper.getsharedpref("user").then((dynamic muser){
     setState(() {
      user=User.fromJson(jsonDecode(muser));
      name=user.name;
      phone=user.phone;
      email=user.email;
      print('name   $name');
       print('phone   $phone');
       print('email   $email');
       
_hideLoading();
     });
    
   });
  
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: appfixedColor,
      appBar: CustomAppBar(
        title: "My Profile",
        scafoldkey: _scaffoldKey,
      ),
      drawer: AppDrawer(
        scafoldkey: _scaffoldKey,
      ),
      body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: _isLoading
              ? circularProgress()
              : ListView(shrinkWrap: true, children: [
                  Container(
                    height: height * .18,
                    color: appPrimaryColor,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: height * .03,
                        ),
                      
                        SizedBox(
                          width: width * .1,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                           
                            Text(
                           name,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 30,
                                  fontWeight: FontWeight.w900),
                            ),
                            SizedBox(
                              height: height * .01,
                            ),
                            Container(
                              width: width * .40,
                              child: RaisedButton(
                                color: Colors.white,
                                onPressed: () {
                                  Navigator.pushNamed(context, "/MyPackages");
                                },
                                child: Text(
                                    AppTranslations.of(context)
                                        .text("My Packages"),
                                    style: TextStyle(
                                      color: Color(0xff809bd0),
                                      fontSize: 15,
                                    )),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20)),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: height * .05,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: height * .07,
                  ),
                  Form(
                    key: globalKey,
                    child: Column(
                      children: [
                        Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: width * .07),
                          child: Container(
                            height: height * .1,
                            child: TextFormField(
                              keyboardType: TextInputType.text,
                              validator: (value) {
                                if (value.isEmpty)
                                  return AppTranslations.of(context)
                                      .text("value is empty");
                              },
                              onSaved: (val) => name = val,
                              initialValue: name,
                              decoration: InputDecoration(
                                prefixIcon: Image.asset(nameImage),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey[100]),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: width * .07),
                          child: Container(
                            height: height * .1,
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              validator: (value) {
                                if (value.isEmpty)
                                  return AppTranslations.of(context)
                                      .text("value is empty");

                                if (!value.contains(PhoneValidator.regex)) {
                                  return AppTranslations.of(context)
                                      .text("enter_valid_phone");
                                }
                              },
                              onSaved: (val) => phone = val,
                              initialValue: '$phone',
                              decoration: InputDecoration(
                                prefixIcon: Image.asset(phoneImage),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey[100]),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              EdgeInsets.symmetric(horizontal: width * .06),
                          child: Container(
                            height: height * .1,
                            child: TextFormField(
                              keyboardType: TextInputType.emailAddress,
                          
                              onSaved: (val) => email = val,
                              initialValue: email,
                              decoration: InputDecoration(
                                prefixIcon: Image.asset(emailImage),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey[100]),
                                ),
                              ),
                            ),
                          ),
                        ),
                      
                        _isLoading2?circularProgress():  Padding(
                            padding: EdgeInsets.only(
                                left: width * .02, right: width * .02),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Container(
                                  width: width * .35,
                                  child: RaisedButton(
                                    color: appPrimaryColor,
                                    onPressed: () {
                                      _submit();
                                    },
                                    child: Text(
                                        AppTranslations.of(context)
                                            .text("Edit"),
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15,
                                        )),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                  ),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                ])),
    );
  }

 

  _submit() async {
    _showLoading2();
    globalKey.currentState.save();
    if (globalKey.currentState.validate()) {
      

      api = new Api();
      request = new Request();
      url = "/api/profile/${user.id}";
      var body = <String, dynamic>{
        "name": name,
        "phone": phone,
        "email": email,
      };
      await request.getHeaader(context).then((dynamic dyHeaders) async {
        headers = dyHeaders;
      });
      print('headers $headers');
      api.putData(url, headers, context,body: body).then((dynamic response) async {
        final String res = response.body;
        final int statusCode = response.statusCode;
        if (statusCode == 401 ||
            statusCode == 400 ||
            statusCode == 403 ||
            statusCode == 501 ||
            statusCode == 404) {
          print(statusCode);
 _hideLoading2();
          Map<String, dynamic> json = jsonDecode(res);
          showAlert(context, jsonEncode(json["errors"]), "error", null);
        } else {
          _hideLoading2();
           Alert(
      context: context,
      type: AlertType.success,
      title:  AppTranslations.of(context).text("and") == "and"
            ? "Success"
            : "تم بنجاح",
      desc: AppTranslations.of(context).text("Profile data updated successfuly"),
      buttons: [
        DialogButton(
          child: Text(
            "Ok",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pushNamed(context, '/home'),
          width: 120,
        )
      ],
    ).show();
        
        }
      });
    }
  }
}
