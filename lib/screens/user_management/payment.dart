import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/model/subscribrion.dart';
import 'package:two_bill/screens/paymentt/billingDate.dart';
import 'package:two_bill/screens/paymentt/creditCard.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/style.dart';

class Payment extends StatefulWidget {
  Subscription subscription;
  String promocode;
  Payment({Key key, @required this.promocode, @required this.subscription})
      : super(key: key);
  @override
  _Payment createState() => _Payment(subscription, promocode);
}

class _Payment extends State<Payment> {
  Subscription subscription;
  String promocode;
  List<String> payment = [];
  _Payment(Subscription subscription, String promocode) {
    this.subscription = subscription;
    this.promocode = promocode;
  }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
  String cardHolderName;
  String cardNumber;
  String cvv;
  String expireDate;
  Helper helper=new Helper();
   LocalLogin localLogin=new LocalLogin();
   connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
    @override
  void initState() {
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
        payment.add("creditCard");
        payment.add("amaan");
        payment.add("fawery");
        payment.add("mahfaza");
  Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  handleValue(value) {

    if (value == "null" || value=="" || value ==null) {
      helper.removeToken();
       localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/SignIn', (_) => false);
    }else{
        // payment.add("creditCard");
        // payment.add("amaan");
        // payment.add("fawery");
        // payment.add("mahfaza");
    }
    
  }


  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: appfixedColor,
      appBar: CustomAppBar(
          title: "Service Payment",
          scafoldkey: _scaffoldKey,
          hasarowBack: false),
      drawer: AppDrawer(
        scafoldkey: _scaffoldKey,
      ),
          bottomNavigationBar:  customBottomNavigationBar(context, width, height, _scaffoldKey),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          width: width,
          height: height,
          padding: EdgeInsets.all(0),
          decoration: new BoxDecoration(
            color: appfixedColor,
            image: DecorationImage(
              image: AssetImage(
                background,
              ),
            ),
          ),
          
          child: Padding(
            padding: EdgeInsets.only(left: 20,right: 20,top: 50),
            child: GridView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: payment.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: .95, crossAxisCount: 2,crossAxisSpacing: 20,mainAxisSpacing: 20),
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>  payment[index]=="fawery"?CardInfo(
                              type: payment[index],
                              subscription: subscription,
                              promocode: promocode,
                              lang: AppTranslations.of(context).text("and")=="and"?"en":"ar",
                            ):BillingData(
                              type: payment[index],
                              subscription: subscription,
                              promocode: promocode,
                              lang: AppTranslations.of(context).text("and")=="and"?"en":"ar",
                            ),
                          ));
                    },
                    child: 
                            Container(
                              //height: 190,
                              decoration: new BoxDecoration(
                                color: appfixedColor,
                                image: DecorationImage(
                                 image:  AssetImage( getImage(payment[index])),
                              
                                    fit: BoxFit.fill),
                              ),
                            ),

                  );
                }),
          ),
        ),
      ),
    );
  }

  String getImage(String payment) {
    if (payment == "creditCard") {
      return "assets/image/card1.png";
    } else if (payment == "amaan") {
      return "assets/image/amaan.jpg";
    } else if (payment == "fawery") {
      return "assets/image/fawey.jpg";
    } else if (payment == "mahfaza") {
      return "assets/image/mahfaza.jpg";
    }
  }
}
