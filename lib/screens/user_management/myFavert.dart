import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/model/favort.dart';
import 'package:two_bill/model/service.dart';
import 'package:two_bill/screens/Home/subscribePackage.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/model/package.dart';
import 'package:two_bill/utils/style.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';

class MyFavortScrean extends StatefulWidget {
  @override
  _MyFavortScrean createState() => _MyFavortScrean();
}

class _MyFavortScrean extends State<MyFavortScrean> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Api api;
  Request request;
LocalLogin localLogin=new LocalLogin();
  List<Favorit> favorits;
  String url;
  var headers;

   Helper helper= new Helper();
  bool _isLoading = false;
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }
connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  @override
  void initState() {
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
     
  Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  handleValue(value) {
 print('value      $value');
    if (value == "null" || value=="" || value ==null) {
      helper.removeToken();
       localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/Intro', (_) => false);
    }else{
      loadDate();
    }
  }
  loadDate() async {
    _showLoading();

    api = new Api();
    request = new Request();
    url = "/api/favorites/listUserFav";
    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    print(headers);

    await api
        .getData(
      url,
      headers,
      context
    )
        .then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print(statusCode);
        _hideLoading();
      } else {
        Map<String, dynamic> json = jsonDecode(res);
        print(json['data']['data']);
        setState(() {
          favorits = Favorit.decode(jsonEncode(json['data']['data']));
        });
        _hideLoading();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: appfixedColor,
       appBar: CustomAppBar(title:"My Favorrites",scafoldkey:_scaffoldKey ,),
     drawer: AppDrawer(
        scafoldkey: _scaffoldKey,
      ),
      bottomNavigationBar:
            customBottomNavigationBar(context, width, height, _scaffoldKey),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: ListView(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: [
                Padding(
                  padding: EdgeInsets.all(width * .05),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(
                        Radius.circular(20),
                      ),
                    
                    ),
                    child: Container(
                        padding: EdgeInsets.all(width * .05),
                        child: Column(children: [
                          Row(children: [
                            Image.asset(star),
                            SizedBox(
                              width: width * .02,
                            ),
                            Text(
                              AppTranslations.of(context).text('My Favorrites'),
                              style: TextStyle(
                                color: Color(0xff5178c2),
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ]),
                          SizedBox(
                            height: height * .02,
                          ),
                          _isLoading
                              ? circularProgress()
                              : Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width * .01),
                                  child: favorits.length > 0
                                      ? ListView.builder(
                                          shrinkWrap: true,
                                          itemCount: favorits.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return Dismissible(
                                              key: ObjectKey(
                                                  favorits[index].package.id),
                                              onDismissed: (direction) async {
                                                 url = "/api/packages/addFavRequest";
                                               var body ={
                                                 'package_id': favorits[index].package.id
                                               };
                                                // print(body);
                                                await request.getHeaader(context).then(
                                                    (dynamic dyHeaders) async {
                                                  headers = dyHeaders;
                                                });
                                                print(headers);

                                                await api
                                                    .postData(
                                                  url,
                                                  headers,
                                                  context,
                                                  body: body
                                                )
                                                    .then((dynamic
                                                        response) async {
                                                  final String res =
                                                      response.body;
                                                  final int statusCode =
                                                      response.statusCode;
                                                  if (statusCode == 401 ||
                                                      statusCode == 400 ||
                                                      statusCode == 403 ||
                                                      statusCode == 501 ||
                                                      statusCode == 404) {
                                                    print(statusCode);
                                                  
                                                  } else {
                                                    Map<String, dynamic> json =
                                                        jsonDecode(res);
                                                    print(json);
                                                   
                                                  }
                                                });
                                              },
                                              child: Card(
                                                color: appfixedColor,
                                                child: ListTile(
                                                    onTap: () {
                                                      Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder: (context) =>
                                                                SubscribePackage(
                                                              package: favorits[
                                                                      index]
                                                                  .package,
                                                                 
                                                            ),
                                                          ));
                                                    },
                                                    leading: Image.asset(star),
                                                    title: AppTranslations.of(
                                                                    context)
                                                                .text("and") ==
                                                            "and"
                                                        ? Text(
                                                            favorits[index]
                                                                .package
                                                                .nameEn,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black),
                                                          )
                                                        : Text(
                                                            favorits[index]
                                                                .package
                                                                .nameAr,
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black),
                                                          )),
                                              ),
                                            );
                                          })
                                      : Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                              SizedBox(
                                                height: height * .01,
                                              ),
                                              Text(
                                                AppTranslations.of(context).text(
                                                    'No favorites avaliable'),
                                                style: TextStyle(
                                                  color: Color(0xffd0d0d0),
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ]),
                                ),
                          SizedBox(
                            height: height * .1,
                          ),
                        ])),
                  ),
                )
              ]),
        ));
  }
}
