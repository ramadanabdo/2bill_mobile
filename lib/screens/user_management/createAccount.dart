import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/screens/user_management/signIn.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/style.dart';

class CreateAccount extends StatefulWidget {
  @override
  _CreateAccount createState() => _CreateAccount();
}

class _CreateAccount extends State<CreateAccount> {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: ListView(shrinkWrap: true, children: [
           Container(
          width: width,
          height: height,
          padding: EdgeInsets.all(0),
          decoration: new BoxDecoration(
            color: appfixedColor,
            image: DecorationImage(
                image: AssetImage(
                  create_account,
                ),
                fit: BoxFit.cover,
            ),
          ),
          child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
            SizedBox(
                height: height * .3,
            ),
            
                  Text(AppTranslations.of(context).text('2Bill'),
                      textAlign: TextAlign.start,
                      style: bigStyle()),
                 
                 
  
            SizedBox(
                height: height * .12,
            ),
            
              Padding(
                padding: EdgeInsets.symmetric(horizontal: width * .04),
                child: ButtonTheme(
                  height: height * .08,
                  minWidth: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    elevation: 0.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onPressed: () {
                    
                      Navigator.pushNamed(
                          context, '/SignIn');
                    },
                    padding: EdgeInsets.all(0),
                    color: appPrimaryColor,
                    child: Text(AppTranslations.of(context).text('Login'),
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16)),
                  ),
                ),
            ),
            SizedBox(
                height: height * .02,
            ),
            Padding(
                padding: EdgeInsets.symmetric(horizontal: width * .04),
                child: ButtonTheme(
                  height: height * .08,
                  minWidth: MediaQuery.of(context).size.width,
                  child: RaisedButton(
                    elevation: 0.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    onPressed: () {
                    
                      Navigator.pushNamed(
                          context, '/SignUp');
                    },
                    padding: EdgeInsets.all(0),
                    color: appPrimaryColor,
                    child: Text(AppTranslations.of(context).text('Create Account'),
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16)),
                  ),
                ),
            ),
           
            
        
        
           ]),
           )
        ])
      )
    );
  }
}
