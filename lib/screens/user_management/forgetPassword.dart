import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/screens/wedget/show_%20SweetAlert.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/utils/style.dart';
import 'package:two_bill/screens/wedget/CustomTextField.dart';

class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPassword createState() => _ForgetPassword();
}

class _ForgetPassword extends State<ForgetPassword> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
  String phone;
   LocalLogin localLogin=new LocalLogin();
  Api api;
   Helper helper= new Helper();
  Request request;
  String url;
  var headers;
  bool _isLoading = false;
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }
connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  @override
  void initState() {
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
 
    
  }
 
   
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
        key: _scaffoldKey,
        appBar: CustomAppBar(
          title: "Forget Password",
          scafoldkey: _scaffoldKey,
          drwer: false,
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: ListView(shrinkWrap: true, children: [
              Container(
                  width: width,
                  height: height,
                  padding: EdgeInsets.all(0),
                  decoration: new BoxDecoration(
                    color: appfixedColor,
                    image: DecorationImage(image: AssetImage(background),fit: BoxFit.cover,),
                    
                  ),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(
                              left: width * .08,
                              right: width * .08,
                              top: height * .03),
                          child:
                              AppTranslations.of(context).text("and") == "and"
                                  ? Image.asset(forgetEn)
                                  : Image.asset(forgetAr),
                        ),
                        Padding(
                            padding: EdgeInsets.only(
                                left: width * .08,
                                right: width * .08,
                                top: height * .02),
                            child: ListView(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                children: [
                                  Text(
                                      AppTranslations.of(context)
                                          .text('Forget Password'),
                                      textAlign: TextAlign.start,
                                      style: big2Style()),
                                  SizedBox(
                                    height: height * .02,
                                  ),
                                  SizedBox(
                                    height: height * .08,
                                  ),
                                  Form(
                                      key: globalKey,
                                      child: ListView(
                                          shrinkWrap: true,
                                          physics:
                                              NeverScrollableScrollPhysics(),
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: width * .02),
                                              child: CustomTextField(
                                                icon:
                                                    AppTranslations.of(context)
                                                                .text("and") ==
                                                            "and"
                                                        ? "phone2"
                                                        : "phoneAr",
                                                hint:
                                                    AppTranslations.of(context)
                                                        .text("Phone"),
                                                onclick: (value) {
                                                  phone = value;
                                                },
                                                isEmail: false,
                                                isNumber: true,
                                              ),
                                            ),
                                            SizedBox(
                                              height: height * .05,
                                            ),
                                           
                                            ButtonTheme(
                                              height: height * .08,
                                              minWidth: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: _isLoading
                                                  ? circularProgress()
                                                  : RaisedButton(
                                                      elevation: 0.0,
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                      ),
                                                      onPressed: () async {
                                                        globalKey.currentState
                                                            .save();
                                                        print(phone);

                                                        if (globalKey
                                                            .currentState
                                                            .validate()) {
                                                          _showLoading();
                                                          // Navigator.pushNamed(context, '/Login');
                                                          var body =
                                                              <String, dynamic>{
                                                            'phone': phone,
                                                          };
                                                          api = new Api();
                                                          request =
                                                              new Request();
                                                          url =
                                                              "/api/auth/forgot";
                                                          await request
                                                              .getHeaader(
                                                                  context)
                                                              .then((dynamic
                                                                  dyHeaders) async {
                                                            headers = dyHeaders;
                                                          });
                                                          print(
                                                              'headers $headers');
                                                          api
                                                              .postData(
                                                                  url, headers,
                                                                  context,
                                                                  body: body)
                                                              .then((dynamic
                                                                  response) async {
                                                            final String res =
                                                                response.body;
                                                            final int
                                                                statusCode =
                                                                response
                                                                    .statusCode;
                                                            if (statusCode == 401 ||
                                                                statusCode ==
                                                                    400 ||
                                                                statusCode ==
                                                                    403 ||
                                                                statusCode ==
                                                                    501 ||
                                                                statusCode ==
                                                                    404) {
                                                              print(statusCode);
                                                              _hideLoading();
                                                              showAlert(
                                                                  context,
                                                                  "unexpected error",
                                                                  "error",
                                                                  null);
                                                            } else {
                                                              Alert(
                                                                context:
                                                                    context,
                                                                type: AlertType
                                                                    .success,
                                                                title: AppTranslations.of(context)
                                                                            .text("and") ==
                                                                        "and"
                                                                    ? "Success"
                                                                    : "تم بنجاح",
                                                                desc: AppTranslations.of(
                                                                        context)
                                                                    .text(
                                                                        "Message sent successfuly"),
                                                                buttons: [
                                                                  DialogButton(
                                                                    child: Text(
                                                                      "Ok",
                                                                      style: TextStyle(
                                                                          color: Colors
                                                                              .white,
                                                                          fontSize:
                                                                              20),
                                                                    ),
                                                                    onPressed: () =>
                                                                        Navigator.pushNamed(
                                                                            context,
                                                                            '/prefyCod'),
                                                                    width: 120,
                                                                  )
                                                                ],
                                                              ).show();
                                                            }
                                                          });
                                                        }
                                                      },
                                                      padding:
                                                          EdgeInsets.all(0),
                                                      color: appPrimaryColor,
                                                      child: Text(
                                                          AppTranslations.of(
                                                                  context)
                                                              .text(
                                                                  'Send Code'),
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 16)),
                                                    ),
                                            ),
                                          ])),
                                  SizedBox(
                                    height: height * .04,
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                          AppTranslations.of(context).text(
                                              'By contining | confirm that i have read & agree to the'),
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              color: Color(0xffbcbcbc),
                                              fontSize: 11,
                                              fontFamily: 'Monstserrat')),
                                      Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              AppTranslations.of(context)
                                                  .text('Term & condition'),
                                              style: TextStyle(
                                                  fontSize: 13,
                                                  color: appPrimaryColor,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                                AppTranslations.of(context)
                                                    .text('and'),
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                    color: Color(0xff555555),
                                                    fontSize: 13,
                                                    fontFamily: 'Monstserrat')),
                                            Text(
                                              AppTranslations.of(context)
                                                  .text('Privacy & policy'),
                                              style: TextStyle(
                                                  fontSize: 13,
                                                  color: appPrimaryColor,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ]),
                                    ],
                                  ),
                                  SizedBox(
                                    height: height * .05,
                                  ),
                                ])),
                      ])),
            ])));
  }
}
