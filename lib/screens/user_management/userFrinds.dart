import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/model/subscribrion.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/application.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/request.dart';

class UserFriends extends StatefulWidget {
  Subscription subsciption;
  UserFriends({Key key, @required this.subsciption}) : super(key: key);
  @override
  _UserFriends createState() => _UserFriends(subsciption);
}

class _UserFriends extends State<UserFriends> {
  Subscription subsciption;
  _UserFriends(Subscription subsciption) {
    this.subsciption = subsciption;
  }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Api api;
  Request request;

  String url;
  var headers;

  List<Subscription> subsciptions = [];

   Helper helper= new Helper();
  bool _isLoading = false;
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
   print('fkjdgfkdgjk   ${subsciption.id}');
    loadDate();
  }

  loadDate() async {
    _showLoading();
  
    api = new Api();
    request = new Request();
    url = "/api/subscriptions/points/${subsciption.id}";
    print(url);
   
    Map<String, dynamic> body = {
      'package_id': subsciption.package.id,
    };
    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    print(headers);

    await api.getData(url, headers,context).then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print(statusCode);
        _hideLoading();
      } else {
        Map<String, dynamic> json = jsonDecode(res);
        print(json['data']['data']);
        setState(() {
          subsciptions = Subscription.decode(jsonEncode(json['data']['data']));
        });
        _hideLoading();
        print(subsciptions.length);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: appfixedColor,
      appBar: CustomAppBar(
        title: "Friends",
        title2: subsciption.user.name,
        scafoldkey: _scaffoldKey,
      ),
       drawer: AppDrawer(
        scafoldkey: _scaffoldKey,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: _isLoading
            ? circularProgress()
            : 
            
            Padding(
              padding: EdgeInsets.all(0),
                          child: subsciptions.length>0? ListView.builder(
                  shrinkWrap: true,
                  itemCount: subsciptions.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                        color: appfixedColor,
                        child: ListTile(
                          onTap: () {},
                          leading: Image.asset(userFriend),
                          title: Row(
                            children: [
                                Text(
                              subsciptions[index].user.name,
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w900,
                                color:Color(0xff6b6b6b)),
                            ),
                            SizedBox(
                              width: width*.47,
                            ),
                             Text(
                             AppTranslations.of(context).text("Point"),
                              style: TextStyle(color: Color(0xff8b8b8b)),
                            ),
                          
                            ]),
                          subtitle: Text(
                            subsciptions[index].user.email,
                            style: TextStyle(color: Color(0xff8b8b8b)),
                          ),
                        ));
                  }):Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                      SizedBox(
                                        height: height * .2,
                                      ),
                                      Text(
                                        AppTranslations.of(context)
                                            .text('No Friends avaliable'),
                                        style: TextStyle(
                                          color: Color(0xffd0d0d0),
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ]),
            ),
      ),
    );
  }
}
