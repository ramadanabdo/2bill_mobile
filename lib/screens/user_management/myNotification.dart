import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/model/notification.dart';
import 'package:two_bill/model/user.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';

class NotificationScreen extends StatefulWidget {
  _NotificationScreen createState() => _NotificationScreen();
}

class _NotificationScreen extends State<NotificationScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
   LocalLogin localLogin=new LocalLogin();
  Api api;
  Request request;
  String url;
  var headers;
  var body;
  List<NotificationModel> notifications = [];
  User user;
   Helper helper= new Helper();
  bool _isLoading = false;
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }
connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  @override
  void initState() {
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
  Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  handleValue(value) {

    if (value == "null" || value=="" || value ==null) {
      helper.removeToken();
       localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/Intro', (_) => false);
    }else{
      loadDate();
    }
  }

  loadDate() async {
    helper = new Helper();
    _showLoading();

    api = new Api();
    request = new Request();
    Future<User> future = helper.getUserData(context);
    await future.then((myuser) {
      setState(() {
        user = myuser;
        _hideLoading();
        //
      });
    });

    body = request.getBody(30, 0, "id", "ASC", "all", "false", "false",
        "user_id", "=", user.id.toString());
    print(body);
    url = "/api/notifications$body";
    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    print(headers);

    await api
        .getData(
      url,
      headers,
      context
    )
        .then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print(statusCode);
        _hideLoading();
      } else {
        Map<String, dynamic> json = jsonDecode(res);
        print(json['data']['data']);
        setState(() {
          notifications =
              NotificationModel.decode(jsonEncode(json['data']['data']));
        });
        _hideLoading();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: appfixedColor,
      appBar: CustomAppBar(
        title: "Notification",
        scafoldkey: _scaffoldKey,
        hasarowBack: true,
      ),
      drawer: AppDrawer(
        scafoldkey: _scaffoldKey,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: _isLoading
            ? circularProgress()
            : Padding(
                padding: EdgeInsets.all(0),
                child: notifications.length > 0
                    ? ListView.builder(
                        shrinkWrap: true,
                        itemCount: notifications.length,
                        itemBuilder: (context, index) {
                          return Container(
                            height: height * .12,
                            padding: EdgeInsets.only(top: height * 0.01),
                            child: Material(
                              shadowColor: appfixedColor,
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: width * .01,
                                  ),
                                  Image.asset(
                                      getImage(notifications[index].type)),
                                  SizedBox(
                                    width: width * .02,
                                  ),
                                  Expanded(
                                    child: Text(
                                      notifications[index].message,
                                      style: TextStyle(
                                        color: appPrimaryColor,
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        })
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                            SizedBox(
                              height: height * .05,
                            ),
                            Center(
                              child: Text(
                                AppTranslations.of(context)
                                    .text('No Service avaliable'),
                                style: TextStyle(
                                  color: Color(0xffd0d0d0),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ]),
              ),
      ),
    );
  }

  String getImage(String type) {
    String image;
    if (type == "info") {
      image = info;
    }
    if (type == "reminder") {
      image = reminder;
    }
    if (type == "alert") {
      image = alert;
    }
    return image;
  }
}
