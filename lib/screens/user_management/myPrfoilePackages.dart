import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/model/package.dart';
import 'package:two_bill/model/subscribrion.dart';
import 'package:two_bill/model/user.dart';
import 'package:two_bill/screens/Home/myPackageDetails.dart';
import 'package:two_bill/screens/Home/subscribePackage.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';

class MyPackages extends StatefulWidget {
    Package package;
  bool hasArowBack;
  MyPackages({Key key, @required this.package, this.hasArowBack})
      : super(key: key);
  @override
  _MyPackages createState() => _MyPackages(package, hasArowBack);
}

class _MyPackages extends State<MyPackages> {
   Package package;
  bool hasArowBack;
  _MyPackages(Package package, bool hasArowBack) {
    this.package = package;
    this.hasArowBack = hasArowBack;
  }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  User user;
   Helper helper= new Helper();
  var headers;
  List<Subscription> subscriptions = [];
 LocalLogin localLogin=new LocalLogin();
  Api api;
  Request request;
  String url;
  bool _isLoading = false;
  var body;
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }
connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
   @override
  void initState() {
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
    if(hasArowBack==null)
     hasArowBack=true;
  Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  handleValue(value) {

    if (value == "null" || value=="" || value ==null) {
      helper.removeToken();
       localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/Intro', (_) => false);
    }else{
      loadDate();
    }
  }
  loadDate() async {
    _showLoading();
    helper = new Helper();
    request = new Request();
    Future<User> future = helper.getUserData(context);
    await future.then((myuser) {
      setState(() {
        user = myuser;
        print(user.id);
      });
    });

    body = request.getBody(30, 0, "id", "ASC", "all", "false", "true",
     
 
          "user_id", "=", user.id.toString());
    print(body);
    api = new Api();
    request = new Request();
    url = "/api/subscriptions$body";

    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    print(headers);

    await api
        .getData(
      url,
      headers,
      context
    )
        .then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print(statusCode);
        _hideLoading();
      } else {
        Map<String, dynamic> json = jsonDecode(res);
        print(json['data']['data']);
        setState(() {
          subscriptions = Subscription.decode(jsonEncode(json['data']['data']));
        });
        _hideLoading();
        print(Subscription.encode(subscriptions).toString());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: appfixedColor,
        appBar: CustomAppBar(
          title: "My Profile",
          scafoldkey: _scaffoldKey,
            hasarowBack: hasArowBack,
        ),
        drawer: AppDrawer(
          scafoldkey: _scaffoldKey,
        ),
        bottomNavigationBar:
            customBottomNavigationBar(context, width, height, _scaffoldKey),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Container(
            width: width,
            height: height,
            padding: EdgeInsets.all(0),
            decoration: new BoxDecoration(
              color: appfixedColor,
              image: DecorationImage(
                  image: AssetImage(profilePackageEn), fit: BoxFit.cover),
            ),
            child: _isLoading
                ? circularProgress()
                :Column(
                     
                        children: [
                          Container(
                            height: height * .11,
                            color: appPrimaryColor,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  user.name,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 30,
                                      fontWeight: FontWeight.w900),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: height * .33,
                          ),
                          subscriptions.length > 0
                              ?  Expanded(
                                          child: SingleChildScrollView(
                                                              child: ListView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: subscriptions.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Container(
                                        padding: EdgeInsets.symmetric(
                                          horizontal: width * .05,
                                        ),
                                        child: Card(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          color: Colors.white,
                                          child: ListTile(
                                            onTap: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        MyPackageDetails(
                                                            subsciption:
                                                                subscriptions[
                                                                    index]),
                                                  ));
                                            },
                                            title: AppTranslations.of(context)
                                                        .text("and") ==
                                                    "and"
                                                ? Text(
                                                    subscriptions[index]
                                                            .package
                                                            .service
                                                            .provider
                                                            .nameEn +
                                                        " - " +
                                                        subscriptions[index]
                                                            .package
                                                            .service
                                                            .nameEn +
                                                        " - " +
                                                        subscriptions[index]
                                                            .package
                                                            .nameEn,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  )
                                                : Text(
                                                    subscriptions[index]
                                                            .package
                                                            .service
                                                            .provider
                                                            .nameAr +
                                                        " - " +
                                                        subscriptions[index]
                                                            .package
                                                            .service
                                                            .nameAr +
                                                        " - " +
                                                        subscriptions[index]
                                                            .package
                                                            .nameAr,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                            subtitle: AppTranslations.of(context)
                                                        .text("and") ==
                                                    "and"
                                                ? Row(
                                                    children: [
                                                      SizedBox(
                                                        width: width * .02,
                                                      ),
                                                      Text(
                                                          '${subscriptions[index].package.priceAfterDiscount}EG/${subscriptions[index].package.type}',
                                                          style: TextStyle(
                                                            color:
                                                                Color(0xff464646),
                                                            fontSize: 16,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                          )),
                                                      SizedBox(
                                                        width: width * .04,
                                                      ),
                                                    ],
                                                  )
                                                : Row(children: [
                                                    SizedBox(
                                                      width: width * .04,
                                                    ),
                                                    subscriptions[index]
                                                                .package
                                                                .type ==
                                                            "monthly"
                                                        ? Text(
                                                            '${subscriptions[index].package.priceAfterDiscount}جنيه/شهريا',
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xff464646),
                                                              fontWeight:
                                                                  FontWeight.bold,
                                                              fontSize: 14,
                                                            ))
                                                        : Text(
                                                            '${subscriptions[index].package.priceAfterDiscount}جنيه/سنوي',
                                                            style: TextStyle(
                                                              color: Color(
                                                                  0xff464646),
                                                              fontWeight:
                                                                  FontWeight.bold,
                                                              fontSize: 14,
                                                            )),
                                                    SizedBox(
                                                      width: width * .02,
                                                    ),
                                                  ]),
                                          ),
                                        ),
                                      );
                                    }),
                              )
                              ): Container(),
                              
                        ]),
                  ),
          ));
        
  }
}
