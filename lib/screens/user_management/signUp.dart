import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:two_bill/model/country.dart';
import 'package:two_bill/screens/wedget/show_%20SweetAlert.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/style.dart';
import 'package:two_bill/screens/wedget/CustomTextField.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

class SignUp extends StatefulWidget {
  @override
  _SignUp createState() => _SignUp();
}
 
class _SignUp extends State<SignUp> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
  DateTime selectedDate = DateTime.now();

  String fullName;
  String email;
  String password;
  String repassword;
  String phone;

  bool _obscureText1 = true;
  bool _obscureText2 = true;
  var headers;
  String body;

  Api api;
  Request request;
  String url;
  bool _isLoading = false;
  Helper helper = new Helper();
    @override
  initState() {
    super.initState();
       Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
    Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  handleValue(value) {

        if (value == "null" || value=="" || value ==null) {
        
        }else{
          Navigator.pushNamedAndRemoveUntil(context, '/home', (_) => false);
        }
  }

  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(AppTranslations.of(context).text(text))));
  }

  registerUser() async {
    if (password == repassword) {
      _showLoading();
      // Navigator.pushNamed(context, '/Login');
      var body = <String, dynamic>{
        'name': fullName,
        'phone': phone,
        'password': password,
        'password_confirmation': repassword,
      };
      api = new Api();
      request = new Request();
      url = "/api/auth/register";
      await request.getHeaader(context).then((dynamic dyHeaders) async {
        headers = dyHeaders;
      });
      await api
          .postData(url, headers,context, body: body)
          .then((dynamic response) async {
        final String res = response.body;
        final int statusCode = response.statusCode;
        if (statusCode == 401 ||
            statusCode == 400 ||
            statusCode == 403 ||
            statusCode == 501 ||
            statusCode == 404) {
          print(statusCode);
          _hideLoading();
          Map<String, dynamic> json = jsonDecode(res);
          if (json['errors']['name'] != null) {
            showAlert(context, json["errors"]["name"][0], "error", null);
          } else if (json['errors']['phone'] != null) {
            showAlert(context, json["errors"]["phone"][0], "error", null);
          } else if (json['errors']['email'] != null) {
            showAlert(context, json["errors"]["email"][0], "error", null);
          }
        } else {
          _hideLoading();
          Alert(
            context: context,
            type: AlertType.success,
            title: AppTranslations.of(context).text("and") == "and"
                ? "Success"
                : "تم بنجاح",
            desc: AppTranslations.of(context)
                .text("Register user successfuly go to login page"),
            buttons: [
              DialogButton(
                child: Text(
                  "Ok",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () => Navigator.pushNamed(context, '/SignIn'),
                width: 120,
              )
            ],
          ).show();
        }
      });
    } else {
      _showSnackBar("Enter the same password");
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      key: _scaffoldKey,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: ListView(shrinkWrap: true, children: [
          Container(
              width: width,
              height: height,
              padding: EdgeInsets.all(0),
              decoration: new BoxDecoration(
                color: appfixedColor,
                image: DecorationImage(
                  image: AssetImage(
                    background,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
              child: Padding(
                padding: EdgeInsets.only(
                    left: width * .05, right: width * .05, top: height * .1),
                child: ListView(
                  shrinkWrap: true,
                  children: [
                    Text(AppTranslations.of(context).text('Sign Up'),
                        textAlign: TextAlign.start, style: big2Style()),
                    SizedBox(
                      height: height * .04,
                    ),
                    Form(
                      key: globalKey,
                      child: ListView(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          children: <Widget>[
                            CustomTextField(
                              hint:
                                  AppTranslations.of(context).text("Full name"),
                              onclick: (value) {
                                fullName = value;
                              },
                              isEmail: false,
                            ),
                            SizedBox(
                              height: height * .02,
                            ),
                            CustomTextField(
                              hint: AppTranslations.of(context)
                                  .text("Phone Number"),
                              onclick: (value) {
                                phone = value;
                              },
                              // container: countryDropDown,

                              isNumber: true,
                            ),
                            SizedBox(
                              height: height * .02,
                            ),
                            TextFormField(
                              validator: (value) {
                                  if (value.length < 8) {
                                  return AppTranslations.of(context).text(
                                      "Password contain at least 8 letters");
                                }
                              },
                              obscureText: _obscureText1,
                              onSaved: (val) => password = val,
                              decoration: InputDecoration(
                                fillColor: Colors.white,
                                filled: true,
                                hintText: AppTranslations.of(context)
                                    .text('Password'),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                    )),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                    )),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                    )),
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    // Based on passwordVisible state choose the icon
                                    _obscureText1
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: passwordColor,
                                  ),
                                  onPressed: () {
                                    // Update the state i.e. toogle the state of passwordVisible variable
                                    setState(() {
                                      _obscureText1 = !_obscureText1;
                                    });
                                  },
                                ),
                              ),
                            ),
                            SizedBox(
                              height: height * .02,
                            ),
                            TextFormField(
                              validator: (value) {
                                if (value.isEmpty)
                               
                                        if (value.length < 8) {
                                  return AppTranslations.of(context).text(
                                      "Password contain at least 8 letters");
                                }
                              },
                              obscureText: _obscureText2,
                              onSaved: (val) => repassword = val,
                              decoration: InputDecoration(
                                fillColor: Colors.white,
                                filled: true,
                                hintText: AppTranslations.of(context)
                                    .text('Re-enter Password'),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                    )),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                    )),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                    )),
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    // Based on passwordVisible state choose the icon
                                    _obscureText2
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: passwordColor,
                                  ),
                                  onPressed: () {
                                    // Update the state i.e. toogle the state of passwordVisible variable
                                    setState(() {
                                      _obscureText2 = !_obscureText2;
                                    });
                                  },
                                ),
                              ),
                            ),
                            /* CustomTextField(
                              hint:
                                  AppTranslations.of(context).text("Password"),
                              onclick: (value) {
                                password = value;
                              },
                              isEmail: false,
                              issecure: true,
                            ),
                            SizedBox(
                              height: height * .02,
                            ),
                            CustomTextField(
                              hint: AppTranslations.of(context)
                                  .text("Re-enter Password"),
                              onclick: (value) {
                                repassword = value;
                              },
                              isEmail: false,
                              issecure: true,
                            ),*/
                            SizedBox(
                              height: height * .03,
                            ),
                            ButtonTheme(
                              height: height * .08,
                              minWidth: MediaQuery.of(context).size.width,
                              child: _isLoading
                                  ? circularProgress()
                                  : RaisedButton(
                                      elevation: 0.0,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10),
                                      ),
                                      onPressed: () async {
                                        globalKey.currentState.save();

                                        if (globalKey.currentState.validate()) {
                                          registerUser();
                                        }
                                      },
                                      padding: EdgeInsets.all(0),
                                      color: appPrimaryColor,
                                      child: Text(
                                          AppTranslations.of(context)
                                              .text('Next'),
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16)),
                                    ),
                            ),
                          ]),
                    ),
                    SizedBox(
                      height: height * .01,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          AppTranslations.of(context)
                              .text('Already have an account ?'),
                          style: TextStyle(
                              color: Color(0xffA0A4AB),
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, '/SignIn');
                          },
                          child: Text(
                            AppTranslations.of(context).text('Login'),
                            style: TextStyle(
                                fontSize: 18,
                                color: appPrimaryColor,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: height * .04,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                            AppTranslations.of(context).text(
                                'By contining | confirm that i have read & agree to the'),
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Color(0xffA0A4AB),
                                fontSize: 10,
                                fontFamily: 'Monstserrat')),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                AppTranslations.of(context)
                                    .text('Term & condition'),
                                style: TextStyle(
                                    fontSize: 13,
                                    color: appPrimaryColor,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(AppTranslations.of(context).text('and'),
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                      color: Color(0xffA0A4AB),
                                      fontSize: 13,
                                      fontFamily: 'Monstserrat')),
                            /*  InkWell(
                                
                                onTap: ()async{
                                  if (await UrlLauncher.canLaunch("https://2bill.net/en/privacy.html")) {
                                               await UrlLauncher.launch("https://2bill.net/en/privacy.html");
                                              } else {
                                                _showSnackBar("Could not launch");
                                              }
                                 
                                },
                                                              child:*/ Text(
                                  AppTranslations.of(context)
                                      .text('Privacy & policy'),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: appPrimaryColor,
                                      fontWeight: FontWeight.bold),
                                ),
                         //     ),
                            ]),
                      ],
                    ),
                    SizedBox(
                      height: height * .05,
                    ),
                  ],
                ),
              )),
        ]),
      ),
    );
  }
}
