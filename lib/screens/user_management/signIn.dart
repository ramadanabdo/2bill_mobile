import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:two_bill/model/user.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/style.dart';
import 'package:two_bill/screens/wedget/CustomTextField.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import '../wedget/show_ SweetAlert.dart';

class SignIn extends StatefulWidget {
  @override
  _SignIn createState() => _SignIn();
}

class _SignIn extends State<SignIn> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
  String phone;
  String password;
  Api api;
  Helper helper = new Helper();
  LocalLogin localLogin;
  Request request;
  bool _obscureText1=true;
  String url;
  var headers;
  bool _isLoading = false;
   @override
  initState() {
    super.initState();
       Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
    
    Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  handleValue(value) {

    if (value == "null" || value=="" || value ==null) {
      
     
    }else{
      print(value);
      Navigator.pushNamedAndRemoveUntil(context, '/home', (_) => false);
    }
  }
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(AppTranslations.of(context).text(text))));
  }

  loginAction() async {
    var body = <String, dynamic>{
      'phone': phone,
      'password': password,
    };
    api = new Api();
    localLogin=new LocalLogin();
    request = new Request();
    helper=new Helper();
    url = "/api/auth/login";
    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    print('headers $headers');
    api.postData(url, headers,context, body: body).then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404  ||
           statusCode==500) {
        _hideLoading();
        print('statusCode  $statusCode');
          Alert(
      context: context,
      type: AlertType.error,
      title:  AppTranslations.of(context).text("and") == "and" ? "Eror" : "خطا",
      desc: AppTranslations.of(context).text("These Credintial doesnot match our Records"),
      buttons: [
        DialogButton(
          child: Text(
            "Ok",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          width: 120,
        )
      ],
    ).show();
      
       
      } else {
          print('statusCode  $statusCode');
        Map<String, dynamic> json = jsonDecode(res);
        print(json['access_token']);
      

        await helper.sharedpref("userToken", json['access_token']);
        print("user    ");
         Future<User> future = helper.getUserData(context);
    future.then((myuser) {
      setState(() {
     
         helper.sharedpref("user",myuser);
        //
      });
    });
            _hideLoading();
            localLogin.encrypt(phone + ' ' + password).then((var encr) {
                localLogin.writeData(encr);
                print(encr);
                localLogin.readData().then((var decr) {
                  localLogin.decrypt(decr).then((var data) {
                    print(data);
                  });
                });
              });
            Navigator.pushNamed(context, '/home');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: ListView(
          shrinkWrap: true,
          children: [
           Container(
                  width: width,
                  height: height,
                  padding: EdgeInsets.all(0),
                  decoration: new BoxDecoration(
                    color: appfixedColor,
                    image: DecorationImage(
                      image: AssetImage(background),
                      fit: BoxFit.cover,
                    ),
                    
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                       Padding(
                        padding: EdgeInsets.only(
                            left: width * .01,
                            right: width * .01,
                            top: height * .03),
                            child:AppTranslations.of(context).text("and")=="and"?Image.asset(loginEn):Image.asset(loginAr),
                       ),
                  
          Padding(
                padding: EdgeInsets.only(
                    left: width * .05, right: width * .05, top: height * .01),
                child: ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                       Text(AppTranslations.of(context).text('Sign In'),
                        textAlign: TextAlign.start, style: big2Style()),
                       SizedBox(
                        height: height * .03,
                      ),
                      Form(
                        key: globalKey,
                        child: ListView(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: width * .02),
                                child: CustomTextField(
                                  icon: AppTranslations.of(context).text("and")=="and"? "phone2":"phoneAr",
                                  hint:
                                      AppTranslations.of(context).text("Phone"),
                                  onclick: (value) {
                                    phone = value;
                                  },
                                  isEmail: false,
                                   isNumber: true,
                                ),
                              ),
                              AppTranslations.of(context).text(
                                      "and")=="and"?SizedBox(
                                height: height * .04,
                              ):SizedBox(
                                height: height * .02,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: width * .02),

                                 child:  TextFormField(
                              validator: (value) {
                                  if (value.length < 8) {
                                  return AppTranslations.of(context).text(
                                      "Password contain at least 8 letters");
                                }
                              },
                              obscureText: _obscureText1,
                              onSaved: (val) => password = val,
                              decoration: InputDecoration(
                                   prefixIcon:Image.asset('assets/image/password.png'),
                                fillColor: Colors.white,
                                filled: true,
                                hintText: AppTranslations.of(context)
                                    .text('Password'),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                    )),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                    )),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                      color: Colors.white,
                                    )),
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    // Based on passwordVisible state choose the icon
                                    _obscureText1
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: passwordColor,
                                  ),
                                  onPressed: () {
                                    // Update the state i.e. toogle the state of passwordVisible variable
                                    setState(() {
                                      _obscureText1 = !_obscureText1;
                                    });
                                  },
                                ),
                              ),
                            ),
                              ),




                              /*  child: CustomTextField(
                                  hint: AppTranslations.of(context)
                                      .text("Password"),
                                  icon: "passwordImage",
                                  onclick: (value) {
                                    password = value;
                                  },
                                  isEmail: false,
                                  issecure: true,
                                ),
                              ),*/
                              SizedBox(
                                height: height * .005,
                              ),
                              Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.pushNamed(
                                            context, '/forgetPassword');
                                      },
                                      child: Text(
                                        AppTranslations.of(context)
                                            .text('Forget Password'),
                                        style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.blue,
                                        ),
                                      ),
                                    ),
                                  ]),
                              AppTranslations.of(context)
                                            .text('and')=="and"?SizedBox(
                                height: height * .02,
                              ):SizedBox(
                                height: height * .002,
                              ),
                              ButtonTheme(
                                height: height * .07,
                                minWidth: MediaQuery.of(context).size.width,
                                child: _isLoading
                                    ? circularProgress()
                                    : RaisedButton(
                                        elevation: 0.0,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        onPressed: () async {
                                          globalKey.currentState.save();
                                          print(phone);
                                          print(password);
                                          if (globalKey.currentState
                                              .validate()) {
                                            _showLoading();
                                            // Navigator.pushNamed(context, '/Login');
                                            loginAction();
                                          }
                                        },
                                        padding: EdgeInsets.all(0),
                                        color: appPrimaryColor,
                                        child: Text(
                                            AppTranslations.of(context)
                                                .text('Login'),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16)),
                                      ),
                              ),
                              SizedBox(
                        height: height * .01,
                      ),
                               Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          AppTranslations.of(context)
                              .text('Dont have an account ?'),
                          style: TextStyle(
                              color: Color(0xffA0A4AB),
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, '/SignUp');
                          },
                          child: Text(
                            AppTranslations.of(context).text('Register'),
                            style: TextStyle(
                                fontSize: 18,
                                color: appPrimaryColor,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                            ]),
                      ),
                     AppTranslations.of(context).text("and")=="and"? SizedBox(
                        height: height * .04,
                      ):SizedBox(
                        height: height * .01,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                              AppTranslations.of(context).text(
                                  'By contining | confirm that i have read & agree to the'),
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  color: Color(0xffA0A4AB),
                                  fontSize: 11,
                                  fontFamily: 'Monstserrat')),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  AppTranslations.of(context)
                                      .text('Term & condition'),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: appPrimaryColor,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(AppTranslations.of(context).text('and'),
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        color: Color(0xffA0A4AB),
                                        fontSize: 13,
                                        fontFamily: 'Monstserrat')),
                                /*  InkWell(
                                onTap: ()async{
                                  if (await UrlLauncher.canLaunch("https://2bill.net/en/privacy.html")) {
                                               await UrlLauncher.launch("https://2bill.net/en/privacy.html");
                                              } else {
                                                _showSnackBar("Could not launch");
                                              }
                                 
                                },
                                                              child:*/Text(
                                  AppTranslations.of(context)
                                      .text('Privacy & policy'),
                                  style: TextStyle(
                                      fontSize: 13,
                                      color: appPrimaryColor,
                                      fontWeight: FontWeight.bold),
                                ),
                                //  ),
                              ]),
                        ],
                      ),
                     
                    ]),
              ),
         
                    ])
           )
          ],
        ),
      ),
    );
  }
}
