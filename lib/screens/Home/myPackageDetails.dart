import 'dart:async';
import 'dart:convert';

import 'package:clipboard/clipboard.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';
import 'package:two_bill/model/Contact.dart';
import 'package:two_bill/model/package.dart';
import 'package:two_bill/model/subscribrion.dart';
import 'package:two_bill/model/user.dart';
import 'package:two_bill/screens/Home/timer.dart';
import 'package:two_bill/screens/user_management/payment.dart';
import 'package:two_bill/screens/user_management/userFrinds.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';

class MyPackageDetails extends StatefulWidget {
  Subscription subsciption;
  MyPackageDetails({Key key, @required this.subsciption}) : super(key: key);
  @override
  _MyPackageDetails createState() => _MyPackageDetails(subsciption);
}

class _MyPackageDetails extends State<MyPackageDetails> {
  Subscription subsciption;
  _MyPackageDetails(Subscription subsciption) {
    this.subsciption = subsciption;
   
  }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  List<Subscription> subsciptions = [];
   Helper helper= new Helper();
  Api api;
  String username = "";
  LocalLogin localLogin=new LocalLogin();
  var headers;
  Request request;
  String url;
  double totalCash = 0.0;
  int seconds = 0;
  int minutes = 0;
  int hours = 0;
  User user;
  Contact contact;
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(text)));
  }
connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  @override
  void initState() {
    super.initState();
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  handleValue(value) {

    if (value == "null" || value=="" || value ==null) {
      helper.removeToken();
       localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/Intro', (_) => false);
    }else{
      loadData();
    }
  }

  _share() async {
    if (AppTranslations.of(context).text("and") == "and") {
      Share.share(
        "Your promocode is : " +
            subsciption.promocode.toString() +
            " \r\n" +
            "Play Store:" +
            "${contact.googlePlayLink}" +
            " \r\n" +
            "App Store:" +
          "${contact.appStoreLink}" ,
        subject: "2BILL",
      );
    } else {
      Share.share(
        subsciption.promocode.toString() +
            " :البروموكود" +
            " \r\n" +
            "${contact.googlePlayLink}" +
            ":متجر جوجل" +
            "\r\n" +
           "${contact.appStoreLink}" +
            ":متجر ابل",
        subject: "2BILL",
      );
    }
  }

  loadData() async {
    _showLoading();
    api = new Api();
    request = new Request();
    url = "/api/subscriptions/points/${subsciption.id}";
    print(url);

    Map<String, dynamic> body = {
      'package_id': subsciption.package.id,
    };
    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    print(headers);

    await api
        .getData(
      url,
      headers,
      context
    )
        .then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print(statusCode);
        _hideLoading();
      } else {
        Map<String, dynamic> json = jsonDecode(res);
        print(json['data']['data']);
        setState(()  {
          subsciptions = Subscription.decode(jsonEncode(json['data']['data']));

          totalCash = (double.parse(
                      subsciption.package.priceBeforeDiscount.toString()) /
                  double.parse(subsciption.package.points.toString())) *
              subsciptions.length.toDouble();
                url = "/api/infos";
   
   

   
        });
         await api.getData(url, headers,context).then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print(statusCode);
      } else {
        Map<String, dynamic> json = jsonDecode(res);
        print(json['data']['data']);
        setState(() {
          contact = Contact.fromJson(json['data']['data']);
        });
      }
    });
        _hideLoading();
        print(subsciptions.length);
      }
    });
    getPromitDate("2021-03-10 21:22:36");
    _showLoading();
    helper = new Helper();
    Future<User> future = helper.getUserData(context);
    future.then((myuser) {
      setState(() {
        user = myuser;
        username = user.name;
        _hideLoading();
        //
      });
    });
  }

  String getPromitDate(String endPermited) {
    String space = " ";
    final startIndex = endPermited.indexOf(space);
    return endPermited.substring(0, endPermited.indexOf(space));
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Color(0xfff5f6f8),
        appBar: CustomAppBar(
          title: "Package Details",
          scafoldkey: _scaffoldKey,
          hasarowBack: true,
        ),
        drawer: AppDrawer(
          scafoldkey: _scaffoldKey,
        ),
        bottomNavigationBar:
            customBottomNavigationBar(context, width, height, _scaffoldKey),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: _isLoading
              ? circularProgress()
              : ListView(shrinkWrap: true, children: [
                  Container(
                    height: height * .1,
                    color: appPrimaryColor,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          username,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 30,
                              fontWeight: FontWeight.w900),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: width * .05),
                    child: ListView(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      children: [
                        RichText(
                          text: new TextSpan(
                            text: AppTranslations.of(context)
                                    .text('Share your promo code with') +
                                ' ${subsciption.package.points} ' +
                                AppTranslations.of(context).text('friends'),
                            style: new TextStyle(
                              fontSize: 18,
                              color: Color(0xff96add8),
                              fontWeight: FontWeight.w900,
                            ),
                          ),
                        ),
                        AppTranslations.of(context).text("and") == "and"
                            ? SizedBox(
                                height: height * .02,
                              )
                            : SizedBox(
                                height: height * .01,
                              ),
                        subsciption.isPaid == 1
                       ?  Container(
                                height: height * .07,
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(12),
                                  border: Border.all(
                                      color: Color(0xff3da9cd), width: 2),
                                ),
                                child: Row(children: [
                                  SizedBox(
                                    width: width * .05,
                                  ),
                                  Text(
                                    AppTranslations.of(context)
                                        .text("promocode"),
                                    style: TextStyle(
                                        color: Color(0xffaeafb1),
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    width: width * .25,
                                  ),
                                  Text(
                                    subsciption.promocode,
                                    style: TextStyle(
                                        color: Color(0xffaeafb1),
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    width: width * .01,
                                  ),
                                  InkWell(
                                    child: Image.asset(copy),
                                    onTap: () {
                                      FlutterClipboard.copy(
                                              subsciption.promocode)
                                          .then((result) {
                                        AppTranslations.of(context)
                                                    .text("and") ==
                                                "and"
                                            ? _showSnackBar(
                                                "Promocode is copied")
                                            : _showSnackBar(
                                                "تم نسخ البروموكود");
                                      });
                                    },
                                  ),
                                  SizedBox(
                                    width: width * .01,
                                  ),
                                  InkWell(
                                    child: Icon(Icons.share),
                                    onTap: () {
                                      _share();
                                    },
                                  ),
                                ]))
                            : Container(),
                        AppTranslations.of(context).text("and") == "and"
                            ? SizedBox(
                                height: height * .02,
                              )
                            : SizedBox(
                                height: height * .01,
                              ),
                        Row(
                          children: [
                            AppTranslations.of(context).text("and") != "and"
                                ? Text(
                                    subsciption.package.firstHeadlineAr,
                                    style: TextStyle(
                                        color: Color(0xff3864b9),
                                        fontSize: 25,
                                        fontWeight: FontWeight.w900),
                                  )
                                : Text(
                                    subsciption.package.firstHeadlineEn,
                                    style: TextStyle(
                                        color: Color(0xff3864b9),
                                        fontSize: 25,
                                        fontWeight: FontWeight.w900),
                                  ),
                            SizedBox(
                              width: width * .01,
                            ),
                          ],
                        ),
                        AppTranslations.of(context).text("and") == "and"
                            ? SizedBox(
                                height: height * .01,
                              )
                            : SizedBox(
                                height: height * .005,
                              ),
                        Row(children: [
                          AppTranslations.of(context).text("and") == "and"
                              ? Text(
                                  subsciption.package.nameEn,
                                  style: TextStyle(
                                    color: Color(0xff464646),
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              : Text(
                                  subsciption.package.nameAr,
                                  style: TextStyle(
                                    color: Color(0xff464646),
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                        ]),
                        AppTranslations.of(context).text("and") == "and"
                            ? SizedBox(
                                height: height * .03,
                              )
                            : SizedBox(
                                height: height * .02,
                              ),
                        AppTranslations.of(context).text("and") == "and"
                            ? Row(
                                children: [
                                  RichText(
                                    text: new TextSpan(
                                      text:
                                          '${subsciption.package.priceBeforeDiscount}EG',
                                      style: new TextStyle(
                                        color: Color(0xff464646),
                                        fontSize: 16,
                                        decoration: TextDecoration.lineThrough,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: width * .05,
                                  ),
                                  Text(
                                      '${subsciption.package.priceAfterDiscount}EG/${subsciption.package.type}',
                                      style: TextStyle(
                                        color: Colors.green,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      )),
                                  SizedBox(
                                    width: width * .04,
                                  ),
                                  subsciption.isPaid == 0
                                      ? Container(
                                          width: width * .3,
                                          child: RaisedButton(
                                            color: Colors.white,
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        Payment(
                                                      subscription: subsciption,
                                                      promocode: subsciption
                                                                  .parentSubscription !=
                                                              null
                                                          ? subsciption
                                                              .parentSubscription
                                                              .promocode
                                                          : null,
                                                    ),
                                                  ));
                                            },
                                            child: Text(
                                                AppTranslations.of(context)
                                                    .text("Pay Now"),
                                                style: TextStyle(
                                                  color: Color(0xff809bd0),
                                                  fontSize: 15,
                                                )),
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20)),
                                          ),
                                        )
                                      : Container(),
                                ],
                              )
                            : Row(children: [
                                RichText(
                                  text: new TextSpan(
                                    text:
                                        '${subsciption.package.priceBeforeDiscount}جنيه',
                                    style: new TextStyle(
                                      color: Color(0xff464646),
                                      fontSize: 16,
                                      decoration: TextDecoration.lineThrough,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: width * .04,
                                ),
                                subsciption.package.type == "monthly"
                                    ? Text(
                                        '${subsciption.package.priceAfterDiscount}جنيه/شهريا',
                                        style: TextStyle(
                                          color: Color(0xff464646),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14,
                                        ))
                                    : Text(
                                        '${subsciption.package.priceAfterDiscount}جنيه/سنوي',
                                        style: TextStyle(
                                          color: Color(0xff464646),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14,
                                        )),
                                SizedBox(
                                  width: width * .02,
                                ),
                                subsciption.isPaid == 0
                                    ? Container(
                                        width: width * .35,
                                        child: RaisedButton(
                                          color: Colors.white,
                                          onPressed: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context) => Payment(
                                                    subscription: subsciption,
                                                    promocode: subsciption
                                                                .parentSubscription !=
                                                            null
                                                        ? subsciption
                                                            .parentSubscription
                                                            .promocode
                                                        : null,
                                                  ),
                                                ));
                                          },
                                          child: Text(
                                              AppTranslations.of(context)
                                                  .text("Pay Now"),
                                              style: TextStyle(
                                                color: Color(0xff809bd0),
                                                fontSize: 15,
                                              )),
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20)),
                                        ),
                                      )
                                    : Container(),
                                /* Text(
                                '${subscription.package.discountPercentage}%OF',
                                    style: TextStyle(
                                      color: Colors.green,
                                    ),
                                  ),*/
                              ]),

                        AppTranslations.of(context).text("and") == "and"
                            ? SizedBox(
                                height: height * .02,
                              )
                            : SizedBox(
                                height: height * .01,
                              ),

                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(
                                AppTranslations.of(context).text("Points"),
                                style: TextStyle(
                                    color: Color(0xff414c5b),
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                AppTranslations.of(context)
                                    .text("Total credit"),
                                style: TextStyle(
                                    color: Color(0xff414c5b),
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),
                            ]),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(
                                subsciptions.length.toString(),
                                style: TextStyle(
                                  color: Color(0xff969ca5),
                                  fontSize: 15,
                                ),
                              ),
                              Text(totalCash.toString(),
                                  style: TextStyle(
                                    color: Color(0xff969ca5),
                                    fontSize: 16,
                                  )),
                            ]),
                        subsciption.isPaid == 1
                            ? Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: width * .05),
                                child: Container(
                                  height:AppTranslations.of(context).text("and")=="and"? height * .15:height * .17,
                                  padding: EdgeInsets.all(0),
                                  decoration: new BoxDecoration(
                                    color: Color(0xfff5f6f8),
                                    image: DecorationImage(
                                      image: AppTranslations.of(context)
                                                  .text("and") ==
                                              "and"
                                          ? AssetImage(packageDetails1En)
                                          : AssetImage(
                                              packageDetails1Ar,
                                            ),
                                    ),
                                  ),
                                  child: Column(children: [
                                    AppTranslations.of(context).text("and")=="and"? SizedBox(
                                      height: height * .02,
                                    ):SizedBox(
                                      height: height * .02,
                                    ),
                                    Text(
                                      AppTranslations.of(context)
                                              .text('you must add') +
                                          ' ${subsciption.package.points} ' +
                                          AppTranslations.of(context)
                                              .text('of your friends within') +
                                          ' ${subsciption.package.permitDays} ' +
                                          AppTranslations.of(context)
                                              .text('days'),
                                      style: TextStyle(
                                        fontSize: 11,
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xff3864b9),
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 50),
                                        child: TestWidget(
                                            date: subsciption.endPermited)),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Text(
                                          AppTranslations.of(context)
                                              .text('Day'),
                                          style: TextStyle(
                                            fontSize: 13,
                                            fontWeight: FontWeight.w900,
                                            color: Color(0xff55cf99),
                                          ),
                                        ),
                                        Text(
                                          AppTranslations.of(context)
                                              .text('Hour'),
                                          style: TextStyle(
                                            fontSize: 13,
                                            fontWeight: FontWeight.w900,
                                            color: Color(0xff55cf99),
                                          ),
                                        ),
                                        Text(
                                          AppTranslations.of(context)
                                              .text('Minute'),
                                          style: TextStyle(
                                            fontSize: 13,
                                            fontWeight: FontWeight.w900,
                                            color: Color(0xff55cf99),
                                          ),
                                        ),
                                        Text(
                                          AppTranslations.of(context)
                                              .text('Second'),
                                          style: TextStyle(
                                            fontSize: 13,
                                            fontWeight: FontWeight.w900,
                                            color: Color(0xff55cf99),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ]),
                                ))
                            : Container(),
                        AppTranslations.of(context).text("and") == "and"
                            ? SizedBox(
                                height: height * .03,
                              )
                            : SizedBox(
                                height: height * .02,
                              ),
                        subsciption.isPaid == 1
                            ? Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: width * .05),
                                child: Container(
                                  height:AppTranslations.of(context).text("and")=="and"? height * .15:height * .17,
                                  padding: EdgeInsets.all(0),
                                  decoration: new BoxDecoration(
                                    color: Color(0xfff5f6f8),
                                    image: DecorationImage(
                                      image: AppTranslations.of(context)
                                                  .text("and") ==
                                              "and"
                                          ? AssetImage(packageDetails2En)
                                          : AssetImage(
                                              packageDetails2Ar,
                                            ),
                                    ),
                                  ),
                                  child: Column(children: [
                                    AppTranslations.of(context).text("and")=="and"? SizedBox(
                                      height: height * .02,
                                    ):SizedBox(
                                      height: height * .02,
                                    ),
                                    Text(
                                      AppTranslations.of(context).text(
                                              'your money will be transformed after') +
                                          ' ${int.parse(subsciption.package.permitDays.toString()) + 10} ' +
                                          AppTranslations.of(context)
                                              .text('days'),
                                      style: TextStyle(
                                        fontSize: 11,
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xff3864b9),
                                      ),
                                    ),
                                    Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 50),
                                        child: TestWidget(
                                            date: subsciption.endDate)),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Text(
                                          AppTranslations.of(context)
                                              .text('Day'),
                                          style: TextStyle(
                                            fontSize: 13,
                                            fontWeight: FontWeight.w900,
                                            color: Color(0xff55cf99),
                                          ),
                                        ),
                                        Text(
                                          AppTranslations.of(context)
                                              .text('Hour'),
                                          style: TextStyle(
                                            fontSize: 13,
                                            fontWeight: FontWeight.w900,
                                            color: Color(0xff55cf99),
                                          ),
                                        ),
                                        Text(
                                          AppTranslations.of(context)
                                              .text('Minute'),
                                          style: TextStyle(
                                            fontSize: 13,
                                            fontWeight: FontWeight.w900,
                                            color: Color(0xff55cf99),
                                          ),
                                        ),
                                        Text(
                                          AppTranslations.of(context)
                                              .text('Second'),
                                          style: TextStyle(
                                            fontSize: 13,
                                            fontWeight: FontWeight.w900,
                                            color: Color(0xff55cf99),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ]),
                                ))
                            : Container(),

                        ///////////////////////////////////////////////////
                      ],
                    ),
                  ),
                  subsciption.isPaid == 1
                      ? GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => UserFriends(
                                    subsciption: subsciption,
                                  ),
                                ));
                          },
                          child: Container(
                            height: height * .08,
                            width: width,
                            decoration: BoxDecoration(
                              color: Color(0xff3864b9),
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(40),
                                topLeft: Radius.circular(40),
                              ),
                            ),
                            child: Center(
                              child: Text(
                                AppTranslations.of(context).text(
                                    'Check Your Friends you have invited'),
                                style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w900,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        )
                      : Container(),
                ]),
        ));
  }
}
