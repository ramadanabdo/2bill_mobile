import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/model/service.dart';
import 'package:two_bill/screens/Home/subscribePackage.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/model/package.dart';
import 'package:two_bill/utils/style.dart';

class PackageScrean extends StatefulWidget {
  ServiceProvider service;
  PackageScrean({Key key, @required this.service}) : super(key: key);
  @override
  _PackageScrean createState() => _PackageScrean(service);
}

class _PackageScrean extends State<PackageScrean> {
  ServiceProvider service;
  _PackageScrean(ServiceProvider service) {
    this.service = service;
  }
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Api api;
  Request request;
   LocalLogin localLogin=new LocalLogin();
  List<bool> _isFavorited;
  String url;
  var headers;
  var body;
  List<Package> packages = [];

   Helper helper= new Helper();
  bool _isLoading = false;
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }
connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  @override
  void initState() {
    super.initState();
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  handleValue(value) {

    if (value == "null" || value=="" || value ==null) {
      helper.removeToken();
       localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/Intro', (_) => false);
    }else{
      loadData();
    }
  }

  loadData() async {
    _showLoading();

    api = new Api();
    request = new Request();

    body = request.getBody(30, 0, "id", "ASC", "all", "false", "false",
        "service_id", "=", service.id.toString());
    print(body);
    url = "/api/packages$body";
    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    print(headers);

    await api
        .getData(
      url,
      headers,
      context
    )
        .then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print(statusCode);
        _hideLoading();
      } else {
        Map<String, dynamic> json = jsonDecode(res);
        print(json['data']['data']);
        setState(() {
          packages = Package.decode(jsonEncode(json['data']['data']));
          _isFavorited = List.filled(packages.length, false);
        });
        _hideLoading();
        print(packages.length);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
        final double shortSize= MediaQuery.of(context).size.shortestSide;
    final bool screenWidth=shortSize < 600.0;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: appfixedColor,
      appBar: CustomAppBar(
        title: "Packages",
        scafoldkey: _scaffoldKey,
        hasarowBack: true,
      ),
      drawer: AppDrawer(
        scafoldkey: _scaffoldKey,
      ),
      bottomNavigationBar:
            customBottomNavigationBar(context, width, height, _scaffoldKey),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: SingleChildScrollView(
    child: 
         
            Container(
              width: width,
              
              padding: EdgeInsets.all(0),
              decoration: new BoxDecoration(
                color: appfixedColor,
                image: DecorationImage(image: AssetImage(background)),
              ),
              child: ListView(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: width * .01,
                          right: width * .01,
                          top: height * .03),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AppTranslations.of(context).text("and") == "and"
                                ? Image.asset(providerEn)
                                : Image.asset(providerAr),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: height * .025,
                                ),
                                AppTranslations.of(context).text('and') == "and"
                                    ? Text(
                                        '' +
                                            AppTranslations.of(context)
                                                .text('Select Package from') +
                                            ' ${service.nameEn}',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          height: 1.5,
                                          color: Color(0xff414C5B),
                                          fontSize: 14,
                                        ))
                                    : Text(
                                        '' +
                                            AppTranslations.of(context)
                                                .text('Select Package from') +
                                            ' ${service.nameAr}',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          height: 1.5,
                                          color: Color(0xff414C5B),
                                          fontSize: 14,
                                        )),
                              ],
                            )
                          ]),
                    ),
                    _isLoading
                        ? circularProgress()
                        : Padding(
                            padding:
                                EdgeInsets.symmetric(horizontal: width * .05),
                            child: packages.length > 0
                                ? GridView.builder(
                                    shrinkWrap: true,
                                   physics: NeverScrollableScrollPhysics(),
                                      //  primary: true,
                                  //  scrollDirection:Axis.vertical ,
                                    itemCount: packages.length,
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: 2,
                                            childAspectRatio: .65,
                                            crossAxisSpacing: 5,
                                            mainAxisSpacing: 5),
                                    itemBuilder: (context, index) {
                                      bool item = _isFavorited[index];
                                      return GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                builder: (context) =>
                                                    SubscribePackage(
                                                     
                                                  package: packages[index],
                                                ),
                                              ));
                                        },
                                      
                                           
                                          child: Card(
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(40),
                                                bottomLeft: Radius.circular(40),
                                              ),
                                            ),
                                            semanticContainer: true,
                                            clipBehavior:
                                                Clip.antiAliasWithSaveLayer,
                                            elevation: 12.0,
                                            child: ListView(
                                              physics: NeverScrollableScrollPhysics(),
                                              children: [
                                       
                                             Container(
                                        
                                         height: 190,
                                          width:
                                                MediaQuery.of(context).size.width,
                                          
                                                color: Colors.grey[200],
                                                child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.center,
                                                    children: [
                                                       AppTranslations.of(context).text("and")=="and"? SizedBox(
                                                        height: 7,
                                                      ): SizedBox(
                                                        height: 5,
                                                      ),
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceAround,
                                                          children: [
                                                           AppTranslations.of(context).text("and")=="and"? Text(
                                                              packages[index].firstHeadlineEn,
                                                              textScaleFactor: 1.2,
                                                              style: TextStyle(
                                                                color: Color(
                                                                    0xff3864b9),
                                                                fontSize: 15,
                                                                fontWeight:
                                                                    FontWeight.bold,
                                                              ),
                                                            ):Text(
                                                              packages[index].firstHeadlineAr,
                                                              textScaleFactor: 2,
                                                              style: TextStyle(
                                                                color: Color(
                                                                    0xff3864b9),
                                                                fontSize: 15,
                                                                fontWeight:
                                                                    FontWeight.bold,
                                                              ),
                                                            ),
                                                            GestureDetector(
                                                              child: _isFavorited[
                                                                      index]
                                                                  ? Icon(
                                                                      Icons.star,
                                                                      color: Colors
                                                                          .orange,
                                                                    )
                                                                  : Icon(
                                                                      Icons.star,
                                                                      color: Color(
                                                                          0xffc4c4c4),
                                                                    ),
                                                              onTap: () async {
                                                                setState(() {
                                                                  _isFavorited[
                                                                          index] =
                                                                      !_isFavorited[
                                                                          index];

                                                                  // _isFavorited[index]=true;
                                                                });
                                                                url =
                                                                    "/api/packages/addFavRequest";
                                                                var body = {
                                                                  'package_id':
                                                                      packages[
                                                                              index]
                                                                          .id
                                                                };
                                                                // print(body);
                                                                await request
                                                                    .getHeaader(context)
                                                                    .then((dynamic
                                                                        dyHeaders) async {
                                                                  headers =
                                                                      dyHeaders;
                                                                });
                                                                print(headers);

                                                                await api
                                                                    .postData(url,
                                                                        headers,
                                                                        context,
                                                                        body: body)
                                                                    .then((dynamic
                                                                        response) async {
                                                                  final String res =
                                                                      response.body;
                                                                  final int
                                                                      statusCode =
                                                                      response
                                                                          .statusCode;
                                                                  if (statusCode == 401 ||
                                                                      statusCode ==
                                                                          400 ||
                                                                      statusCode ==
                                                                          403 ||
                                                                      statusCode ==
                                                                          501 ||
                                                                      statusCode ==
                                                                          404) {
                                                                    print(
                                                                        statusCode);
                                                                  } else {
                                                                    Map<String,
                                                                            dynamic>
                                                                        json =
                                                                        jsonDecode(
                                                                            res);
                                                                    print(json);
                                                                  }
                                                                });
                                                              },
                                                            )
                                                          ]),
                                                  AppTranslations.of(context).text("and")=="and"? SizedBox(
                                                        height: 30,
                                                      ): SizedBox(
                                                        height: 20,
                                                      ),
                                                      AppTranslations.of(context)
                                                                  .text("and") ==
                                                              "and"
                                                          ? Text(
                                                              packages[index]
                                                                  .nameEn,
                                                              style: TextStyle(
                                                                color: Color(
                                                                    0xff464646),
                                                                fontSize: 13,
                                                                fontWeight:
                                                                    FontWeight.bold,
                                                              ),
                                                            )
                                                          : Text(
                                                              packages[index]
                                                                  .nameAr,
                                                              style: TextStyle(
                                                                color: Color(
                                                                    0xff464646),
                                                                fontSize: 13,
                                                                fontWeight:
                                                                    FontWeight.bold,
                                                              ),
                                                            ),
                                                       AppTranslations.of(context).text("and")=="and"? SizedBox(
                                                        height: 10,
                                                      ): SizedBox(
                                                        height: 5,
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: [
                                                         AppTranslations.of(context).text("and")=="and"?  Text(
                                                            packages[index].secondHeadlineEn,
                                                            style: TextStyle(
                                                              color:
                                                                  Color(0xff464646),
                                                              fontSize: 12,
                                                              fontWeight:
                                                                  FontWeight.w900,
                                                            ),
                                                          ):
                                                           Text(
                                                            packages[index].secondHeadlineAr,
                                                            style: TextStyle(
                                                              color:
                                                                  Color(0xff464646),
                                                              fontSize: 12,
                                                              fontWeight:
                                                                  FontWeight.w900,
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: width * .03,
                                                          ),
                                                          
                                                        ],
                                                      ),
                                                       AppTranslations.of(context).text("and")=="and"? SizedBox(
                                                        height: 10,
                                                      ): SizedBox(
                                                        height: 5,
                                                      ),
                                                      AppTranslations.of(context)
                                                                  .text("and") ==
                                                              "and"
                                                          ? Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: [
                                                                RichText(
                                                                  text:
                                                                      new TextSpan(
                                                                    text:
                                                                        '${packages[index].priceBeforeDiscount}EG',
                                                                    style:
                                                                        new TextStyle(
                                                                      color: Color(
                                                                          0xff464646),
                                                                      fontSize: 14,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                      decoration:
                                                                          TextDecoration
                                                                              .lineThrough,
                                                                    ),
                                                                  ),
                                                                ),
                                                                SizedBox(
                                                                  width:
                                                                      width * .01,
                                                                ),
                                                                Text(
                                                                    '${packages[index].priceAfterDiscount}EG/${packages[index].type}',
                                                                    style:
                                                                        TextStyle(
                                                                      color: Color(
                                                                          0xff464646),
                                                                      fontSize: 14,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold,
                                                                    ))
                                                              ],
                                                            )
                                                          : Row(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                            children: [
                                                              RichText(
                                                                text: new TextSpan(
                                                                  text:
                                                                      '${packages[index].priceBeforeDiscount}جنيه',
                                                                  style:
                                                                      new TextStyle(
                                                                    color: Color(
                                                                        0xff464646),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize: 14,
                                                                    decoration:
                                                                        TextDecoration
                                                                            .lineThrough,
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                width: width * .01,
                                                              ),
                                                             packages[index].type=="monthly"? Text(
                                                                  '${packages[index].priceAfterDiscount}جنيه/شهريا',
                                                                  style: TextStyle(
                                                                    color: Color(
                                                                        0xff464646),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize: 14,
                                                                  )):Text(
                                                                  '${packages[index].priceAfterDiscount}جنيه/سنوي',
                                                                  style: TextStyle(
                                                                    color: Color(
                                                                        0xff464646),
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                    fontSize: 14,
                                                                  ))
                                                            ]),
                                                        AppTranslations.of(context).text("and")=="and"? SizedBox(
                                                        height: 20,
                                                      ): SizedBox(
                                                        height: 10,
                                                      ),
                                                     AppTranslations.of(context).text("and")=="and"? Row(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: [

                                                        Text(
                                                          '${packages[index].discountPercentage}%OFF',
                                                          style: TextStyle(
                                                            color: Colors.green,
                                                          ),
                                                        ),
                                                         
                                                        ],
                                                      ): Row(
                                                        mainAxisAlignment: MainAxisAlignment.center,
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                        children: [
                                                        
                                                        Text(
                                                          'خصم%${packages[index].discountPercentage}',
                                                          style: TextStyle(
                                                            color: Colors.green,
                                                          ),
                                                        ),
                                                         
                                                        ],
                                                      )
                                                      
                                                    ])
                                              ),
                                          
                                                    Container(
                                                        height:50,
                                                        decoration: BoxDecoration(
                                                          color:
                                                              Color(0xff3864b9),
                                                          borderRadius:
                                                              BorderRadius.only(
                                                            bottomLeft:
                                                                Radius.circular(
                                                                    40),
                                                          ),
                                                        ),
                                                        child: AppTranslations.of(
                                                                        context)
                                                                    .text(
                                                                        "and") ==
                                                                "and"
                                                            ? Center(
                                                                child: Text(
                                                                  AppTranslations.of(
                                                                          context)
                                                                      .text(
                                                                          'Subscribe'),
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize: 18,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                              )
                                                            : Center(
                                                                child: Text(
                                                                  AppTranslations.of(
                                                                          context)
                                                                      .text(
                                                                          'Subscribe'),
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize: 18,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                  ),
                                                                ),
                                                              ),
                                                      ),
                                                    
                                                 
                                            ],)
                                          )
                                          );
                                        
                                    
                                    })
                                : Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                        SizedBox(
                                          height: height * .01,
                                        ),
                                        Text(
                                          AppTranslations.of(context)
                                              .text('No Packages avaliable'),
                                          style: TextStyle(
                                            color: Color(0xffd0d0d0),
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ]),
                          )
                  ])),
         
        ),
      ),
    );
  }

  /* Widget card() {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      elevation: 12.0,
      child: Column(children: [
        Expanded(
          child: Container(
              decoration: BoxDecoration(
                color: Colors.grey[100],
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(40),
                ),
              ),
              child: Expanded(
                child: Column(children: [
                  Expanded(
                    child: Text(
                      'jksd',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Divider(),
                  Expanded(child: Text(
                    " packages[index].nameEn",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),),
                  Expanded(
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Text(
                              'packages[index].points',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                              ),
                            ),
                          ),
                       Expanded(child:    Text(
                         '  AppTranslations.of(context).text("Spead")',
                         style: TextStyle(
                           color: Colors.black,
                           fontSize: 15,
                         ),
                       ),),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Row(
                      children: [
                        Expanded(
                          child: RichText(
                            text: new TextSpan(
                              text: '100 EG',
                              style: new TextStyle(
                                color: Colors.black,
                                fontSize: 13,
                                decoration: TextDecoration.lineThrough,
                              ),
                            ),
                          ),
                        ),
                        Divider(),
                        Expanded(
                          child: Text('100EG/500}',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 13,
                              )),
                        )
                      ],
                    ),
                  ),
               Expanded(child:    Text(
                 '50%OF',
                 style: TextStyle(
                   color: Colors.green,
                 ),
               ),),

                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(40),
                        ),
                      ),
                      child: Center(
                        child: Text(
                          'ldk;l,ask',
                          style: TextStyle(
                            backgroundColor: Colors.blue,
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ]),
              )),

        ),

      ]),
    );*/

}
