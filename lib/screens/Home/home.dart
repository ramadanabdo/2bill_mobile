import 'dart:convert';
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:two_bill/model/service.dart';
import 'package:two_bill/model/provider.dart';
import 'package:two_bill/model/user.dart';
import 'package:two_bill/screens/Home/serviceProvider.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/utils/style.dart';

class Home extends StatefulWidget {
  @override
  _Home createState() => _Home();
}

class _Home extends State<Home> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final TextEditingController search = new TextEditingController();
  List<Provider> providers = [];
  var headers;
  bool _isLoading = false;
  Helper helper = new Helper();
  LocalLogin localLogin = new LocalLogin();
  Api api;
  Request request;
  String url;
  String body;

  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }

  connectstatus(value) {
    if (value == false) {
      showErrorDialog(context, "check_internet");
    }
  }

  onSearchTextChanged(String text) async {
    _showLoading();
    String columns;
    if (AppTranslations.of(context).text("and") == "and") {
      columns = "name_en";
    } else {
      columns = "name_ar";
    }
    api = new Api();
    request = new Request();
    body = request.getBody(30, 0, "id", "ASC", "all", "false", "false", columns,
        "like", search.text);
    url = "/api/providers$body";

    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    print(body);

    await api.getData(url, headers, context).then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print('dhdshdhjfhjsfhfh$statusCode');
        _hideLoading();
      } else {
        Map<String, dynamic> json = jsonDecode(res);
        print(json['data']['data']);
        setState(() {
          providers = Provider.decode(jsonEncode(json['data']['data']));
        });
        _hideLoading();
      }
    });
  }

  loadData() async {
    _showLoading();
    api = new Api();
    request = new Request();
    body = request.getBody(
        10, 0, "id", "ASC", "all", "false", "true", null, null, null);
    url = "/api/providers$body";

    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    print(headers);

    await api.getData(url, headers, context).then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        if (statusCode == 401) {
          localLogin.readData().then((var decr) async {
            print("decr      $decr");
            if (decr == null || decr == "") {
              await helper.removeToken();

              await localLogin.clearFile();
              print('refhere');
              Navigator.of(context)
                  .pushNamedAndRemoveUntil("/SignIn", (route) => false);
            } else {
              localLogin.decrypt(decr).then((var data) async {
                print(data);
                var list = data.split(" ");
                String _username = list[0];
                String _password = list[1];

                var body = <String, dynamic>{
                  'phone': _username,
                  'password': _password,
                };

                helper = new Helper();
                url = "/api/auth/login";
                await request
                    .getHeaader(context)
                    .then((dynamic dyHeaders) async {
                  headers = dyHeaders;
                });
                print('headers $headers');
                var bodyData = json.encode(body);
                print(bodyData);
                await http
                    .post('https://admin.2bill.net' + '$url',
                        body: bodyData, headers: headers)
                    .then((http.Response response) async {
                  final String res = response.body;
                  final int statusCode = response.statusCode;
                  if (response.statusCode == 200) {
                    Map<String, dynamic> json = jsonDecode(res);
                    print(json['access_token']);
                    await helper.sharedpref("userToken", json['access_token']);
                    print("user    ");
                    Future<User> future = helper.getUserData(context);
                    future.then((myuser) async {
                      await helper.sharedpref("user", myuser);
                    });
                    print('refhere2');

                  loadData();
                  } else {
                    await helper.removeToken();
                    await localLogin.clearFile();
                    print('refhere3');
                    Navigator.of(context)
                        .pushNamedAndRemoveUntil("/SignIn", (route) => false);
                  }
                });

                //////////////////////////////
              });
            }
          });
        }

        print('statusCode  $statusCode');
      } else {
        print('statusCode  $statusCode');
        Map<String, dynamic> json = jsonDecode(res);
        print(json['data']['data']);
        setState(() {
          providers = Provider.decode(jsonEncode(json['data']['data']));
        });
        _hideLoading();
      }
    });
  }

  @override
  initState() {
    super.initState();
    Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
    Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
  }

  handleValue(value) {
    if (value == "null" || value == "" || value == null) {
      helper.removeToken();
      localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/SignIn', (_) => false);
    } else {
      loadData();
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    final double shortSize = MediaQuery.of(context).size.shortestSide;
    //final bool screenWidth=shortSize < 600.0;
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: appfixedColor,
        appBar: CustomAppBar(
          title: "Home",
          scafoldkey: _scaffoldKey,
          hasarowBack: false,
        ),
        drawer: AppDrawer(
          scafoldkey: _scaffoldKey,
        ),
        bottomNavigationBar:
            customBottomNavigationBar(context, width, height, _scaffoldKey),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: SingleChildScrollView(
              child: ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    SizedBox(
                      height: height * .01,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: width * .05),
                      child: Material(
                        borderRadius: BorderRadius.circular(30),
                        elevation: 20.0,
                        shadowColor: appfixedColor,
                        child: TextFormField(
                          controller: search,
                          onChanged: onSearchTextChanged,
                          decoration: InputDecoration(
                            contentPadding: new EdgeInsets.symmetric(
                                vertical: height * .01),
                            hintText: AppTranslations.of(context)
                                .text('Search for Service'),
                            prefixIcon: Icon(
                              Icons.search,
                              color: Colors.grey,
                            ),
                            fillColor: Colors.white,
                            filled: true,
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide(
                                  color: Colors.white,
                                )),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide(
                                  color: Colors.white,
                                )),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide(
                                  color: Colors.white,
                                )),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: width * .1,
                          right: width * .1,
                          top: height * .03),
                      child: Row(children: [
                        AppTranslations.of(context).text("and") == "and"
                            ? Image.asset(homeEn)
                            : Image.asset(
                                homeAr,
                              ),
                        SizedBox(
                          width: width * .02,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: height * .015,
                            ),
                            Text(
                              AppTranslations.of(context)
                                  .text('Welcome to 2Bill'),
                              textAlign: TextAlign.start,
                              style: smalBig2Style(),
                            ),
                            Text(
                              AppTranslations.of(context).text(
                                  'Here you can get 50% off from your package'),
                              textAlign: TextAlign.justify,
                              overflow: TextOverflow.fade,
                              maxLines: 2,
                              softWrap: true,
                              style: TextStyle(
                                color: Color(0xff414C5B),
                                fontSize: 11,
                              ),
                            ),
                            SizedBox(
                              height: height * .05,
                            ),
                          ],
                        ),
                      ]),
                    ),
                    SizedBox(
                      height: height * .02,
                    ),
                    _isLoading
                        ? circularProgress()
                        : Padding(
                            padding:
                                EdgeInsets.symmetric(horizontal: width * .05),
                            child: GridView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: providers.length,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                        childAspectRatio: .79,
                                        crossAxisCount: 2),
                                itemBuilder: (context, index) {
                                  return GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                Serviceprovider(
                                              provider: providers[index],
                                            ),
                                          ));
                                    },
                                    child: Card(
                                        semanticContainer: true,
                                        clipBehavior:
                                            Clip.antiAliasWithSaveLayer,
                                        elevation: 12.0,
                                        child: Column(
                                          children: [
                                            Container(
                                              height: 150,
                                              decoration: new BoxDecoration(
                                                color: appfixedColor,
                                                image: DecorationImage(
                                                    image: NetworkImage(
                                                        providers[index].logo),
                                                    fit: BoxFit.cover),
                                              ),
                                            ),
                                            Container(
                                                height: 40,
                                                color: Colors.white,
                                                padding:
                                                    EdgeInsets.only(top: 10),
                                                child:
                                                    AppTranslations.of(context)
                                                                .text("and") ==
                                                            "and"
                                                        ? Center(
                                                            child: Text(
                                                              providers[index]
                                                                  .nameEn,
                                                              style:
                                                                  smalBigStyle(),
                                                            ),
                                                          )
                                                        : Center(
                                                            child: Text(
                                                              providers[index]
                                                                  .nameAr,
                                                              style:
                                                                  smalBig2Style(),
                                                            ),
                                                          )),
                                          ],
                                        )),
                                  );
                                }),
                          ),
                  ]),
            )));
  }
}
