import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/model/Contact.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/utils/style.dart';
import 'dart:io' show Platform;
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
class About extends StatefulWidget {
  @override
  _About createState() => _About();
}

class _About extends State<About> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
String urlPath="";
LocalLogin localLogin=new LocalLogin();
    Api api;
  Helper helper = new Helper();
  Contact contact;
  Request request;
  String url;
    var headers;
     connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  @override
  void initState() {
        Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
    super.initState();
  Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  handleValue(value) {

    if (value == "null" || value=="" || value ==null) {
      helper.removeToken();
      localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/Intro', (_) => false);
    }else{
      loadData();
    }
  }
 
  bool _isLoading = false;
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }


  loadData() async {
    _showLoading();
    api = new Api();
    request = new Request();
    url = "/api/infos";
    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    print(headers);

    await api.getData(url, headers,context).then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print(statusCode);
      } else {
        Map<String, dynamic> json = jsonDecode(res);
        print(json['data']['data']);
        setState(() {
          contact = Contact.fromJson(json['data']['data']);
        });
         if(Platform.isIOS){
          setState(() {
            urlPath=contact.googlePlayLink;
          });
    }else{
       setState(() {
            urlPath=contact.appStoreLink;
          });
    }
        _hideLoading();
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: appfixedColor,
        appBar: CustomAppBar(
          title: "About AS",
          scafoldkey: _scaffoldKey,
        ),
        drawer: AppDrawer(
          scafoldkey: _scaffoldKey,
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: ListView(
         
              children: [
            SizedBox(
              height: height*.1,
            ),
                SizedBox(child: AppTranslations.of(context).text("and")=="and"?
                
                Image.asset(logo): Image.asset(logoAr),
                height: height*.2,
                width: width*.3,),
                 SizedBox(
                   height: height*.02,
                 ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
         
              
                    Text(
                      AppTranslations.of(context)
                          .text('Powered by 2bill for banking & payment'),
                      style: TextStyle(
                       
                          color: Color(0xff414c58),
                          fontSize: 15,
                          fontWeight: FontWeight.w900,
                          fontFamily: 'Monstserrat'),
                    ),
                    // SizedBox(
                    //   height: height * .01,
                    // ),
                    // Text(
                    //   AppTranslations.of(context).text('Technology services'),
                    //   style: TextStyle(
                       
                    //       color: Color(0xff414c58),
                    //       fontSize: 15,
                    //       fontWeight: FontWeight.w900,
                    //       fontFamily: 'Monstserrat'),
                    // ),
                    SizedBox(
                      height: height * .02,
                    ),
                    
                 
                    Container(
                      width: width * .3,
                      child: RaisedButton(
                        elevation: 0.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        onPressed: () async {
                          if (await UrlLauncher.canLaunch(urlPath)) {
                                      await UrlLauncher.launch(urlPath);
                                    } else {
                                      throw 'Could not launch ';
                                    }  
                        },
                        padding: EdgeInsets.all(0),
                        color: appPrimaryColor,
                        child: Text(
                            AppTranslations.of(context).text('Rating app'),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16)),
                      ),
                    ),
                   
                  ],
                ),
              ])
            ));
  }
}
