import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:two_bill/utils/app_translations.dart';

class TestWidget extends StatefulWidget {
  String date;
  TestWidget({Key key, @required this.date}) : super(key: key);
  @override
  _TestWidgetState createState() => _TestWidgetState(date);
}

class _TestWidgetState extends State<TestWidget> {
   String date;
  _TestWidgetState(String date) {
    this.date = date;
  }
  DateTime _lastButtonPress;
  List<String> _pressDuration=[];
  Timer _ticker;

  @override
  Widget build(BuildContext context) {
    return
        Row(
           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
           children: [
                           Text(_pressDuration[0].toString(),
            style: TextStyle(fontSize: 30,fontWeight: FontWeight.w900,color:   Color(0xff3864b9),),),
                              Text(_pressDuration[1].toString(),
            style: TextStyle(fontSize: 30,fontWeight: FontWeight.w900,color:   Color(0xff3864b9),),),
                               Text(_pressDuration[2].toString(),
            style: TextStyle(fontSize: 30,fontWeight: FontWeight.w900,color:   Color(0xff3864b9),),),
                               Text(_pressDuration[3].toString(),
            style: TextStyle(fontSize: 30,fontWeight: FontWeight.w900,color:   Color(0xff3864b9),),),
                               Text(_pressDuration[4].toString(),
            style: TextStyle(fontSize: 30,fontWeight: FontWeight.w900,color:   Color(0xff3864b9),),),
                               Text(_pressDuration[5].toString(),
            style: TextStyle(fontSize: 30,fontWeight: FontWeight.w900,color:   Color(0xff3864b9),),),
                               Text(_pressDuration[6].toString(),
            style: TextStyle(fontSize: 30,fontWeight: FontWeight.w900,color:   Color(0xff3864b9),),),
                              ],

      
    );
        
         
        
        
      
    
  }


  @override
  void initState() {
    super.initState();
    print(date);

DateTime parseDate =
    new DateFormat("yyyy-MM-dd hh:mm:ss").parse(date);
  print('here22 ${parseDate.difference(DateTime.now()).inDays}');
 
   _lastButtonPress = parseDate;
 
    _updateTimer();
    _ticker = Timer.periodic(Duration(seconds:1),(_)=>_updateTimer());

  }


  @override
  void dispose() {
    _ticker.cancel();
    super.dispose();
  }



  void _updateTimer() {
  
    final duration =_lastButtonPress.difference(DateTime.now());
 //   print(duration.inDays);
    
    setState(() {
      _pressDuration = _formatDuration(duration);
    });
  }

 List< String> _formatDuration(Duration duration) {
      List<String>dates=[];
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }

    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
      String twoDigitHour = twoDigits(duration.inHours.remainder(24));
       String twoDigitDay = twoDigits(duration.inDays.remainder(30));
      dates.add(twoDigitDay);
       dates.add(":");
        dates.add(twoDigitHour);
          dates.add(":");
           dates.add(twoDigitMinutes);
             dates.add(":");
             dates.add(twoDigitSeconds);
            
              
    return dates;
    
  }
}