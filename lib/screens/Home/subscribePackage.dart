import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:two_bill/model/package.dart';
import 'package:two_bill/model/subscribrion.dart';
import 'package:two_bill/screens/user_management/payment.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/screens/wedget/show_%20SweetAlert.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';

class SubscribePackage extends StatefulWidget {
  Package package;

  SubscribePackage({Key key, @required this.package})
      : super(key: key);
  @override
  _SubscribePackage createState() => _SubscribePackage(package);
}

class _SubscribePackage extends State<SubscribePackage> {
  Package package;
  _SubscribePackage(Package package) {
    this.package = package;
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
   LocalLogin localLogin=new LocalLogin();
  String phoneNumber;
  String cardNumber;
  String promoCode = "";
  Subscription subscription;
  Api api;
   Helper helper= new Helper();
  Request request;
  String url;
  bool _isLoading = false;
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }

  var headers;
  connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  @override
  void initState() {
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
  Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  handleValue(value) {

    if (value == "null" || value=="" || value ==null) {
      helper.removeToken();
       localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/Intro', (_) => false);
    }else{
      loadDate();
    }
  }

  loadDate() async {}

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: appfixedColor,
      appBar: CustomAppBar(
        title: "Service Payment",
        scafoldkey: _scaffoldKey,
      ),
      drawer: AppDrawer(
        scafoldkey: _scaffoldKey,
      ),
      bottomNavigationBar:
          customBottomNavigationBar(context, width, height, _scaffoldKey),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Form(
          key: globalKey,
          child: ListView(shrinkWrap: true, children: [
            Padding(
              padding: EdgeInsets.all(width * .02),
              child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  child: Container(
                      padding: EdgeInsets.all(width * .05),
                      child: Column(children: [
                        Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              AppTranslations.of(context).text("and") == "and"
                                  ? Text(
                                      'Service from ${package.service.nameEn}',
                                      style: TextStyle(
                                        color: Color(0xffc4c4c4),
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  : Text(
                                      '${package.service.nameAr} الخدمه من',
                                      style: TextStyle(
                                        color: Color(0xffc4c4c4),
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                              Image.asset(
                                add,
                                color: Color(0xffc4c4c4),
                              ),
                            ]),
                        SizedBox(
                          height: height * .02,
                        ),
                        Row(
                          children: [
                            AppTranslations.of(context).text("and") == "and"
                                ? Text(
                                    package.firstHeadlineEn,
                                    style: TextStyle(
                                        color: Color(0xff3864b9),
                                        fontSize: 25,
                                        fontWeight: FontWeight.w900),
                                  )
                                : Text(
                                    package.firstHeadlineAr,
                                    style: TextStyle(
                                        color: Color(0xff3864b9),
                                        fontSize: 25,
                                        fontWeight: FontWeight.w900),
                                  ),
                            SizedBox(
                              width: width * .01,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: height * .02,
                        ),
                        Row(children: [
                          AppTranslations.of(context).text("and") == "and"
                              ? Text(
                                  package.nameEn,
                                  style: TextStyle(
                                    color: Color(0xff464646),
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                  ),
                                )
                              : Text(
                                  package.nameAr,
                                  style: TextStyle(
                                    color: Color(0xff464646),
                                    fontSize: 22,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                        ]),
                        SizedBox(
                          height: height * .02,
                        ),
                        Row(
                          children: [
                            AppTranslations.of(context).text("and") == "and"
                                ? Text(
                                    package.descriptionEn,
                                       overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Color(0xff464646),
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                : Text(
                                    package.descriptionAr,
                                       overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      color: Color(0xff464646),
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                            SizedBox(
                              width: width * .02,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: height * .02,
                        ),
                        AppTranslations.of(context).text("and") == "and"
                            ? Row(
                                children: [
                                  RichText(
                                    text: new TextSpan(
                                      text: '${package.priceBeforeDiscount}EG',
                                      style: new TextStyle(
                                        color: Color(0xff464646),
                                        fontSize: 16,
                                        decoration: TextDecoration.lineThrough,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: width * .02,
                                  ),
                                  Text(
                                      '${package.priceAfterDiscount}EG/${package.type}',
                                      style: TextStyle(
                                        color: Color(0xff464646),
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      )),
                                  SizedBox(
                                    width: width * .04,
                                  ),
                                  Text(
                                    '${package.discountPercentage}%OFF',
                                    style: TextStyle(
                                      color: Colors.green,
                                    ),
                                  ),
                                ],
                              )
                            : Row(children: [
                                RichText(
                                  text: new TextSpan(
                                    text: '${package.priceBeforeDiscount}جنيه',
                                    style: new TextStyle(
                                      color: Color(0xff464646),
                                      fontSize: 16,
                                      decoration: TextDecoration.lineThrough,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: width * .04,
                                ),
                                package.type == "monthly"
                                    ? Text(
                                        '${package.priceAfterDiscount}جنيه/شهريا',
                                        style: TextStyle(
                                          color: Color(0xff464646),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14,
                                        ))
                                    : Text(
                                        '${package.priceAfterDiscount}جنيه/سنوي',
                                        style: TextStyle(
                                          color: Color(0xff464646),
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14,
                                        )),
                                SizedBox(
                                  width: width * .02,
                                ),
                                Text(
                                  'خصم${package.discountPercentage}%',
                                  style: TextStyle(
                                    color: Colors.green,
                                  ),
                                ),
                              ]),
                        SizedBox(
                          height: height * .05,
                        ),
                        Row(children: [
                          AppTranslations.of(context).text("and") == "and"
                              ? Image.asset(
                                  subscribeEn,
                                )
                              : Image.asset(
                                  subscribeAr,
                                ),
                          SizedBox(
                            width: width * .07,
                          ),
                          Text(
                            AppTranslations.of(context)
                                .text("Enter your phone"),
                            style: TextStyle(
                              color: Color(0xff3864b9),
                              fontSize: 20,
                              fontWeight: FontWeight.w900,
                            ),
                          )
                        ]),
                        SizedBox(
                          height: height * .03,
                        ),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          validator: (value) {
                            if (value.isEmpty)
                              return AppTranslations.of(context)
                                  .text("value is empty");
                          },
                          onSaved: (val) => phoneNumber = val,
                          decoration: InputDecoration(
                            labelText: AppTranslations.of(context)
                                .text('Mobile Number'),
                            prefixIcon: IconButton(
                              icon: Icon(
                                Icons.mobile_screen_share_sharp,
                                color: Color(0xffc4c4c4),
                              ),
                              onPressed: () {},
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey[100]),
                            ),
                          ),
                        ),
                        ExpansionTile(
                            initiallyExpanded: true,
                            
                            title: RichText(
                              text: new TextSpan(
                                text: AppTranslations.of(context)
                                    .text('Do you have promo code'),
                                style: new TextStyle(
                                  fontSize: 18,
                                  color: Color(0xff3864b9),
                                  fontWeight: FontWeight.w900,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                            children: <Widget>[
                              Container(
                                child: TextFormField(
                                  onSaved: (val) => promoCode = val,
                                  decoration: InputDecoration(
                                    labelText: AppTranslations.of(context)
                                        .text('promo code'),
                                    prefixIcon: IconButton(
                                      icon: Icon(
                                        Icons.content_paste,
                                        color: Color(0xffc4c4c4),
                                      ),
                                      onPressed: () {},
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.grey[100]),
                                    ),
                                  ),
                                ),
                              ),
                            ]),
                        SizedBox(
                          height: height * .02,
                        )
                      ]))),
            ),
            SizedBox(
              height: height * .02,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: width * .05),
              child: ButtonTheme(
                height: height * .08,
                child: _isLoading
                    ? circularProgress()
                    : RaisedButton(
                        elevation: 0.0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        onPressed: () async {
                          globalKey.currentState.save();

                          if (globalKey.currentState.validate()) {
                            _showLoading();
                            helper = new Helper();
                            api = new Api();
                            request = new Request();

                            await request
                                .getHeaader(context)
                                .then((dynamic dyHeaders) async {
                              headers = dyHeaders;
                            });
                            print('headers $headers');

                            if (promoCode != null && promoCode != "") {
                              url = "/api/subscriptions/checkPromoCode";
                              var body = <String, dynamic>{
                                'package_id': package.id,
                                'promocode': promoCode
                              };
                              api
                                  .postData(url, headers,context, body: body)
                                  .then((dynamic response) async {
                                final String res = response.body;
                                final int statusCode = response.statusCode;
                                if (statusCode == 401 ||
                                    statusCode == 400 ||
                                    statusCode == 403 ||
                                    statusCode == 501 ||
                                    statusCode == 404) {
                                  _hideLoading();
                                  print('sdhfhfhfdh    $statusCode');

                                  Map<String, dynamic> json = jsonDecode(res);
                                  print(json);
                                  showAlert(
                                      context,
                                      jsonEncode(
                                          json["errors"]["promocode"][0]),
                                      "error",
                                      null);
                                } else {
                                  url = "/api/subscriptions";
                                  var body = <String, dynamic>{
                                    'package_id': package.id,
                                    'phone': phoneNumber,
                                    'promocode': promoCode
                                  };

                                  api
                                      .postData(url, headers,context, body: body)
                                      .then((dynamic response) async {
                                    final String res = response.body;
                                    final int statusCode = response.statusCode;
                                    if (statusCode == 401 ||
                                        statusCode == 400 ||
                                        statusCode == 403 ||
                                        statusCode == 501 ||
                                        statusCode == 404) {
                                      _hideLoading();
                                      print(statusCode);

                                      Map<String, dynamic> json =
                                          jsonDecode(res);

                                      showAlert(context, json['message'],
                                          "error", null);
                                    } else {
                                      print(' usb $statusCode');
                                      Map<String, dynamic> json =
                                          jsonDecode(res);
                                      if (json['data']['is_paid'] == true || json['data']['is_paid'] == 1) {
                                        json['data']['is_paid'] = "1";
                                      } else{
                                        json['data']['is_paid'] = "0";
                                      }
                                      if (json['data']['active'] == true || json['data']['active'] == 1) {
                                        json['data']['active'] = "1";
                                      } else{
                                        json['data']['active'] = "0";
                                      }
                                      setState(() {
                                        subscription =
                                            Subscription.fromJson(json['data']);
                                      });
                                      _hideLoading();
                                      // Alert(
                                      //   context: context,
                                      //   type: AlertType.success,
                                      //   title: AppTranslations.of(context)
                                      //               .text("and") ==
                                      //           "and"
                                      //       ? "Promocode"
                                      //       : "الكود",
                                      //   desc: subscription.promocode,
                                      //   buttons: [
                                      //     DialogButton(
                                      //       child: Text(
                                      //         "Ok",
                                      //         style: TextStyle(
                                      //             color: Colors.white,
                                      //             fontSize: 20),
                                      //       ),
                                      //       onPressed: () => Navigator.push(
                                      //           context,
                                      //           MaterialPageRoute(
                                      //             builder: (context) => Payment(
                                      //               subscription: subscription,
                                      //               promocode: promoCode,
                                      //             ),
                                      //           )),
                                      //       width: 120,
                                      //     )
                                      //   ],
                                      // ).show();
                                     // print(json['data']['is_paid']);
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => Payment(
                                              subscription: subscription,
                                              promocode: promoCode,
                                            ),
                                          ));
                                    }
                                  });
                                }
                              });
                            } else {
                              url = "/api/subscriptions";

                              var body = <String, dynamic>{
                                'package_id': package.id,
                                'phone': phoneNumber,
                                'promocode': promoCode
                              };
                              api
                                  .postData(url, headers,context, body: body)
                                  .then((dynamic response) async {
                                final String res = response.body;
                                final int statusCode = response.statusCode;
                                if (statusCode == 401 ||
                                    statusCode == 400 ||
                                    statusCode == 403 ||
                                    statusCode == 501 ||
                                    statusCode == 404) {
                                  _hideLoading();
                                  print('sub susb usb $statusCode');

                                  Map<String, dynamic> json = jsonDecode(res);
                                  Alert(
                                    context: context,
                                    type: AlertType.error,
                                    title: AppTranslations.of(context)
                                                .text("and") ==
                                            "and"
                                        ? "Eror"
                                        : "خطا",
                                    desc: AppTranslations.of(context)
                                                .text("and") ==
                                            "and"
                                        ? json['message']
                                        : "انت بالفعل مشترك في هذه الباقة",
                                    buttons: [
                                      DialogButton(
                                        child: Text(
                                          "Ok",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 20),
                                        ),
                                        onPressed: () => Navigator.pushNamed(
                                            context, '/MyPackages'),
                                        width: 120,
                                      )
                                    ],
                                  ).show();
                                } else {
                                  print(' usb $statusCode');
                                  Map<String, dynamic> json = jsonDecode(res);
                                  if (json['data']['is_paid'] == true) {
                                    json['data']['is_paid'] = "1";
                                  } else
                                    json['data']['is_paid'] = "0";
                                  if (json['data']['active'] == true) {
                                    json['data']['active'] = "1";
                                  } else
                                    json['data']['active'] = "0";
                                  setState(() {
                                    subscription =
                                        Subscription.fromJson(json['data']);
                                  });
                                  _hideLoading();
                                  // Alert(
                                  //   context: context,
                                  //   type: AlertType.success,
                                  //   title: AppTranslations.of(context)
                                  //               .text("and") ==
                                  //           "and"
                                  //       ? "Promocode"
                                  //       : "الكود",
                                  //   desc: subscription.promocode,
                                  //   buttons: [
                                  //     DialogButton(
                                  //       child: Text(
                                  //         "Ok",
                                  //         style: TextStyle(
                                  //             color: Colors.white,
                                  //             fontSize: 20),
                                  //       ),
                                  //       onPressed: () => Navigator.push(
                                  //           context,
                                  //           MaterialPageRoute(
                                  //             builder: (context) => Payment(
                                  //               subscription: subscription,
                                  //               promocode: promoCode,
                                  //             ),
                                  //           )),
                                  //       width: 120,
                                  //     )
                                  //   ],
                                  // ).show();
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => Payment(
                                          subscription: subscription,
                                          promocode: promoCode,
                                        ),
                                      ));
                                }
                              });
                            }
                          }
                        },
                        padding: EdgeInsets.all(0),
                        color: appPrimaryColor,
                        child: Text(
                            AppTranslations.of(context).text('Continue'),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16)),
                      ),
              ),
            ),
            SizedBox(
              height: height * .08,
            ),
          ]),
        ),
      ),
    );
  }
}
