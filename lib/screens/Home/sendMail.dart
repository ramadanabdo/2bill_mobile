import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:two_bill/screens/wedget/CustomTextField.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/screens/wedget/show_%20SweetAlert.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/utils/style.dart';

class SendMail extends StatefulWidget {
  @override
  _SendMail createState() => _SendMail();
}

class _SendMail extends State<SendMail> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> globalKey = new GlobalKey<FormState>();
   LocalLogin localLogin=new LocalLogin();
   Helper helper=new Helper();
  String name;
  String email;
  String text;
  Api api;
  Request request;
  var headers;
  String url;
  bool _isLoading = false;
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }
connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  sendEmai() async {
    globalKey.currentState.save();

    if (globalKey.currentState.validate()) {
      _showLoading();
      // Navigator.pushNamed(context, '/Login');
      var body = <String, dynamic>{
        'name': name,
        'email': email,
        'message': text,
      };
      api = new Api();
      request = new Request();
      url = "/api/infos";
      await request.getHeaader(context).then((dynamic dyHeaders) async {
        headers = dyHeaders;
      });
      await api
          .postData(url, headers, context,body: body)
          .then((dynamic response) async {
        final String res = response.body;
        final int statusCode = response.statusCode;
        if (statusCode == 401 ||
            statusCode == 400 ||
            statusCode == 403 ||
            statusCode == 501 ||
            statusCode == 404) {
          print(statusCode);
          _hideLoading();
           Map<String, dynamic> json = jsonDecode(res);
          if (json['errors']['message'] != null) {
            showAlert(context, json["errors"]["message"][0], "error", null);
          } else if (json['errors']['name'] != null) {
            showAlert(context, json["errors"]["name"][0], "error", null);
          } else if (json['errors']['email'] != null) {
            showAlert(context, json["errors"]["email"][0], "error", null);
          }
        } else {
          _hideLoading();
          Alert(
            context: context,
            type: AlertType.success,
            title: AppTranslations.of(context).text("and") == "and"
                ? "Success"
                : "تم بنجاح",
            desc: AppTranslations.of(context).text("Message sent successfully"),
            buttons: [
              DialogButton(
                child: Text(
                  "Ok",
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () => Navigator.pop(context),
                width: 120,
              )
            ],
          ).show();
        }
      });
    }
  }
 @override
  void initState() {
    super.initState();
           Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  handleValue(value) {

    if (value == "null" || value=="" || value ==null) {
      helper.removeToken();
       localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/Intro', (_) => false);
    }else{
      loadData();
    }
  }
  loadData()async{}
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: appfixedColor,
        appBar: CustomAppBar(
          title: "Contact Us",
          scafoldkey: _scaffoldKey,
        ),
        drawer: AppDrawer(
          scafoldkey: _scaffoldKey,
        ),
        bottomNavigationBar:
            customBottomNavigationBar(context, height, width, _scaffoldKey),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Stack(alignment: Alignment.center, children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                  left: width * .05, right: width * .05, top: height * .05),
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(15),
                  ),
                ),
                elevation: 12.0,
                child: Container(
                
                  padding: EdgeInsets.all(width * .08),
                  child: Form(
                    key: globalKey,
                    child: ListView(
                        shrinkWrap: true,
                      //  physics: NeverScrollableScrollPhysics(),
                        children: [
                          Text(
                            AppTranslations.of(context)
                                .text('Dear Customer thank you for'),
                            textAlign: TextAlign.start,
                            style: smalBigStyle(),
                          ),
                          Text(
                            AppTranslations.of(context)
                                .text('cintacting us kindly find that'),
                            textAlign: TextAlign.start,
                            style: smalBigStyle(),
                          ),
                          Text(
                            AppTranslations.of(context)
                                .text('Customer service working hours'),
                            textAlign: TextAlign.start,
                            style: smalBigStyle(),
                          ),
                          Text(
                            AppTranslations.of(context)
                                .text('from 9:00 Am till  1:00 Am'),
                            textAlign: TextAlign.start,
                            style: smalBigStyle(),
                          ),
                          SizedBox(
                            height: height * .02,
                          ),
                          CustomTextField(
                            hint: AppTranslations.of(context).text('Name'),
                            onclick: (value) {
                              name = value;
                            },
                            color: true,
                          ),
                          SizedBox(
                            height: height * .02,
                          ),
                          CustomTextField(
                            hint: AppTranslations.of(context).text('Email'),
                            onclick: (value) {
                              email = value;
                            },
                            isEmail: true,
                            color: true,
                          ),
                          SizedBox(
                            height: height * .02,
                          ),
                          CustomTextField(
                            hint: AppTranslations.of(context).text('Message'),
                            onclick: (value) {
                              text = value;
                            },
                            minLength: true,
                        
                            mainlan: 3,
                            color: true,
                          ),
                          SizedBox(
                            height: height * .02,
                          ),
                          ButtonTheme(
                            height: height * .08,
                            minWidth: MediaQuery.of(context).size.width,
                            child: _isLoading
                                ? circularProgress()
                                : RaisedButton(
                                    elevation: 0.0,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    onPressed: () async {
                                      sendEmai();
                                    },
                                    padding: EdgeInsets.all(0),
                                    color: appPrimaryColor,
                                    child: Text(
                                        AppTranslations.of(context)
                                            .text('Leave a message'),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16)),
                                  ),
                          ),
                        ]),
                  ),
                ),
              ),
            ),
            Positioned(
              top: height * .01,
              child: Image(
                image: AssetImage(sendMail),
              ),
            ),
          ]),
        ));
  }
}
