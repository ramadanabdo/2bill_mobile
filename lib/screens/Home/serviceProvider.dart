import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:textfield_search/textfield_search.dart';
import 'package:two_bill/model/package.dart';
import 'package:two_bill/model/service.dart';
import 'package:two_bill/model/provider.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:http/http.dart' as http;
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';
import 'package:two_bill/utils/style.dart';

import 'package.dart';

class Serviceprovider extends StatefulWidget {
  Provider provider;

  Serviceprovider({Key key, @required this.provider}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
   
    return new _Serviceprovider(provider);
  }

}

class _Serviceprovider extends State<Serviceprovider> {
 Provider provider;
    _Serviceprovider(Provider provider){
      this.provider=provider;
    }

    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
   LocalLogin localLogin=new LocalLogin();
  Api api;
  Request request;
  String url;
  var headers;
var body;
  List<ServiceProvider> services = [];
  
   Helper helper= new Helper();
  bool _isLoading = false;


  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }
  connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }

  @override
  void initState() {
    super.initState();
       Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
   Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  handleValue(value) {

    if (value == "null" || value=="" || value ==null) {
      helper.removeToken();
       localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/Intro', (_) => false);
    }else{
      loadDate();
    }
  }
  loadDate() async {
  
    _showLoading();

    api = new Api();
    request = new Request();
     body = request.getBody(30, 0, "id", "ASC", "all",
      "false", "false", "provider_id", "=", provider.id.toString());
    url = "/api/services$body";
 
    print(body);
    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    print(headers);

    await api
        .getData(
      url,
      headers,
      context
    )
        .then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print(statusCode);
        _hideLoading();
      } else {
        Map<String, dynamic> json = jsonDecode(res);
        print(json['data']['data']);
        setState(() {
          services = ServiceProvider.decode(jsonEncode(json['data']['data']));
        });
        _hideLoading();
        print(services.length);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: appfixedColor,
      appBar: CustomAppBar(title:"Service Payment",scafoldkey:_scaffoldKey ,),
        drawer: AppDrawer(
        scafoldkey: _scaffoldKey,
      ),
   bottomNavigationBar:
            customBottomNavigationBar(context, width, height, _scaffoldKey),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: SingleChildScrollView(
                  child: Container(
              width: width,
             
              padding: EdgeInsets.all(0),
              decoration: new BoxDecoration(
                color: appfixedColor,
                image: DecorationImage(image: AssetImage(background),fit: BoxFit.cover,),
                
              ),
              child: ListView(
                  shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: width * .01,
                          right: width * .01,
                          top: height * .03),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AppTranslations.of(context).text("and") == "and"
                                ? Image.asset(providerEn)
                                : Image.asset(providerAr),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: height * .025,
                                ),
                                AppTranslations.of(context).text('and') == "and"
                                    ? Text(
                                        '' +
                                            AppTranslations.of(context)
                                                .text('Select service from') +
                                            ' ${provider.nameEn}',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          height: 1.5,
                                          color: Color(0xff414C5B),
                                          fontSize: 14,
                                        ))
                                    : Text(
                                        '' +
                                            AppTranslations.of(context)
                                                .text('Select service from') +
                                            ' ${provider.nameAr}',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          height: 1.5,
                                          color: Color(0xff414C5B),
                                          fontSize: 14,
                                        )),
                              ],
                            )
                          ]),
                    ),
                
                    _isLoading
                        ? circularProgress()
                        : Padding(
                            padding:
                                EdgeInsets.symmetric(horizontal: width * .05),
                            child: services.length>0? ListView.builder(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: services.length,
                                itemBuilder: (context, index) {
                                  return GestureDetector(
                                    onTap: () {
                                      
                                        Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => PackageScrean(
                                      service: services[index],
                          
                                    ),
                                  ));
                                     
                                    },
                                    child: Container(
                                      
                                        height: 100,
                                        padding: EdgeInsets.only(top:height*0.01),
                                            
                                        child: Material(
                                          borderRadius: BorderRadius.circular(30),
                                          elevation: 20.0,
                                          shadowColor: appfixedColor,
                                          child: Row(
                                            children: [
                                              SizedBox(
                                                width: width * .05,
                                              ),
                                              Image.network(services[index].logo),
                                              SizedBox(
                                                width: width * .03,
                                              ),
                                              AppTranslations.of(context)
                                                          .text("and") ==
                                                      "and"
                                                  ? Center(
                                                      child: Text(
                                                        services[index].nameEn,
                                                        style: TextStyle(
                                                          color: appPrimaryColor,
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    )
                                                  : Center(
                                                      child: Text(
                                                        services[index].nameAr,
                                                        style: TextStyle(
                                                          color: appPrimaryColor,
                                                          fontSize: 18,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      ),
                                                    )
                                            ],
                                          ),
                                        )),
                                  );
                                  
                                }): Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                                SizedBox(
                                                  height: height * .01,
                                                ),
                                                Text(
                                                  AppTranslations.of(context).text(
                                                      'No Service avaliable'),
                                                  style: TextStyle(
                                                    color: Color(0xffd0d0d0),
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.bold,
                                                  ),
                                                ),
                                              ]),
                          ),
                  ])),
        ),
      ),
    );
  }
}
