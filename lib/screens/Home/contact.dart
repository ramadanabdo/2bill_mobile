import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:two_bill/model/Contact.dart';
import 'package:two_bill/screens/wedget/appDrawer.dart';
import 'package:two_bill/screens/wedget/app_bar.dart';
import 'package:two_bill/screens/wedget/loadingWidget.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/app_translations.dart';
import 'package:two_bill/utils/colors.dart';
import 'package:two_bill/utils/constants.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/localLogin.dart';
import 'package:two_bill/utils/request.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactInfo extends StatefulWidget {
  @override
  _ContactInfo createState() => _ContactInfo();
}

class _ContactInfo extends State<ContactInfo> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  LocalLogin localLogin=new LocalLogin();
  Api api;
   Helper helper= new Helper();
  Contact contact;
  Request request;
  String url;
    var headers;
  bool _isLoading = false;
  _showLoading() {
    setState(() {
      _isLoading = true;
    });
  }

  _hideLoading() {
    setState(() {
      _isLoading = false;
    });
  }


  loadData() async {
    _showLoading();
    api = new Api();
    request = new Request();
    url = "/api/infos";
    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    print(headers);

    await api.getData(url, headers,context).then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print(statusCode);
      } else {
        Map<String, dynamic> json = jsonDecode(res);
        print(json['data']['data']);
        setState(() {
          contact = Contact.fromJson(json['data']['data']);
        });
        _hideLoading();
      }
    });
  }
void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(AppTranslations.of(context).text(text))));
  }
  connectstatus(value) {
    if (value == false) {

      showErrorDialog(context,"check_internet");
    }
  }
  @override
  initState() {
    super.initState();
     Future<bool> connecting = Helper().checkconnectivity();
    connecting.then((bool value) => connectstatus(value));
  Future<dynamic> future = helper.getsharedpref("userToken");
    future.then((value) => handleValue(value));
    
  }
  handleValue(value) {

    if (value == "null" || value=="" || value ==null) {
      helper.removeToken();
      localLogin.clearFile();
      Navigator.pushNamedAndRemoveUntil(context, '/Intro', (_) => false);
    }else{
      loadData();
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: appfixedColor,
        appBar: CustomAppBar(
          title: "Help & Support",
          scafoldkey: _scaffoldKey,
        ),
        drawer: AppDrawer(
          scafoldkey: _scaffoldKey,
        ),
        bottomNavigationBar:
            customBottomNavigationBar(context, height, width, _scaffoldKey),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: _isLoading
                ? circularProgress()
                : ListView(shrinkWrap: true, children: [
                    Padding(
                        padding: EdgeInsets.all(width * .02),
                        child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                            ),
                            child: Container(
                                padding: EdgeInsets.all(width * .05),
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: height * .05,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            AppTranslations.of(context)
                                                .text('Call us'),
                                            style: TextStyle(
                                                color: Color(0xff555555),
                                                fontSize: 25,
                                                fontWeight: FontWeight.w900),
                                          ),
                                          GestureDetector(
                                            onTap: () 
                                             async {
                                              if (await canLaunch("tel://${contact.phone}")) {
                                                await launch("tel://${contact.phone}");
                                              } else {
                                                _showSnackBar("Could not Email");
                                              }
                                              
                                            },
                                            child: Image.asset(phone2),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: height * .02,
                                      ),
                                      Text(
                                        contact.phoneMessage.toString(),
                                        style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.black,
                                        ),
                                      ),
                                      SizedBox(
                                        height: height * .02,
                                      ),
                                      InkWell(
                                        onTap: ()async {
                                              if (await canLaunch("tel://${contact.phone}")) {
                                                await launch("tel://${contact.phone}");
                                              } else {
                                                _showSnackBar("Could not Email");
                                              }
                                              
                                            },
                                                                              child: Text(
                                          contact.phone.toString(),
                                          style: TextStyle(
                                              color: Color(0xff555555),
                                              fontSize: 25,
                                              fontWeight: FontWeight.w900),
                                        ),
                                      ),
                                      SizedBox(
                                        height: height * .02,
                                      ),
                                      Container(
                                        height: 1.0,
                                        width: width,
                                        color: Color(0xffcfcfcf),
                                      ),
                                      SizedBox(
                                        height: height * .02,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            AppTranslations.of(context)
                                                .text('Contact us'),
                                            style: TextStyle(
                                                color: Color(0xff555555),
                                                fontSize: 25,
                                                fontWeight: FontWeight.w900),
                                          ),
                                          GestureDetector(
                                            onTap: () async {
                                              if (await canLaunch(
                                                 "mailto://${contact.email}")) {
                                                await launch(
                                                    "mailto://${contact.email}");
                                              } else {
                                                _showSnackBar("Could not Email");
                                              }
                                              
                                            },
                                            child: Image.asset(
                                              email2,
                                              width: 20,
                                              color: Color(0xff436dbd),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: height * .02,
                                      ),
                                      InkWell(
                                        onTap: ()async {
 if (await canLaunch(
                                                 "mailto:${contact.email}")) {
                                                await launch(
                                                    "mailto:${contact.email}");
                                              } else {
                                                _showSnackBar("Could not Email");
                                              }
                                              
                                            },
                                                                              child: Text(
                                          contact.emailMessage.toString(),
                                          style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: height * .02,
                                      ),
                                      InkWell(
                                        onTap: ()async {
                                          if (await canLaunch(
                                                 "mailto:${contact.email}")) {
                                                await launch(
                                                    "mailto:${contact.email}");
                                              } else {
                                                _showSnackBar("Could not Email");
                                              }
                                              
                                            },
                                                                              child: Text(
                                          contact.email.toString(),
                                          style: TextStyle(
                                              color: Color(0xff555555),
                                              fontSize: 25,
                                              fontWeight: FontWeight.w900),
                                        ),
                                      ),
                                      SizedBox(
                                        height: height * .05,
                                      ),
                                    ]))))
                  ])));
  }
}
