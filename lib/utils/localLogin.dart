import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_des/flutter_des.dart';
import 'package:path_provider/path_provider.dart';
import 'package:two_bill/model/user.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/helper.dart';
import 'package:two_bill/utils/request.dart';
import 'package:http/http.dart' as http;

class LocalLogin {

  Helper helper = new Helper();
  static final BASE_URL = 'https://admin.2bill.net';

  String url;
  var headers;
  Request request = new Request();
  static const key = "u1BvOHzUOcklgNpn1MaWvdn9DT4LyzSX";
  static const iv = "12345678";
  Future<dynamic> encrypt(contents) async {
    return await FlutterDes.encryptToBase64(contents, key, iv: iv);
  }

  Future<String> decrypt(contents) async {
    return await FlutterDes.decryptFromBase64(contents, key, iv: iv);
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/2bill.txt');
  }

  Future<File> writeData(contents) async {
    final file = await _localFile;

    // Write the file.
    return file.writeAsString(contents.toString());
  }

  Future<File> clearFile() async {
    final file = await _localFile;

    // Write the file.
    return file.delete();
  }

  Future<String> readData() async {
    try {
      final file = await _localFile;

      // Read the file.
      String contents = await file.readAsString();

      return contents;
    } catch (e) {
      // If encountering an error, return 0.
      return null;
    }
  }

  // ignore: non_constant_identifier_names
  void auth_refreshtoken(context) async {
    readData().then((var decr) async {
      print("decr      $decr");
      if (decr == null || decr == "") {
        await helper.removeToken();
      
         await clearFile();
  print('refhere');
              Navigator.of(context)
                  .pushNamedAndRemoveUntil("/SignIn", (route) => false);
           
       
      } else {
        decrypt(decr).then((var data) async {
          print(data);
          var list = data.split(" ");
          String _username = list[0];
          String _password = list[1];

          var body = <String, dynamic>{
            'phone': _username,
            'password': _password,
          };

          helper = new Helper();
          url = "/api/auth/login";
          await request.getHeaader(context).then((dynamic dyHeaders) async {
            headers = dyHeaders;
          });
          print('headers $headers');
          var bodyData = json.encode(body);
          print(bodyData);
        await   http
              .post('$BASE_URL' + '$url', body: bodyData, headers: headers)
              .then((http.Response response) async {
            final String res = response.body;
            final int statusCode = response.statusCode;
            if (response.statusCode == 200) {
              Map<String, dynamic> json = jsonDecode(res);
              print(json['access_token']);
              await helper.sharedpref("userToken", json['access_token']);
              print("user    ");
              Future<User> future = helper.getUserData(context);
              future.then((myuser) async {
                await helper.sharedpref("user", myuser);
              
              });
               print('refhere2');
               
              Navigator.of(context)
                  .pushNamedAndRemoveUntil("/", (route) => false);
            }else{
               await helper.removeToken();
            await clearFile();
             print('refhere3');
                  Navigator.of(context)
                      .pushNamedAndRemoveUntil("/SignIn", (route) => false);
              
            }
          });

          //////////////////////////////
        });
      }
    });
  }
}
