import 'package:flutter/cupertino.dart';
import 'package:two_bill/utils/app_translations.dart';

import 'helper.dart';

class Request {
  static  Map<String, String> headers;
   Helper helper;
   getBody(
      int limit,
      int offset,
      String field,
      String sort,
      String resource,
      String deleted,
      String paginate,
    String columns,
    String operand,
      String columnValues) {
         String body;
         if(columns==null||operand==null || columnValues==null){
           body="?limit=$limit&offset=$offset&field=$field&sort=$sort&resource=$resource&deleted=$deleted&paginate=$paginate";
         }
    body = "?limit=$limit&offset=$offset&field=$field&sort=$sort&resource=$resource&deleted=$deleted&paginate=$paginate&columns[0]=$columns&operand[0]=$operand&column_values[0]=$columnValues";
    return body;
  }

  Future<dynamic> getHeaader(BuildContext context) async {
    helper = new Helper();
    String userToken = await helper.getsharedpref("userToken");
    if(userToken!=null){
   headers = {
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'Bearer $userToken',
      'Content-Type': 'application/json',
      'language':AppTranslations.of(context).text("and")=="and"?'en':'ar',
      'Accept': 'application/json',
      'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
    };
    }else{
      headers = {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
    };
    }
 
    return headers;
  }
}
