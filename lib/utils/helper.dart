import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:two_bill/model/user.dart';
import 'package:two_bill/services/api.dart';
import 'package:two_bill/utils/request.dart';

class Helper {
  String userToken;
  SharedPreferences pref;
  Api api;
  Request request;
  String url;
  var headers;
  User user;
  
 getsharedpref(String define) async {
    pref = await SharedPreferences.getInstance();
    return pref.getString(define);
  }






  Future<void> removeToken() async {

    pref = await SharedPreferences.getInstance();
    
    await pref.clear();

  }

  Future<void> sharedpref(String define, dynamic object) async {
    print(define);
    print(object);
    if (define == "userToken") {
      pref = await SharedPreferences.getInstance();
      pref.setString("$define", object);
    } else {
      pref = await SharedPreferences.getInstance();
      pref.setString("$define", jsonEncode(object));
    }
  }

  Future<bool> checkconnectivity() async {
    bool isconnected = false;
    print('dfhfkfdkh');
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      // I am connected to a mobile network.
      isconnected = true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      // I am connected to a wifi network.
      isconnected = true;
    } else {
      isconnected = false;
    }
    print(isconnected);
    return isconnected;
  }

  Future<User> getUserData(BuildContext context) async {
    api = new Api();
    request = new Request();
    url = "/api/user";
    await request.getHeaader(context).then((dynamic dyHeaders) async {
      headers = dyHeaders;
    });
    print('headers $headers');
    await api.getData(url, headers,context).then((dynamic response) async {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode == 401 ||
          statusCode == 400 ||
          statusCode == 403 ||
          statusCode == 501 ||
          statusCode == 404) {
        print(statusCode);
      } else {
        Map<String, dynamic> json = jsonDecode(res);
       // print(json);
        user = User.fromJson(json['data']);
        print(user.name);
      }
    });
    return user;
  }
}
