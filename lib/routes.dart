
import 'package:flutter/cupertino.dart';
import 'package:two_bill/model/package.dart';
import 'package:two_bill/screens/Home/about.dart';
import 'package:two_bill/screens/Home/contact.dart';
import 'package:two_bill/screens/Home/home.dart';
import 'package:two_bill/screens/Home/package.dart';
import 'package:two_bill/screens/Home/sendMail.dart';
import 'package:two_bill/screens/Home/serviceProvider.dart';
import 'package:two_bill/screens/splash/intro.dart';
import 'package:two_bill/screens/splash/intro_1.dart';
import 'package:two_bill/screens/splash/intro_3.dart';
import 'package:two_bill/screens/splash/intro_4.dart';
import 'package:two_bill/screens/splash/intro_2.dart';
import 'package:two_bill/screens/user_management/forgetPassword.dart';
import 'package:two_bill/screens/user_management/myNotification.dart';
import 'package:two_bill/screens/user_management/payment.dart';
import 'package:two_bill/screens/user_management/changePassword.dart';
import 'package:two_bill/screens/user_management/createAccount.dart';
import 'package:two_bill/screens/user_management/myFavert.dart';
import 'package:two_bill/screens/user_management/myPrfoilePackages.dart';
import 'package:two_bill/screens/user_management/myProfile.dart';
import 'package:two_bill/screens/user_management/perifyCode.dart';
import 'package:two_bill/screens/user_management/signIn.dart';
import 'package:two_bill/screens/user_management/signUp.dart';




final routes = {
   '/'             :(BuildContext context) => new Intro(),
   '/home'             :(BuildContext context) => new Home(),
    '/Intro'             :(BuildContext context) => new Intro(),
 '/Intro_1':         (BuildContext context) => new Intro_1(),
 '/Intro_2':         (BuildContext context) => new Intro_2(),
 '/Intro_3':         (BuildContext context) => new Intro_3(),
 '/Intro_4':         (BuildContext context) => new Intro_4(),
 '/SignUp' :        (BuildContext context) => new SignUp(),
  '/SignIn':      (BuildContext context) => new SignIn(),
  '/createAccount': (BuildContext context) => new CreateAccount(),
  '/changePassword':(BuildContext context) => new ChangePassword(),
    '/forgetPassword':(BuildContext context) => new ForgetPassword(),
 
  '/MyFavorit'        :(BuildContext context) => new MyFavortScrean(),
  '/MyPackages'         :(BuildContext context) => new MyPackages(),
  '/MyProfile'         :(BuildContext context) => new MyProfile(),
   '/sendMail'         :(BuildContext context) => new SendMail(),
   '/contact'          :(BuildContext context) => new ContactInfo(),
   '/about'            :(BuildContext context) => new About(),
   '/notification'     :(BuildContext context) => new NotificationScreen(),
   '/prefyCod'        :(BuildContext context) => new PerfiyCod(), 


};